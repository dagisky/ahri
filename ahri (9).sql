-- phpMyAdmin SQL Dump
-- version 3.4.10.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 21, 2015 at 12:07 AM
-- Server version: 5.5.20
-- PHP Version: 5.3.10

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ahri`
--

-- --------------------------------------------------------

--
-- Table structure for table `about_ahri`
--

CREATE TABLE IF NOT EXISTS `about_ahri` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `detail` longtext,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `about_ahri`
--

INSERT INTO `about_ahri` (`id`, `title`, `detail`, `created`, `modified`) VALUES
(1, 'Office of the Director', '&nbsp;', '2014-11-18 00:00:00', '2014-11-29 03:39:08'),
(2, 'Scientific Advisory board', NULL, '2014-11-18 00:00:00', '2014-11-18 00:00:00'),
(3, 'Organizational chart', NULL, '2014-11-18 00:00:00', '2014-11-18 00:00:00'),
(4, 'AHRI/ALERT Ethics Review Committee(AAERC)', NULL, '2014-11-18 00:00:00', '2014-11-18 00:00:00'),
(5, 'Pan-Africa Bioethics Initiatives(PABIN)', NULL, '2014-11-18 00:00:00', '2014-11-18 00:00:00'),
(6, 'Tuberculosis Research Advisory Committee(TRAC)', NULL, '2014-11-18 00:00:00', '2014-11-18 00:00:00'),
(7, 'Stakeholder Engagement', NULL, '2014-11-18 00:00:00', '2014-11-18 00:00:00'),
(8, 'About Us', '<h3>Armauer Hansen Research Institute (AHRI)</h3>\r\n<p>\r\n    The Armauer Hansen Research Institute (AHRI) was founded in 1970 through the initiative of the Norwegian \r\n    and Swedish Save the Children organizations seconded by the Ministry of Health of Ethiopia. The Institute \r\n    got its name from the Norwegian physician, Gerhard Henrik Armauer Hansen, who first described the leprosy \r\n    bacillus (Mycobacterium leprae). AHRI was established as a biomedical research institute located next to \r\n    the All Africa Leprosy Rehabilitation and Training Hospital (ALERT).\r\n</p>\r\n\r\n<p>\r\n    The Institute joined the Ethiopian Ministry of Health in 2004. AHRI receives core support for its research \r\n    activities from SIDA and NORAD. It enjoys tax free privileges from the Ethiopian government. Postgraduate \r\n    (MSc and some PhD) theses are funded from the core budget of the Institute. Most of other research is \r\n    funded by competitive grants. AHRI research activities cover basic (immunology and molecular biology), \r\n    epidemiological and translational research. AHRI has published more than 380 papers in peer reviewed \r\n    journals so far. It has also produced several theses and dissertations from Ethiopian and international \r\n    scholars in biomedical research. The Institute has a network of national and international collaborators\r\n    in peer reviewed grant projects, clinical trial partnerships, capacity building activities and in training \r\n    of MSc and PhD students. this is cool</p>', '2014-11-18 00:00:00', '2014-12-11 11:36:31'),
(9, 'Vision and Mission', '<h3>Mission</h3>\r\n<ul>\r\n    <li>To contribute to a better understanding of mycobacterial and other relevant infectious diseases through\r\n        basic biomedical research</li>\r\n    <li>To contribute to the development and evaluation of new and improved methods and tools for the prevention,\r\n        treatment and control of important infectious diseases in Ethiopia and the Horn of Africa region</li>\r\n    <li>To contribute to capacity building and human resource development in Ethiopia and East Africa.</li>\r\n    \r\n</ul>\r\n\r\n<h3>Vision</h3>\r\n\r\n<ul>\r\n    <li>Leadership in providing scientific evidence for setting policies related to improved control of\r\n        high-burden infectious diseases in Ethiopia and the Horn of Africa region</li>\r\n    <li>Human resource development, involving competition for a place in the search for new knowledge and\r\n        skills applied to relevant basic and applied bio-medical research</li>\r\n    <li>Establishment of functional, efficient, viable and sustainable mechanism (s) of addressing the public\r\n        health needs of Ethiopia and the region</li>\r\n    <li>Creation of productive, long-term research collaborations, partnerships and networks in Ethiopia and\r\n        the region</li>\r\n    <li>The ability to take risks and at times fail in the pursuit of new insights</li>\r\n</ul>', '2014-11-18 00:00:00', '2014-11-18 00:00:00'),
(10, 'Summer Internship', NULL, '2014-11-18 10:00:00', '2014-11-18 00:00:00'),
(11, 'Post Graduate Research Support Program', NULL, '2014-11-20 00:00:00', '2014-11-20 00:00:00'),
(12, 'Useful Links', NULL, '2014-11-20 00:00:00', '2014-11-20 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `data_statistics_staffs`
--

CREATE TABLE IF NOT EXISTS `data_statistics_staffs` (
  `data_stat_staff_id` int(11) NOT NULL AUTO_INCREMENT,
  `position` varchar(100) DEFAULT NULL,
  `staff_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`data_stat_staff_id`),
  KEY `staff_id` (`staff_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `data_statistics_staffs`
--

INSERT INTO `data_statistics_staffs` (`data_stat_staff_id`, `position`, `staff_id`) VALUES
(1, 'Data Analyzer', 92),
(2, 'PHD in Statistics', 93);

-- --------------------------------------------------------

--
-- Table structure for table `directors`
--

CREATE TABLE IF NOT EXISTS `directors` (
  `director_id` int(11) NOT NULL AUTO_INCREMENT,
  `staff_id` int(11) DEFAULT NULL,
  `director_title` varchar(7) DEFAULT NULL,
  `edu_level` varchar(255) DEFAULT NULL,
  `biography` longtext,
  `from_date` date DEFAULT NULL,
  `to_date` date DEFAULT NULL,
  `status` enum('active','inactive') DEFAULT 'inactive',
  PRIMARY KEY (`director_id`),
  KEY `staff_id` (`staff_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `directors`
--

INSERT INTO `directors` (`director_id`, `staff_id`, `director_title`, `edu_level`, `biography`, `from_date`, `to_date`, `status`) VALUES
(3, 43, 'Dr.', 'M.D., Ph.D.  Scientific Director', '<div>Dr. Abraham Aseffa, M.D., Ph.D.,&nbsp;graduated in medicine from Gondar College of Medical Sciences&nbsp;in Ethiopia (great distinction) and specialized in Medical Microbiology at the University of Leipzig in Germany where he did his dissertation on the development of an attenuated live vaccine against&nbsp;Listeria monocytogenes&nbsp;(summa cum laude).&nbsp;He joined and later headed the Department of Microbiology in Gondar College as Associate Professor. He conducted leprosy research for 1 year in Carville, LA as a senior Fulbright scholar and basic leishmania immunology research for 3 years in WHO IRTC in Lausanne, Switzerland. He has been working at the Armauer Hansen research Institute (AHRI) since 2001 as senior scientist, Deputy Director and currently as Scientific Director. Abraham Aseffa has substantial experience in collaborative TB, TB/HIV, leishmania and infectious disease research and in supervising graduate students at AHRI (over 15 PhD and 50 MSc completed and ongoing), as well as teaching Immunology and Clinical Microbiology at Addis Ababa University as honorary faculty member. He has co-authored over 167 articles in peer reviewed journals. Abraham is an active contributor to many professional associations and initiatives in Ethiopia and Africa. He has shown particular interest in capacity building efforts of health research manpower and ethics.<br></div>', '2007-01-23', '2014-11-11', 'active'),
(4, 81, 'Mr.', '', '<h2>Biography</h2><div>I believe artificial intelligent systems can solve big\r\nproblems of our world by pushing the boundaries of technology. We are living in\r\nthe information age. There is too much information in our world today ranging\r\nfrom our daily mundane activities like our status on facebook, our browsing\r\nhistory or even the route we take to reach to work to cutting edge scientific\r\ndiscoveries like Large Hardon Colider particle accelerator.&nbsp; Living in the age of information abundance\r\nsomeone might ask why we humans could not find solutions for our prominent\r\nproblems like sustainable energy resource, cure for cancer or even cure for HIV.\r\nI say it is not information we luck in these disciplines, but what we luck is\r\nsomeone who will understand and decipher the multitude disciplinary solutions\r\nfor the problems. I believe the solutions for these humanity greatest\r\nchallenges exist today scattered in the researches, raw data and scientific\r\ndiscoveries from multitude disciplines, but one first need to engage himself in\r\nall researches, data and discoveries from all the necessary disciplines because\r\nOnly Then one can figure out the connections and have solution. Unfortunately a\r\nhundred life time will not be enough for someone just to go through the\r\ninformation available today. The other alternative is to wait for generations\r\nbecause as each generation’s passes by the next generation becomes more\r\ninformed, smarter and at some point they may be able to solve our world’s\r\ndilemma. &nbsp;Or we can just pull our\r\nresources gather minds of programmers, engineers, designers, physiologists,\r\ndoctors and many more to make epic battle in perfecting AI systems. Then our\r\nbots will do the bulky work for us and inform us or even teach us the things\r\nthat matter. They can make the learning curve we humans face as we are\r\nintroduced to new information so gentle that t it would seem to come naturally.\r\nImagine if all of us have such intelligent agents on our mobile devices,\r\ncomputers in our business infrastructures, in our school there would be\r\nexponential growth all over the world.</div><p></p>\r\n\r\n<p>My goal is to create a team from different disciplines dedicated\r\nto advancing the frontier of artificial intelligence research and applications.\r\nIn time I plan to build an intelligent, elegant and engaging electronic educational\r\nsystem that can adopt to anyone’s need, mood and capability. Therefor I plan to\r\nhave excellent expertise in intelligent systems and robotics. I also intend to\r\nhave deep understanding of computer and mobile application development.</p>', '2007-01-23', '2014-11-11', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `downloads`
--

CREATE TABLE IF NOT EXISTS `downloads` (
  `download_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) DEFAULT NULL,
  `download_url` varchar(255) DEFAULT NULL,
  `synapse` text,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`download_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE IF NOT EXISTS `events` (
  `event_id` int(11) NOT NULL AUTO_INCREMENT,
  `event_title` varchar(255) DEFAULT NULL,
  `event_content` text,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`event_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`event_id`, `event_title`, `event_content`, `created`, `modified`) VALUES
(2, 'cleaning routine descission', 'Here we pin the best cleaning routines and techniques we can find on the web - anything to make cleaning easier and more efficient. Keep other pinners informed and let the great ideas and tips flow', '2014-10-20 07:10:09', '2014-10-20 07:10:09'),
(3, 'cleaning routine descission', 'Here we pin the best cleaning routines and techniques we can find on the web - anything to make cleaning easier and more efficient. Keep other pinners informed and let the great ideas and tips flow', '2014-10-20 07:11:13', '2014-10-20 07:11:13'),
(4, 'cleaning routine descission', 'Here we pin the best cleaning routines and techniques we can find on the web - anything to make cleaning easier and more efficient. Keep other pinners informed and let the great ideas and tips flow', '2014-10-20 07:11:49', '2014-10-20 07:11:49'),
(5, 'cleaning routine descission', 'Here we pin the best cleaning routines and techniques we can find on the web - anything to make cleaning easier and more efficient. Keep other pinners informed and let the great ideas and tips flow', '2014-10-20 07:12:28', '2014-10-20 07:12:28'),
(6, 'cleaning routine descission', 'Here we pin the best cleaning routines and techniques we can find on the web - anything to make cleaning easier and more efficient. Keep other spinner informed and let the great ideas and tips flow of the torrent', '2014-11-08 06:37:49', '2014-11-08 06:37:49'),
(8, 'cleaning routine descission', 'Here we pin the best cleaning routines and techniques we can find on the web - anything to make cleaning easier and more efficient. Keep other spinners informed and let the great ideas and tips flow of the torrent', '0000-00-00 00:00:00', '2014-11-08 06:38:53');

-- --------------------------------------------------------

--
-- Table structure for table `galleries`
--

CREATE TABLE IF NOT EXISTS `galleries` (
  `gallery_id` int(11) NOT NULL AUTO_INCREMENT,
  `gallery_title` varchar(100) DEFAULT NULL,
  `description` text,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`gallery_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `galleries`
--

INSERT INTO `galleries` (`gallery_id`, `gallery_title`, `description`, `created`, `modified`) VALUES
(2, 'Hawassa Langano leshirisir', 'Men those was good times', '2014-10-23 08:39:49', '2014-10-23 08:39:49'),
(4, 'Dre dewa Langano leshirisir', 'Men those was good times', '2014-10-23 08:43:36', '2014-10-23 08:43:36');

-- --------------------------------------------------------

--
-- Table structure for table `laboratories`
--

CREATE TABLE IF NOT EXISTS `laboratories` (
  `laboratory_id` int(11) NOT NULL AUTO_INCREMENT,
  `laboratory_name` varchar(100) DEFAULT NULL,
  `profile_img` varchar(255) DEFAULT NULL,
  `description` longtext,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`laboratory_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

--
-- Dumping data for table `laboratories`
--

INSERT INTO `laboratories` (`laboratory_id`, `laboratory_name`, `profile_img`, `description`, `created`, `modified`) VALUES
(15, 'Virology and bac', 'http://ahri.gov/resource/img/lab/15.jpg', 'this is cool of the amizing and this should change<br>this should end better', NULL, NULL),
(16, 'Isolation Laboratory', 'http://ahri.gov/resource/img/lab/16.jpg', 'standing frozen in the life of chosen you wont find me the past is so behind me burred in the snow', NULL, NULL),
(18, 'Photon Gun Laboratory', 'http://ahri.gov/resource/img/lab/18.jpg', 'it funny how some distance makes everything seem small and the fears that once control me can''t get me at all up here in the cold air I finally can breath I know I left a life behind, but I am to relieved to breath', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `laboratory_images`
--

CREATE TABLE IF NOT EXISTS `laboratory_images` (
  `img_id` int(11) NOT NULL AUTO_INCREMENT,
  `laboratory_id` int(11) DEFAULT NULL,
  `img_url` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`img_id`),
  KEY `laboratory_id` (`laboratory_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `laboratory_staffs`
--

CREATE TABLE IF NOT EXISTS `laboratory_staffs` (
  `lab_staff_id` int(11) NOT NULL AUTO_INCREMENT,
  `position` varchar(100) DEFAULT NULL,
  `staff_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`lab_staff_id`),
  KEY `staff_id` (`staff_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `laboratory_staffs`
--

INSERT INTO `laboratory_staffs` (`lab_staff_id`, `position`, `staff_id`) VALUES
(2, 'Lab Tech', 80);

-- --------------------------------------------------------

--
-- Table structure for table `mailing_list`
--

CREATE TABLE IF NOT EXISTS `mailing_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(200) DEFAULT NULL,
  `ip_address` varchar(30) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `status` enum('active','inactive') DEFAULT 'active',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `mailing_list`
--

INSERT INTO `mailing_list` (`id`, `email`, `ip_address`, `created`, `status`) VALUES
(1, 'dagisky@gmail.com', '127.0.0.1', '2014-11-18 02:17:03', 'active'),
(2, 'foo@gmail.com', '127.0.0.1', '2014-11-18 02:18:24', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE IF NOT EXISTS `news` (
  `news_id` int(11) NOT NULL AUTO_INCREMENT,
  `news_title` varchar(255) DEFAULT NULL,
  `news_content` text,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`news_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`news_id`, `news_title`, `news_content`, `created`, `modified`) VALUES
(2, 'Up Coming Conference ', 'Windows, Linux: server and client System Support and Maintenance\r\nSoftware Installation, Updating and Management (System Optimization of System Software and Application software) \r\nSystem Security (installation and Updating Antivirus, Manual Detection and Removal of Malicious software’s)\r\nNetwork Installation and Maintenance\r\nComputer Hardware installation Maintenance \r\n', '2014-10-13 11:12:00', '2014-10-13 07:07:24'),
(3, 'Sweat-Eating Bacteria: Acne Miracle Cure?', '<span><br><img alt="" src="http://static.ddmcdn.com/gif/1-human-skin.jpg"><br>The latest front in combating acne breakouts could be in bacteria that feed on your sweat.<br><br></span>Ammonia-oxideizing bacteria (AOB) digest ammonia, which is a major component of sweat. A new, small study has shown that applying a topical creme containing the bacteria leads to healthier skin and could be used to treat acne and promote healing in wounds.<span><br><br></span><h3><a href="http://news.discovery.com/human/health/human-skins-nine-most-amazing-features-140711.htm" target="" rel="">Human Skin''s Nine Most Amazing Features</a><br></h3>In the study, researchers isolated a strain of&nbsp;<i>Nitrosomonas eutropha</i><span>&nbsp;from organic soil samples. They then had a group of volunteers apply the bacteria to their skin for three weeks while another group used a placebo. The 24 volunteers were asked not to use hair products for the first two weeks and then returned to their normal hair routines for the third week.<br>Ok now this is cool!<br><br></span><br><br><br>', '0000-00-00 00:00:00', '2014-11-08 05:57:17'),
(5, 'Sweat-Eating Bacteria: Acne Miracle Cure?', '<span><br><img alt="" src="http://static.ddmcdn.com/gif/1-human-skin.jpg"><br>The latest front in combating acne breakouts could be in bacteria that feed on your sweat.<br><br></span>Ammonia-oxideizing bacteria (AOB) digest ammonia, which is a major component of sweat. A new, small study has shown that applying a topical creme containing the bacteria leads to healthier skin and could be used to treat acne and promote healing in wounds.<span><br><br></span><h3><a href="http://news.discovery.com/human/health/human-skins-nine-most-amazing-features-140711.htm" target="" rel="">Human Skin''s Nine Most Amazing Features</a><br></h3>In the study, researchers isolated a strain of&nbsp;<i>Nitrosomonas eutropha</i><span>&nbsp;from organic soil samples. They then had a group of volunteers apply the bacteria to their skin for three weeks while another group used a placebo. The 24 volunteers were asked not to use hair products for the first two weeks and then returned to their normal hair routines for the third week. ok men<br></span><br><br><br>', '2014-11-08 05:55:19', '2014-11-08 05:55:19');

-- --------------------------------------------------------

--
-- Table structure for table `other_staff`
--

CREATE TABLE IF NOT EXISTS `other_staff` (
  `other_staff_id` int(11) NOT NULL AUTO_INCREMENT,
  `Position` varchar(100) DEFAULT NULL,
  `staff_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`other_staff_id`),
  KEY `staff_id` (`staff_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `other_staff`
--

INSERT INTO `other_staff` (`other_staff_id`, `Position`, `staff_id`) VALUES
(1, 'Inspiration Singer', 65),
(2, 'Inspiration Singer', 66),
(5, 'Inspiration Singer', 69),
(6, 'Inspiration Singer', 70),
(7, 'Inspiration Singer', 71);

-- --------------------------------------------------------

--
-- Table structure for table `photos`
--

CREATE TABLE IF NOT EXISTS `photos` (
  `photo_id` int(11) NOT NULL AUTO_INCREMENT,
  `photo_url` varchar(255) DEFAULT NULL,
  `photo_thumb` varchar(255) DEFAULT NULL,
  `gallery_id` int(11) DEFAULT NULL,
  `synapse` text,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`photo_id`),
  KEY `gallery_id` (`gallery_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=72 ;

--
-- Dumping data for table `photos`
--

INSERT INTO `photos` (`photo_id`, `photo_url`, `photo_thumb`, `gallery_id`, `synapse`, `created`) VALUES
(56, 'http://ahri.gov/resource/img/gallery/2_56.jpg', 'http://ahri.gov/resource/img/gallery/thumb-2_56.jpg', 2, '                            ', NULL),
(58, 'http://ahri.gov/resource/img/gallery/2_58.jpg', 'http://ahri.gov/resource/img/gallery/thumb-2_58.jpg', 2, '                            ', NULL),
(68, 'http://ahri.gov/resource/img/gallery/4_68.jpg', 'http://ahri.gov/resource/img/gallery/thumb-4_68.jpg', 4, '                            ', NULL),
(70, 'http://ahri.gov/resource/img/gallery/4_70.jpg', 'http://ahri.gov/resource/img/gallery/thumb-4_70.jpg', 4, '                            ', NULL),
(71, 'http://ahri.gov/resource/img/gallery/4_71.jpg', 'http://ahri.gov/resource/img/gallery/thumb-4_71.jpg', 4, NULL, '2014-12-11 11:42:30');

-- --------------------------------------------------------

--
-- Table structure for table `publications`
--

CREATE TABLE IF NOT EXISTS `publications` (
  `publication_id` int(11) NOT NULL AUTO_INCREMENT,
  `publication_title` varchar(255) NOT NULL,
  `publication_synapse` text,
  `publication_url` varchar(255) DEFAULT NULL,
  `published_date` date DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`publication_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `publications`
--

INSERT INTO `publications` (`publication_id`, `publication_title`, `publication_synapse`, `publication_url`, `published_date`, `created`, `modified`) VALUES
(1, 'The burden of neglected tropical diseases in Ethiopia, and opportunities for integrated control and elimination', 'pub_synapse God Please help', 'http://www.ncbi.nlm.nih.gov/pubmed/23095679', '2012-10-02', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `researchers`
--

CREATE TABLE IF NOT EXISTS `researchers` (
  `researcher_id` int(11) NOT NULL AUTO_INCREMENT,
  `researcher_title` varchar(5) DEFAULT NULL,
  `staff_id` int(11) DEFAULT NULL,
  `researcher_level` enum('Senior Scientist','Postdoctoral Scientist','Researcher','Assistant Researcher','Research Assistant') DEFAULT NULL,
  `edu_level` varchar(255) DEFAULT NULL,
  `biography` longtext,
  PRIMARY KEY (`researcher_id`),
  KEY `staff_id` (`staff_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `researchers`
--

INSERT INTO `researchers` (`researcher_id`, `researcher_title`, `staff_id`, `researcher_level`, `edu_level`, `biography`) VALUES
(1, NULL, 1, NULL, NULL, NULL),
(2, NULL, 2, NULL, NULL, NULL),
(3, NULL, 4, 'Research Assistant', NULL, NULL),
(5, 'Dr', 6, 'Postdoctoral Scientist', NULL, NULL),
(11, 'Dr.', 78, 'Assistant Researcher', 'Phd in Dental Science', 'Give me your teeth&nbsp;'),
(12, 'Prof.', 79, 'Assistant Researcher', 'Researcher', 'this is cool'),
(14, 'Dr.', 95, 'Senior Scientist', 'MD, PhD', '<h2>Biography</h2><div><p>Rawleigh Howe received a BSc degree in Biology from Brown University in 1976. He worked as a research assistant in an immunology lab at University of Massachusetts Medical School, before enrolling at Washington University, St. Louis, U.S. where he received his PhD in Immunology in 1984. He studied in the laboratory of JH Russell, and developed a model of Cytotoxic T Lymphocyte induction using murine CTL clones. He worked as a post-doctoral fellow at the Ludwig Institute for Cancer Research in Lausanne, Switzerland in the laboratory of H. Robson MacDonald, and studied cellular induction properties of immature thymocytes in a murine model. He then took a position as Senior Scientist at the Armauer Hansen Research Institute in Addis Ababa, Ethiopia from 1989-1994, and also served as Institute Deputy director and adjunct Assistant professor at Addis Ababa University, Department of Microbiology. While at AHRI his major focus was on the cytokine profiles of T cells from Leprosy skin and nerve lesions and peripheral blood, using in vitro and clonal models in man.</p><p>He then studied medicine from 1994-1998, receiving the MD degree from Wayne State University in Michigan, U.S. He completed an Internal Medicine residency at the Mayo Clinic College of Medicine, U.S., receiving board certification in 2001. He also completed a fellowship in Clinical Pharmacology at the Mayo Clinic in 2004, during which he studied T cell cytokine responses to Measles Virus in man. Following a stint practicing internal medicine at the Albert Schweitzer Hospital in Deschapelles, Haiti in 2005, he went to the University of Colorado Health Science Center in Denver, U.S. where he undertook a clinical fellowship in Infectious Diseases, receiving board certification in 2008. During his stay at Denver, he worked in the laboratory of Cara Wilson, studying T cell responses in HIV patients.</p><p>He returned to AHRI in 2008. He has been the AHRI Principal Investigator for two multinational consortium projects on Tuberculosis and Biomarkers, has mentored many students in research fields of HIV and Tuberculosis, and is actively working with the Federal Ministry of Health as a technical consultant for a number of clinical research and capacity building initiatives.&nbsp;</p><h3>Name of Students he supervised</h3></div><ul><li>Girmaye Desalegn (Master’s student)</li><li>Melat Gebru (Master’s student)</li><li>Mikias Negash (Master’s student)</li><li>Melaku Adal (Doctoral student)</li><li>Fekadu Desta (Doctoral student)</li><li>Yonas Bekele (Doctoral student)</li><li>Mekdes Meaza (Master’s student)</li><li>Mary van der Waal (Doctoral student)</li><li>Yodit Alemayehu</li><li>Wegene Tamane (outside of AHRI)</li></ul><h1>Selected Publication</h1><p>Total of 48 listed in Pubmed with an average journal impact factor of 5.3</p><p><span><span>1.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>Howe, R, Dillon, S, Rogers, L, Palmer, B, MaWhinney, S, Blyveis, N, Schlichtemeier, R, D’Souza, M, Ingoldby, L, Harwood, J, Rietmeijer, C PhD, MSPH, Ray, G, Connick, L, and Wilson, C. Phenotypic and Functional Characterization of HIV-1–Speci?c CD4+CD8+ Double-Positive T Cells in Early and Chronic HIV-1 Infection. J Acquir Immune De?c Syndr 50:444. 2009</span></p><p><span><span>2.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><span>Howe R, Dillon S, Rogers L, McCarter M, Kelly C, Gonzalez R, Madinger N, Wilson CC.&nbsp;<a href="http://www.ncbi.nlm.nih.gov/pubmed/19174326?ordinalpos=5&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum" target="" rel="">Evidence for dendritic cell-dependent CD4 (+) T helper-1 type responses to commensal bacteria in normal human intestinal lamina propria.</a>&nbsp;Clin Immunol.131:317. 2009</span></span></p><p><span><span>3.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><span>Howe RC, Ovsyannikova IG, Pinsky NA, Poland GA..&nbsp;<a href="http://www.ncbi.nlm.nih.gov/pubmed/15694995?ordinalpos=66&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum" target="" rel="">Identification of Th0 cells responding to measles virus.</a>&nbsp;Hum Immunol. 66:104-15. 2005.</span></span></p><p><span><span>4.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><span>Howe R, Micallef IN, Inwards DJ, Ansell SM, Dewald GW, Dispenzieri A, Gastineau DA, Gertz MA, Geyer SM, Hanson CA, Lacy MQ, Tefferi A, Litzow MR.&nbsp;<a href="http://www.ncbi.nlm.nih.gov/pubmed/12858205?ordinalpos=3&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum" target="" rel="">Secondary myelodysplastic syndrome and acute myelogenous leukemia are significant complications following autologous stem cell transplantation for lymphoma.</a>&nbsp; Bone Marrow Transplant. 32: 317-24. 2003.</span></span></p><p><span><span>5.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>Mesret Y. Reed AH. Howe RC. Proliferative responses of T cells from the skin and nerve lesions of leprosy patients. Clinical Immunology &amp; Immunopathology. 77(3):243-52, 1995.</span></p><p><span><span>6.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>Howe RC. Wondimu A. Demissee A. Frommel D. Functional heterogeneity among CD4+ T-cell clones from blood and skin lesions of leprosy patients. Identification of T-cell clones distinct from ThO, Thl and Th2. Immunology. 84(4):585-94, 1995.</span></p><p><span><span>7.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>Akuffo H. Maasho K. Howe R. Natural and acquired resistance to Leishrnania: cellular activation by Leishmania aethiopica of mononuclear cells from unexposed individuals is through the stimulation of natural killer (NK) cells. Clinical &amp; Experimental Immunology. 94(3):516-21, 1993</span></p><p><span><span>8.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>Howe RC. MacDonald HR Clonogenic potential of murine CD4+8+ thymocytes. Direct demonstration using a V beta 6-specific proliferative stimulus in Mlsa mice. Journal of Immunology. 143(3):793-7, 1989</span></p><p><span><span>9.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>Howe RC. Pedrazzini T. MacDonald HR.Functional responsiveness in vitro and in vivo of alpha/beta T cell receptors expressed by the B2A2 (J11d)- subset of CD4-8- thymocytes. European Journal of Immunology. 19(l):25-30, 1989</span></p><p><span><span>10.&nbsp;&nbsp;</span>MacDonald HR. Schneider R. Lees RK. Howe RC. Acha-Orbea H. Festenstein H. Zinkemagel RM. Hengartner H.&nbsp; T-cell receptor V beta use predicts reactivity and tolerance to Mlsa-encoded antigens. Nature. 332(6159):40-5, 1988</span></p><p><span><span>11.&nbsp;&nbsp;</span>Howe RC. MacDonald HR. Heterogeneity of immature (Lyt-2-/L3T4-) thymocytes. Identification of four major phenotypically distinct subsets differing in cell cycle status and in vitro activation requirements. Journal of Immunology. 140(4):1047-55, 1988.</span></p><p><span><span>12.&nbsp;&nbsp;</span>Howe RC. Lowenthal JW. MacDonald HR.&nbsp; Role of interleukin 1 in early T cell development: Lyt-2-L3T4- thymocytes bind and respond in vitro to recombinant IL 1 Journal of Immunology. 137(10):3195-200, 1986</span></p><p><span><span>13.&nbsp;&nbsp;</span>Howe RC. Russell JH. Isolation of alloreactive CTL clones with cyclical changes in lytic activity. Journal of Immunology. 131(5):2141-6, 1983&nbsp;</span></p><h1><span>&nbsp;<a target="" rel="">Contact Information</a></span></h1><p>Rawleigh Howe</p><p>Box 1005</p><p>Armauer Hansen Research Institute</p><p>Addis Ababa, Ethiopia</p><p><span>Phone 910-865382; email:&nbsp;<a target="" rel=""></a><a target="" rel="">rawcraig@yahoo.com</a></span></p><p>&nbsp;</p>'),
(15, 'Mr.', 96, 'Assistant Researcher', 'MD, PhD', '<h2>Biography</h2><div><p>Rawleigh Howe received a BSc degree in Biology from Brown University in 1976. He worked as a research assistant in an immunology lab at University of Massachusetts Medical School, before enrolling at Washington University, St. Louis, U.S. where he received his PhD in Immunology in 1984. He studied in the laboratory of JH Russell, and developed a model of Cytotoxic T Lymphocyte induction using murine CTL clones. He worked as a post-doctoral fellow at the Ludwig Institute for Cancer Research in Lausanne, Switzerland in the laboratory of H. Robson MacDonald, and studied cellular induction properties of immature thymocytes in a murine model. He then took a position as Senior Scientist at the Armauer Hansen Research Institute in Addis Ababa, Ethiopia from 1989-1994, and also served as Institute Deputy director and adjunct Assistant professor at Addis Ababa University, Department of Microbiology. While at AHRI his major focus was on the cytokine profiles of T cells from Leprosy skin and nerve lesions and peripheral blood, using in vitro and clonal models in man.</p><p>He then studied medicine from 1994-1998, receiving the MD degree from Wayne State University in Michigan, U.S. He completed an Internal Medicine residency at the Mayo Clinic College of Medicine, U.S., receiving board certification in 2001. He also completed a fellowship in Clinical Pharmacology at the Mayo Clinic in 2004, during which he studied T cell cytokine responses to Measles Virus in man. Following a stint practicing internal medicine at the Albert Schweitzer Hospital in Deschapelles, Haiti in 2005, he went to the University of Colorado Health Science Center in Denver, U.S. where he undertook a clinical fellowship in Infectious Diseases, receiving board certification in 2008. During his stay at Denver, he worked in the laboratory of Cara Wilson, studying T cell responses in HIV patients.</p><p>He returned to AHRI in 2008. He has been the AHRI Principal Investigator for two multinational consortium projects on Tuberculosis and Biomarkers, has mentored many students in research fields of HIV and Tuberculosis, and is actively working with the Federal Ministry of Health as a technical consultant for a number of clinical research and capacity building initiatives.&nbsp;</p><h3>Name of Students he supervised</h3></div><ul><li>Girmaye Desalegn (Master’s student)</li><li>Melat Gebru (Master’s student)</li><li>Mikias Negash (Master’s student)</li><li>Melaku Adal (Doctoral student)</li><li>Fekadu Desta (Doctoral student)</li><li>Yonas Bekele (Doctoral student)</li><li>Mekdes Meaza (Master’s student)</li><li>Mary van der Waal (Doctoral student)</li><li>Yodit Alemayehu</li><li>Wegene Tamane (outside of AHRI)</li></ul><h1>Selected Publication</h1><p>Total of 48 listed in Pubmed with an average journal impact factor of 5.3</p><p><span><span>1.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>Howe, R, Dillon, S, Rogers, L, Palmer, B, MaWhinney, S, Blyveis, N, Schlichtemeier, R, D’Souza, M, Ingoldby, L, Harwood, J, Rietmeijer, C PhD, MSPH, Ray, G, Connick, L, and Wilson, C. Phenotypic and Functional Characterization of HIV-1–Speci?c CD4+CD8+ Double-Positive T Cells in Early and Chronic HIV-1 Infection. J Acquir Immune De?c Syndr 50:444. 2009</span></p><p><span><span>2.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><span>Howe R, Dillon S, Rogers L, McCarter M, Kelly C, Gonzalez R, Madinger N, Wilson CC.&nbsp;<a href="http://www.ncbi.nlm.nih.gov/pubmed/19174326?ordinalpos=5&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum" target="" rel="">Evidence for dendritic cell-dependent CD4 (+) T helper-1 type responses to commensal bacteria in normal human intestinal lamina propria.</a>&nbsp;Clin Immunol.131:317. 2009</span></span></p><p><span><span>3.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><span>Howe RC, Ovsyannikova IG, Pinsky NA, Poland GA..&nbsp;<a href="http://www.ncbi.nlm.nih.gov/pubmed/15694995?ordinalpos=66&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum" target="" rel="">Identification of Th0 cells responding to measles virus.</a>&nbsp;Hum Immunol. 66:104-15. 2005.</span></span></p><p><span><span>4.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><span>Howe R, Micallef IN, Inwards DJ, Ansell SM, Dewald GW, Dispenzieri A, Gastineau DA, Gertz MA, Geyer SM, Hanson CA, Lacy MQ, Tefferi A, Litzow MR.&nbsp;<a href="http://www.ncbi.nlm.nih.gov/pubmed/12858205?ordinalpos=3&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum" target="" rel="">Secondary myelodysplastic syndrome and acute myelogenous leukemia are significant complications following autologous stem cell transplantation for lymphoma.</a>&nbsp; Bone Marrow Transplant. 32: 317-24. 2003.</span></span></p><p><span><span>5.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>Mesret Y. Reed AH. Howe RC. Proliferative responses of T cells from the skin and nerve lesions of leprosy patients. Clinical Immunology &amp; Immunopathology. 77(3):243-52, 1995.</span></p><p><span><span>6.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>Howe RC. Wondimu A. Demissee A. Frommel D. Functional heterogeneity among CD4+ T-cell clones from blood and skin lesions of leprosy patients. Identification of T-cell clones distinct from ThO, Thl and Th2. Immunology. 84(4):585-94, 1995.</span></p><p><span><span>7.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>Akuffo H. Maasho K. Howe R. Natural and acquired resistance to Leishrnania: cellular activation by Leishmania aethiopica of mononuclear cells from unexposed individuals is through the stimulation of natural killer (NK) cells. Clinical &amp; Experimental Immunology. 94(3):516-21, 1993</span></p><p><span><span>8.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>Howe RC. MacDonald HR Clonogenic potential of murine CD4+8+ thymocytes. Direct demonstration using a V beta 6-specific proliferative stimulus in Mlsa mice. Journal of Immunology. 143(3):793-7, 1989</span></p><p><span><span>9.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>Howe RC. Pedrazzini T. MacDonald HR.Functional responsiveness in vitro and in vivo of alpha/beta T cell receptors expressed by the B2A2 (J11d)- subset of CD4-8- thymocytes. European Journal of Immunology. 19(l):25-30, 1989</span></p><p><span><span>10.&nbsp;&nbsp;</span>MacDonald HR. Schneider R. Lees RK. Howe RC. Acha-Orbea H. Festenstein H. Zinkemagel RM. Hengartner H.&nbsp; T-cell receptor V beta use predicts reactivity and tolerance to Mlsa-encoded antigens. Nature. 332(6159):40-5, 1988</span></p><p><span><span>11.&nbsp;&nbsp;</span>Howe RC. MacDonald HR. Heterogeneity of immature (Lyt-2-/L3T4-) thymocytes. Identification of four major phenotypically distinct subsets differing in cell cycle status and in vitro activation requirements. Journal of Immunology. 140(4):1047-55, 1988.</span></p><p><span><span>12.&nbsp;&nbsp;</span>Howe RC. Lowenthal JW. MacDonald HR.&nbsp; Role of interleukin 1 in early T cell development: Lyt-2-L3T4- thymocytes bind and respond in vitro to recombinant IL 1 Journal of Immunology. 137(10):3195-200, 1986</span></p><p><span><span>13.&nbsp;&nbsp;</span>Howe RC. Russell JH. Isolation of alloreactive CTL clones with cyclical changes in lytic activity. Journal of Immunology. 131(5):2141-6, 1983&nbsp;</span></p><h1><span>&nbsp;<a target="" rel="">Contact Information</a></span></h1><p>Rawleigh Howe</p><p>Box 1005</p><p>Armauer Hansen Research Institute</p><p>Addis Ababa, Ethiopia</p><p><span>Phone 910-865382; email:&nbsp;<a target="" rel=""></a><a target="" rel="">rawcraig@yahoo.com</a></span></p><p>&nbsp;</p>');

-- --------------------------------------------------------

--
-- Table structure for table `researchs`
--

CREATE TABLE IF NOT EXISTS `researchs` (
  `research_id` int(11) NOT NULL AUTO_INCREMENT,
  `research_title` varchar(255) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `description` text,
  `inititation_date` date DEFAULT NULL,
  `due_date` date DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`research_id`),
  KEY `category_id` (`category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `researchs`
--

INSERT INTO `researchs` (`research_id`, `research_title`, `category_id`, `description`, `inititation_date`, `due_date`, `created`, `modified`) VALUES
(1, 'The burden of neglected tropical diseases in EthiopiaThe burden of neglected tropical diseases in Ethiopia', NULL, 'Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid.\r\n                3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt \r\n                laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin \r\n                coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes \r\n                anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings \r\n                occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven''t \r\n                heard of them accusamus labore sustainable VHS.', '2014-04-09', '2015-05-05', NULL, NULL),
(2, 'Surveillance of bacterial meningitis', 1, 'Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven''t heard of them accusamus labore sustainable VHS.', '2006-06-05', '2014-10-08', NULL, NULL),
(3, 'Nosocomial infections in Ethiopian hospitals', NULL, 'Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven''t heard of them accusamus labore sustainable VHS.', '2014-03-02', '2014-10-23', NULL, NULL),
(4, 'High prevalence of Cryptococcal antigenemia among HIV-infected patients receiving antiretroviral therapy in Ethiopia', 2, 'good work! this is awesome', '2014-02-11', '2014-12-16', NULL, NULL),
(5, 'Transmission of Mycobacterium tuberculosis between farmers and cattle in central Ethiopia', 1, 'all good', '0000-00-00', '0000-00-00', NULL, NULL),
(6, 'Advances in tuberculosis', 1, 'These Scholarships are awarded to international students with a degree equivalent to an Australian Bachelor’s Degree with Honours in Science (or a relevant discipline) which includes an independent research project and who ranked in the top 10% of the cohort\r\n\r\nRead more: Excellence Awards at University of Sydney in Australia, 2015 Scholarship Positions 2014 2015 \r\nhttp://scholarship-positions.com', '0000-00-00', '0000-00-00', NULL, NULL),
(7, 'Population-based prevalence survey of tuberculosis in the Tigray region of Ethiopia', 2, 'The Faculty of Science offers a number of Postgraduate Research Excellence Awards. These are full-time scholarships for exceptional international students from world leading research institutions wishing to undertake a research doctorate degree in the Faculty.\r\n\r\nRead more: Excellence Awards at University of Sydney in Australia, 2015 Scholarship Positions 2014 2015 \r\nhttp://scholarship-positions.com', '2014-10-21', '2014-11-12', NULL, NULL),
(9, 'Cell Regeneration', 2, 'cool effects', '2009-06-16', '2014-11-11', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `research_categories`
--

CREATE TABLE IF NOT EXISTS `research_categories` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_title` varchar(255) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `research_categories`
--

INSERT INTO `research_categories` (`category_id`, `category_title`, `created`, `modified`) VALUES
(1, 'Bacterial', '2014-04-07 00:00:00', '2015-03-10 00:00:00'),
(2, 'Viral', '2014-02-02 00:00:00', '2015-02-05 00:00:00'),
(3, 'Other', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `staffs`
--

CREATE TABLE IF NOT EXISTS `staffs` (
  `staff_id` int(11) NOT NULL AUTO_INCREMENT,
  `fname` varchar(30) DEFAULT NULL,
  `lname` varchar(30) DEFAULT NULL,
  `sex` enum('Male','Female') DEFAULT 'Male',
  `profile_img` varchar(255) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `phone1` varchar(25) DEFAULT NULL,
  `phone2` varchar(25) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `staff_type` enum('Researcher','Data_Statistics','Laboratory','Administrator','Director','Other') DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`staff_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=97 ;

--
-- Dumping data for table `staffs`
--

INSERT INTO `staffs` (`staff_id`, `fname`, `lname`, `sex`, `profile_img`, `email`, `phone1`, `phone2`, `dob`, `staff_type`, `created`, `modified`) VALUES
(1, 'Wude ', 'Mehrit', 'Female', NULL, 'wudemjes@gmail.com', NULL, NULL, '2014-10-14', NULL, NULL, NULL),
(2, 'Seble', 'Wongel', 'Female', NULL, 'bondem@gmail.com', NULL, NULL, '2014-10-21', NULL, NULL, NULL),
(3, 'Dagmawi', 'Moges', 'Male', NULL, 'dagisky@gmail.com', NULL, NULL, '1991-08-05', NULL, '2014-10-11 00:00:00', '2014-10-11 02:36:09'),
(4, 'Yonas ', 'Bekele', 'Male', NULL, 'yonas@gmail.com', NULL, NULL, '1994-08-09', NULL, '2014-10-11 09:43:00', '2014-10-11 00:00:00'),
(6, 'Adane', 'Mihret', 'Male', NULL, 'adane@gmail.com', NULL, NULL, '1984-10-09', NULL, '2014-10-11 00:00:00', '2014-10-11 00:00:00'),
(35, 'Tsegaye', 'Hailu', 'Male', 'http://ahri.gov/resource/img/Tsegaye_35.jpg', 'tsegsha2@gmail.com', NULL, NULL, '0000-00-00', NULL, NULL, NULL),
(36, 'Sami', 'Ayele', 'Male', 'http://ahri.gov/resource/img/Sami_36.jpg', 'sami@gmail.com', NULL, NULL, '0000-00-00', NULL, NULL, NULL),
(37, 'Sami', 'Ayele', 'Male', 'http://ahri.gov/resource/img/Sami_37.jpg', 'sami@gmail.com', NULL, NULL, '0000-00-00', NULL, NULL, NULL),
(38, 'Dagmawi', 'Moges', 'Male', 'http://ahri.gov/resource/img/Dagmawi_38.jpg', 'dagisky@gmail.com', NULL, NULL, '0000-00-00', NULL, NULL, NULL),
(43, 'Abraham', 'Aseffa', 'Male', 'http://ahri.gov/resource/img/director/Dagmawi_431.jpg', 'dagisky@gmail.com', '251922428411', NULL, '1960-02-16', 'Director', '2014-12-08 11:50:21', '2014-12-08 11:50:21'),
(44, 'Wongelawit', 'Moges', 'Female', 'http://ahri.gov/resource/img/user/Wongelawit_44.jpg', 'wongeljes@gmail.com', NULL, NULL, '0000-00-00', NULL, NULL, NULL),
(45, 'Masresha', 'foo', 'Male', 'http://ahri.gov/resource/img/user/Masresha_45.jpg', 'foo@gmail.com', NULL, NULL, '0000-00-00', NULL, NULL, NULL),
(46, 'Masresha', 'foo', 'Male', NULL, 'foo@gmail.com', NULL, NULL, '0000-00-00', NULL, NULL, NULL),
(47, 'Masresha', 'foo', 'Male', NULL, 'foo@gmail.com', NULL, NULL, '0000-00-00', NULL, NULL, NULL),
(48, 'Masresha', 'foo', 'Male', NULL, 'foo@gmail.com', NULL, NULL, '0000-00-00', NULL, NULL, NULL),
(49, 'Masresha', 'foo', 'Male', NULL, 'foo@gmail.com', NULL, NULL, '0000-00-00', NULL, NULL, NULL),
(50, 'Dagmawi', 'Moges', 'Male', NULL, 'dagmjes@yahoo.com', NULL, NULL, '0000-00-00', NULL, NULL, NULL),
(51, 'Dagmawi', 'Moges', 'Male', NULL, 'dagmjes@yahoo.com', NULL, NULL, '0000-00-00', NULL, NULL, NULL),
(52, 'Dagmawi', 'Moges', 'Male', NULL, 'dagmjes@yahoo.com', NULL, NULL, '0000-00-00', NULL, NULL, NULL),
(53, 'Dagmawi', 'Moges', 'Male', NULL, 'dagmjes@yahoo.com', NULL, NULL, '0000-00-00', NULL, NULL, NULL),
(54, 'Dagmawi', 'Moges', 'Male', NULL, 'dagmjes@yahoo.com', NULL, NULL, '0000-00-00', NULL, NULL, NULL),
(55, 'Dagmawi', 'Moges', 'Male', NULL, 'dagisky@gmail.com', NULL, NULL, '0000-00-00', NULL, NULL, NULL),
(56, 'Dagmawi', 'Moges', 'Male', 'http://ahri.gov/resource/img/user/Dagmawi_.jpg', 'dagisky@gmail.com', NULL, NULL, '0000-00-00', NULL, NULL, NULL),
(57, 'Dagmawi', 'Moges', 'Male', 'http://ahri.gov/resource/img/user/Dagmawi_.jpg', 'dagmjes@yahoo.com', NULL, NULL, '0000-00-00', NULL, NULL, NULL),
(58, 'Dagmawi', 'Moges', 'Male', NULL, 'dagmjes@yahoo.com', NULL, NULL, '0000-00-00', NULL, NULL, NULL),
(59, 'Dagmawi', 'Moges', 'Male', NULL, 'dagmjes@yahoo.com', NULL, NULL, '0000-00-00', NULL, NULL, NULL),
(60, 'Dagmawi', 'Moges', 'Male', NULL, 'dagmjes@yahoo.com', NULL, NULL, '0000-00-00', NULL, NULL, NULL),
(61, 'dagmawi', 'moges', 'Male', NULL, 'dagmjes@yahoo.com', NULL, NULL, '0000-00-00', 'Laboratory', NULL, NULL),
(62, 'Dagmawi', 'Moges', 'Male', NULL, 'dagisky@gmail.com', NULL, NULL, '0000-00-00', '', NULL, NULL),
(64, 'Colton', 'Dixon', 'Male', NULL, 'colton@dixon.com', NULL, NULL, '0000-00-00', 'Other', NULL, NULL),
(65, 'Colton', 'Dixon', 'Male', NULL, 'colton@dixon.com', NULL, NULL, '0000-00-00', 'Other', NULL, NULL),
(66, 'Colton', 'Dixon', 'Male', NULL, 'colton@dixon.com', NULL, NULL, '0000-00-00', 'Other', NULL, NULL),
(69, 'Colton', 'Dixon', 'Male', 'http://ahri.gov//resource/img/staff/Colton_69.JPG', 'colton@dixon.com', NULL, NULL, '0000-00-00', 'Other', NULL, NULL),
(70, 'Colton', 'Dixon', 'Male', 'http://ahri.gov/resource/img/staff/Colton_70.JPG', 'colton@dixon.com', NULL, NULL, '0000-00-00', 'Other', NULL, NULL),
(71, 'Colton', 'Dixon', 'Male', 'http://ahri.gov/resource/img/staff/Colton_71.JPG', 'colton@dixon.com', NULL, NULL, '0000-00-00', 'Other', NULL, NULL),
(74, 'Wude', 'Mehrit', 'Female', NULL, 'wudemjes@yahoo.com', NULL, NULL, '2000-05-15', 'Researcher', NULL, NULL),
(78, 'Kalkidan', 'Moges', 'Female', 'http://ahri.gov/resource/img/staff/Kalkidan_78.jpg', 'dagmjes@yahoo.com', NULL, NULL, '1980-02-13', 'Researcher', '2014-11-29 03:58:28', '2014-11-29 03:58:28'),
(79, 'Wude', 'Mehrit', 'Male', NULL, 'wudemjes@yahoo.com', NULL, NULL, '2000-05-15', 'Researcher', '2014-12-01 02:33:26', '2014-12-01 02:33:26'),
(80, 'Wongelawit', 'Moges', 'Female', 'http://ahri.gov/resource/img/staff/Wongelawit_80.jpg', 'wudemjes@yahoo.com', NULL, NULL, '2014-12-15', 'Laboratory', '2014-12-01 03:23:48', '2014-12-01 03:23:48'),
(81, 'Dagmawi', 'Moges', 'Male', 'http://ahri.gov/resource/img/director/Dagmawi_3.jpg', 'dagisky@gmail.com', '251922428411', NULL, '0000-00-00', 'Director', '2014-12-02 12:39:45', '2014-12-02 12:39:45'),
(82, 'Bekele', 'Mola', 'Male', NULL, 'beke@gmail.com', NULL, NULL, '2014-12-15', '', '2014-12-02 12:30:56', '2014-12-02 12:30:56'),
(83, 'Bekele', 'Mola', 'Male', NULL, 'beke@gmail.com', NULL, NULL, '2014-12-15', '', '2014-12-02 12:31:21', '2014-12-02 12:31:21'),
(84, 'Bekele', 'Mola', 'Male', NULL, 'dagmjes@yahoo.com', NULL, NULL, '2014-12-02', '', '2014-12-02 12:34:54', '2014-12-02 12:34:54'),
(85, 'Bekele', 'Mola', 'Male', NULL, 'dagmjes@yahoo.com', NULL, NULL, '2014-12-02', '', '2014-12-02 12:35:37', '2014-12-02 12:35:37'),
(86, 'Bekele', 'Mola', 'Male', NULL, 'dagmjes@yahoo.com', NULL, NULL, '2014-12-02', '', '2014-12-02 12:35:44', '2014-12-02 12:35:44'),
(87, 'Bekele', 'Mola', 'Male', NULL, 'dagmjes@yahoo.com', NULL, NULL, '2014-12-02', '', '2014-12-02 12:35:55', '2014-12-02 12:35:55'),
(88, 'Bekele', 'Mola', 'Male', NULL, 'dagmjes@yahoo.com', NULL, NULL, '2014-12-02', '', '2014-12-02 12:36:38', '2014-12-02 12:36:38'),
(89, 'Bekele', 'Mola', 'Male', NULL, 'dagmjes@yahoo.com', NULL, NULL, '2014-12-02', '', '2014-12-02 12:36:51', '2014-12-02 12:36:51'),
(90, 'Bekele', 'Mola', 'Male', NULL, 'dagisky@gmail.com', NULL, NULL, '2014-12-02', 'Data_Statistics', '2014-12-02 12:39:09', '2014-12-02 12:39:09'),
(91, 'Bekele', 'Mola', 'Male', NULL, 'dagisky@gmail.com', NULL, NULL, '2014-12-02', 'Data_Statistics', '2014-12-02 12:41:23', '2014-12-02 12:41:23'),
(92, 'Kalkidan', 'Moges', 'Female', NULL, 'dagmjes@yahoo.com', NULL, NULL, '2013-05-05', 'Data_Statistics', '2014-12-11 12:07:48', '2014-12-11 12:07:48'),
(93, 'Kalkidan', 'Moges', 'Female', 'http://ahri.gov/resource/img/staff/Kalkidan_93.JPG', 'dagmjes@yahoo.com', NULL, NULL, '2013-05-05', 'Data_Statistics', '2014-12-02 01:18:06', '2014-12-02 01:18:06'),
(95, 'Rawleigh', 'Howe', 'Male', NULL, 'rawcraig@yahoo.com', NULL, NULL, '2013-05-13', 'Researcher', '2014-12-08 11:46:36', '2014-12-08 11:46:36'),
(96, 'Rawleigh', 'Howe', 'Male', NULL, 'rawcraig@yahoo.com', NULL, NULL, '2013-05-13', 'Researcher', '2014-12-08 11:47:17', '2014-12-08 11:47:17');

-- --------------------------------------------------------

--
-- Table structure for table `teams`
--

CREATE TABLE IF NOT EXISTS `teams` (
  `team_id` int(11) NOT NULL AUTO_INCREMENT,
  `team_name` varchar(255) DEFAULT NULL,
  `team_leader` int(11) DEFAULT NULL,
  `description` longtext,
  `focus_area` mediumtext,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`team_id`),
  KEY `team_leader` (`team_leader`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `teams`
--

INSERT INTO `teams` (`team_id`, `team_name`, `team_leader`, `description`, `focus_area`, `created`, `modified`) VALUES
(2, 'leprosy', 1, 'foo', 'lolo', NULL, '2014-11-15 12:45:24'),
(3, 'Meningitis and Other Bacterial deseases', 1, NULL, NULL, NULL, NULL),
(4, 'Virology', 7, NULL, NULL, NULL, NULL),
(5, 'Tuberculosis', 1, '<h3>Tuberculosis</h3>News show/add (done)<div>Events show/add (done)</div><div>Vacancy show/add (done)</div><div>galary show/add (done)</div><div>Teams show/add</div><div>Director show/add (db)(current, past)</div><div>Laboratory</div><div>Suport and Techincal staff show/add</div><div>each staff detail profile page</div><div>email registration</div><div>side bars</div><div>Admin Page link</div><div><br></div><div>Ketsela Desta</div><div><br></div><div><br></div><div>dagisky@gmail.com</div><div>jlovesme</div><div><br></div>', '<ul><li><p><b>Purge </b>(verb): [WITH OBJ.]&nbsp; 1 rid (someone) of an unwanted feeling,\r\nmemory, or condition: <u>Bob had helped purge Martha of\r\nthe terrible guilt that had haunted her.</u>&nbsp;&nbsp; ; remove (an unwanted feeling,\r\nmemory, etc.) in such a way<span>: <u>his hatred was purged</u></span>.remove\r\n(a group of people considered undesirable) from an organization or place in an\r\nabrupt or violent manner: <u>he purged all but 26 of\r\nthe central committee members</u>.&nbsp; ; remove someone from (an organization\r\nor place) in such a way<span>:<u> an opportunity to purge the\r\nparty of unsatisfactory members.</u></span></p></li><li><p><u></u></p><p><b>Irrelevant</b> <span>(adi):&nbsp; Having no bearing&nbsp;on or connection with\r\nthe subject at&nbsp;issue<span> "<u>an irrelevant\r\ncomment"; "irrelevant allegations"</u></span></span></p></li><li><p><u></u></p><p><b>Allegations </b>(Noun): <span>(law) a\r\nformal accusation against somebody (often in a court&nbsp;of&nbsp;law) <span>"<u>an allegation of malpractice"</u></span></span></p></li><li><p><b>Apparently</b> (adv): <span>From\r\nappearances alone<span> <u>"irrigation often produces\r\nbumper crops from apparently desert land</u>"</span></span></p><p><u>\r\n\r\n</u></p><p>2 <span>Unmistakably;\r\nvisibly clear; in an evident manner<span> "<u>she has\r\napparently been living here for some time"; "I thought he owned the\r\nproperty, but apparently not"</u></span></span></p></li></ul>', '2014-10-30 12:28:02', '2014-10-30 12:28:02'),
(6, 'leprosy', 1, 'this is amaizing', 'awesomeness', '2014-11-12 02:52:57', '2014-11-12 02:52:57'),
(8, 'leprosy', 0, 'Team of change. &nbsp;we are determined to change the world.&nbsp;', '<ul><li>Nano Technology</li><li>Nano Robots&nbsp;</li><li>Enzyme&nbsp;</li><li>Molecular biology</li><li>genetics&nbsp;</li></ul><br>', NULL, '2014-11-12 02:56:25');

-- --------------------------------------------------------

--
-- Table structure for table `teams_researchers`
--

CREATE TABLE IF NOT EXISTS `teams_researchers` (
  `team_researcher_id` int(11) NOT NULL AUTO_INCREMENT,
  `researcher_id` int(11) DEFAULT NULL,
  `team_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`team_researcher_id`),
  KEY `researcher_id` (`researcher_id`),
  KEY `team_id` (`team_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `technical_staffs`
--

CREATE TABLE IF NOT EXISTS `technical_staffs` (
  `technical_staff_id` int(11) NOT NULL AUTO_INCREMENT,
  `staff_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`technical_staff_id`),
  KEY `staff_id` (`staff_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `trainings`
--

CREATE TABLE IF NOT EXISTS `trainings` (
  `training_id` int(11) NOT NULL AUTO_INCREMENT,
  `training_title` varchar(255) DEFAULT NULL,
  `trainer_name` varchar(150) DEFAULT NULL,
  `trainer_email` varchar(30) DEFAULT NULL,
  `training_desc` text,
  `training_starting_date` date DEFAULT NULL,
  `training_ending_date` date DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`training_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=34 ;

--
-- Dumping data for table `trainings`
--

INSERT INTO `trainings` (`training_id`, `training_title`, `trainer_name`, `trainer_email`, `training_desc`, `training_starting_date`, `training_ending_date`, `created`, `modified`) VALUES
(10, 'Microbiology', 'Wude Mehrit', 'wudebuz@gmail.com', 'This course will help the trainee to understand the microbial world even better. foo1', '2014-11-17', '2014-11-02', '2014-11-08 05:01:37', '2014-11-08 05:01:37'),
(18, 'awesome bacteria (GCLP) Training', 'dagmawi Moges', 'dagisky@gmail.com', 'men cool You are encouraged to use this function any time you need to generate a local URL so that your pages become more portable in the event your URL changes.\r\n\r\nSegments can be optionally passed to the function as a string or an array. Here is a string example:', '1970-01-01', '1970-01-01', '2014-12-11 11:54:01', '2014-12-11 11:54:01'),
(25, 'TB', 'dagmawi Moges', 'dagisky@gmail.com', 'News show/add (done)\r\nEvents show/add (done)\r\nVacancy show/add (done)\r\ngalary show/add (done)\r\nTeams show/add\r\nDirector show/add (db)(current, past)\r\nLaboratory\r\nSuport and Techincal staff show/add\r\neach staff detail profile page\r\nemail registration\r\nside bars', '2014-10-01', '2014-10-01', '2014-10-29 11:13:23', '2014-10-29 11:13:23'),
(29, 'Men this Now works', 'Dagmawi Moges Alemu', 'wudebuz@gmail.com', 'News show/add (done)\r\nEvents show/add (done)\r\nVacancy show/add (done)\r\ngalary show/add (done)\r\nTeams show/add\r\nDirector show/add (db)(current, past)\r\nLaboratory\r\nSuport and Techincal staff show/add\r\neach staff detail profile page\r\nemail registration\r\nside bars', '2014-06-01', '2014-12-18', '2014-10-29 11:18:21', '2014-10-29 11:18:21'),
(30, 'Men this Now works', 'Dagmawi Moges Alemu', 'wudebuz@gmail.com', 'News show/add (done)\r\nEvents show/add (done)\r\nVacancy show/add (done)\r\ngalary show/add (done)\r\nTeams show/add\r\nDirector show/add (db)(current, past)\r\nLaboratory\r\nSuport and Techincal staff show/add\r\neach staff detail profile page\r\nemail registration\r\nside bars', '2014-06-01', '2014-12-18', '2014-10-29 11:18:41', '2014-10-29 11:18:41'),
(32, 'Microbiology', 'Wude Mehrit', 'wudebuz@gmail.com', 'This course will help the trainee to understand the microbial world even better.', '2014-11-10', '2014-10-23', '2014-11-08 03:49:27', '2014-11-08 03:49:27'),
(33, 'NanoTechnology', 'Dagmwi Moges', 'dagisky@gmail.com', 'this is a training on nano and micro bots to better human living conditions. these systems has the ability to purge any anomaly in the human body system.', '2015-06-16', '2015-08-04', '2014-11-19 12:09:44', '2014-11-19 12:09:44');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `fname` varchar(25) DEFAULT NULL,
  `lname` varchar(25) DEFAULT NULL,
  `avatar_name` varchar(20) DEFAULT NULL,
  `profile_img` varchar(255) DEFAULT NULL,
  `email` varchar(40) DEFAULT NULL,
  `username` varchar(15) DEFAULT NULL,
  `password` varchar(40) DEFAULT NULL,
  `user_type` enum('Administrator','Director','Generic') DEFAULT NULL,
  `status` enum('Active','Inactive') DEFAULT 'Active',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `fname`, `lname`, `avatar_name`, `profile_img`, `email`, `username`, `password`, `user_type`, `status`, `created`, `modified`) VALUES
(1, 'dagmawi', 'Moges', 'dagisky', NULL, 'dagisky@gmail.com', 'dagisky', 'jlovesme', 'Administrator', 'Active', '2014-10-18 00:00:00', '2014-10-18 00:00:02');

-- --------------------------------------------------------

--
-- Table structure for table `vacancies`
--

CREATE TABLE IF NOT EXISTS `vacancies` (
  `vacancy_id` int(11) NOT NULL AUTO_INCREMENT,
  `position` varchar(200) DEFAULT NULL,
  `openings` smallint(6) DEFAULT '1',
  `deadline` date DEFAULT NULL,
  `eligibility` text,
  `responsibilities` text,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`vacancy_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `vacancies`
--

INSERT INTO `vacancies` (`vacancy_id`, `position`, `openings`, `deadline`, `eligibility`, `responsibilities`, `created`, `modified`) VALUES
(2, 'ICT Managger', 1, '2014-12-11', '<ul><li>Bsc in Computer Science, Electrical Engineering, IT, and related Fields</li><li>Ability to work together&nbsp;&nbsp;</li><li>Good customer management</li><li><br></li></ul>', '', '2014-10-20 08:32:52', '2014-12-11 12:00:05'),
(3, 'ICT Managger', 1, '2014-11-11', '<ul><li>Bsc in Computer Science, Electrical Engineering, IT, and related Fields</li><li>Ability to work together&nbsp;&nbsp;</li><li>Good customer management</li><li>Good Accounting Skill</li></ul>', '<ul><li>Manage and destroy the world</li><li>Explode on the middle of populated street wearing explosive vest</li><li>Terrorize People&nbsp;</li></ul>', '2014-10-20 08:32:52', '2014-11-08 07:04:07'),
(4, 'ICT Managger', 1, '2014-11-11', '<ul><li>Bsc in Computer Science, Electrical Engineering, IT, and related Fields</li><li>Ability to work together&nbsp;&nbsp;</li><li>Good customer management</li><li>Good Accounting Skill</li></ul>', '<ul><li>Manage and destroy the world</li><li>Explode on the middle of populated street wearing explosive vest</li><li>Terrorize People&nbsp;</li><li>Make Home Made Bombs</li></ul>', '2014-10-20 08:32:52', '2014-11-08 07:04:35'),
(6, 'Terrorist', 1, '2014-11-19', '<ul><li>Bsc In Chemical Engineering, Molecular Biology, Virology, Bacteriology&nbsp;&nbsp;</li><li>Recommendation Letter from well known Terrorist Organization</li><li>(EPFT) &nbsp;exam &nbsp;&gt; &nbsp;6.5&nbsp;English Proficiency for Terrorist&nbsp;</li></ul>', '<ul><li>Explode in the middle of crowded area using explosive vest</li><li>make Home made bombs</li><li>motivate fellow terrorists</li><li>make biological, chemical and atomic warheads&nbsp;</li></ul>', '2014-11-08 07:10:15', '2014-11-08 07:14:03');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `data_statistics_staffs`
--
ALTER TABLE `data_statistics_staffs`
  ADD CONSTRAINT `data_statistics_staffs_ibfk_1` FOREIGN KEY (`staff_id`) REFERENCES `staffs` (`staff_id`);

--
-- Constraints for table `directors`
--
ALTER TABLE `directors`
  ADD CONSTRAINT `directors_ibfk_1` FOREIGN KEY (`staff_id`) REFERENCES `staffs` (`staff_id`);

--
-- Constraints for table `laboratory_images`
--
ALTER TABLE `laboratory_images`
  ADD CONSTRAINT `laboratory_images_ibfk_1` FOREIGN KEY (`laboratory_id`) REFERENCES `laboratories` (`laboratory_id`);

--
-- Constraints for table `laboratory_staffs`
--
ALTER TABLE `laboratory_staffs`
  ADD CONSTRAINT `laboratory_staffs_ibfk_1` FOREIGN KEY (`staff_id`) REFERENCES `staffs` (`staff_id`);

--
-- Constraints for table `other_staff`
--
ALTER TABLE `other_staff`
  ADD CONSTRAINT `other_staff_ibfk_1` FOREIGN KEY (`staff_id`) REFERENCES `staffs` (`staff_id`);

--
-- Constraints for table `photos`
--
ALTER TABLE `photos`
  ADD CONSTRAINT `photos_ibfk_1` FOREIGN KEY (`gallery_id`) REFERENCES `galleries` (`gallery_id`);

--
-- Constraints for table `researchers`
--
ALTER TABLE `researchers`
  ADD CONSTRAINT `researchers_ibfk_1` FOREIGN KEY (`staff_id`) REFERENCES `staffs` (`staff_id`);

--
-- Constraints for table `researchs`
--
ALTER TABLE `researchs`
  ADD CONSTRAINT `researchs_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `research_categories` (`category_id`);

--
-- Constraints for table `teams_researchers`
--
ALTER TABLE `teams_researchers`
  ADD CONSTRAINT `teams_researchers_ibfk_1` FOREIGN KEY (`researcher_id`) REFERENCES `researchers` (`researcher_id`),
  ADD CONSTRAINT `teams_researchers_ibfk_2` FOREIGN KEY (`team_id`) REFERENCES `teams` (`team_id`);

--
-- Constraints for table `technical_staffs`
--
ALTER TABLE `technical_staffs`
  ADD CONSTRAINT `technical_staffs_ibfk_1` FOREIGN KEY (`staff_id`) REFERENCES `staffs` (`staff_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
