<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Teams
 *
 * @author Dagi
 */
class Teams extends CI_Controller {

    //put your code here
    private $view_sp = array('mainNav' => 'mainNav', 'mainPane' => array('Teams/team_list'), 'jumbotron' => array(), 'aside' => array('_newsSlider', '_sideNav'));

    public function __construct() {
        parent::__construct();
        session_start();
    }

    public function index($page = null) {
        if ($this->Authorizer->authorize(array(ADMIN, DIRECTOR))) {
            if ($_SESSION['user_type'] == ADMIN) {
                $this->view_sp['mainNav'] = 'Admin/adminNav';
                $this->view_sp['aside'] = array('_newsSlider', 'Admin/_sideNav1');
            } else if ($_SESSION['user_type'] == DIRECTOR) {
                $this->view_sp['mainNav'] = '';
            }
        }
        $per_page = 10;
        $this->load->model('Team');
        $this->load->library('pagination');
        if ($page != null) {
            $page_data = $this->Team->view($per_page, $page);
        } else {
            $page_data = $this->Team->view($per_page);
        }

        $config['base_url'] = site_url('Teams/index');
        $config['total_rows'] = $page_data['num_rows'];
        $config['per_page'] = $per_page;
        $config['full_tag_open'] = '<ul class="pager">';
        $config['full_tag_close'] = '</ul>';

        $config['prev_link'] = 'Previous';
        $config['prev_tag_open'] = '<li style="margin-left:5px;">';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = 'Next';
        $config['next_tag_open'] = '<li style="margin-left:5px;">';
        $config['next_tag_close'] = '</li>';
        $this->pagination->initialize($config);

        if ($page_data['num_rows'] > $per_page) {
            $this->load->vars(array('paginator' => $this->pagination->create_links()));
        }

        $this->load->vars('teams', $page_data['data']);
        $this->load->view('Layout', $this->view_sp);
    }

    public function team($team_id) {
        $this->view_sp['mainPane'] = array('Teams/team_det');
        $this->load->model('Team');
        $this->load->model('Researcher');
        $this->load->model('Staff');
        $team = $this->Team->find($team_id);
        $this->load->vars('team', $team);
        $this->load->view('Layout', $this->view_sp);
    }

    public function add() {
        $this->view_sp['mainPane'] = array('Teams/add_team');
        $this->view['aside'] = array();
        $this->view_sp['styles'] = array('bootstrap-datetimepicker.min', 'bootstrap3-wysiwyg5');
        $this->view_sp['scripts'] = array('moment', 'bootstrap-datetimepicker.min', 'wysihtml5-0.3.0.min', 'bootstrap3-wysihtml5');
        $this->load->model('Researcher');
        $this->load->model('Staff');
        $researchers = $this->Researcher->view();
             
        if ($this->Authorizer->authorize(array(ADMIN, DIRECTOR))) {
            if ($_SESSION['user_type'] == ADMIN) {
                $this->view_sp['mainNav'] = 'Admin/adminNav';
                $this->view_sp['aside'] = array('_newsSlider', 'Admin/_sideNav1');
            } else if ($_SESSION['user_type'] == DIRECTOR) {
                $this->view_sp['mainNav'] = '';
            }
            $this->load->library('form_validation');
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

            $this->form_validation->set_rules('team_name', 'Team Name', 'trim|required');
            $this->form_validation->set_rules('team_leader', 'Team Leader', 'trim|required');
            $this->form_validation->set_rules('team_desc', 'Team Description', 'trim|required');
            $this->form_validation->set_rules('focus_area', 'Team Focus Area', 'trim|required');

            $this->load->model('Team');
            $this->load->model('Research_category');
            $researchCategory = $this->Research_category->view();
            $this->load->vars('researchCatgories', $researchCategory['data']);
            $this->load->vars('researchers', $researchers['data']);
            
            if ($this->form_validation->run() == FALSE) {
                $this->view_sp['mainPane'] = array('Teams/add_team');
            } else {
                $data = array(
                    'team_name' => $this->input->post('team_name'),
                    'team_leader' => $this->input->post('team_leader'),
                    'description' => $this->input->post('team_desc'),
                    'focus_area' => $this->input->post('focus_area'),
                    'created' => date("Y-m-d h:i:s", time()),
                    'modified' => date("Y-m-d h:i:s", time())
                );

                if ($team_id = $this->Team->insert($data)) {
                    $team = $this->Team->find($team_id);
                    $this->load->vars('team', $team);
                } else {
                    echo 'Failed';
                }
            }
             
            $this->load->view('Layout', $this->view_sp);
        } else {
            redirect(base_url() . 'index.php/Auth');
        }
    }

    public function edit($team_id) {
        $this->view_sp['mainPane'] = array('Teams/edit_team');
        $this->view['aside'] = array();
        $this->view_sp['styles'] = array('bootstrap-datetimepicker.min', 'bootstrap3-wysiwyg5');
        $this->view_sp['scripts'] = array('moment', 'bootstrap-datetimepicker.min', 'wysihtml5-0.3.0.min', 'bootstrap3-wysihtml5');
        $this->load->model('Researcher');
        $this->load->model('Staff');
        $researchers = $this->Researcher->view();
        $this->load->vars('researchers', $researchers['data']);

        if ($this->Authorizer->authorize(array(ADMIN, DIRECTOR))) {
            if ($_SESSION['user_type'] == ADMIN) {
                $this->view_sp['mainNav'] = 'Admin/adminNav';
                $this->view_sp['aside'] = array('_newsSlider', 'Admin/_sideNav1');
            } else if ($_SESSION['user_type'] == DIRECTOR) {
                $this->view_sp['mainNav'] = '';
            }
            $this->load->library('form_validation');
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

            $this->form_validation->set_rules('team_name', 'Team Name', 'trim|required');
            $this->form_validation->set_rules('team_leader', 'Team Leader', 'trim|required');
            $this->form_validation->set_rules('team_desc', 'Team Description', 'trim|required');
            $this->form_validation->set_rules('focus_area', 'Team Focus Area', 'trim|required');

            $this->load->model('Team');
            $this->load->model('Research_category');
            $researchCategory = $this->Research_category->view();
            $this->load->vars('researchCatgories', $researchCategory['data']);
            $team = $this->Team->find($team_id);
            if ($this->form_validation->run() == FALSE) {
                $this->view_sp['mainPane'] = array('Teams/edit_team');
                $this->load->vars('team', $team);
            } else {
                $data = array(
                    'team_name' => $this->input->post('team_name'),
                    'team_leader' => $this->input->post('team_leader'),
                    'description' => $this->input->post('team_desc'),
                    'focus_area' => $this->input->post('focus_area'),
                    'created' => $team['created'],
                    'modified' => date("Y-m-d h:i:s", time())
                );

                if ($this->Team->edit($team_id, $data)) {
                    $team = $this->Team->find($team_id);
                    $this->load->vars('team', $team);
                } else {
                    echo 'Failed';
                }
            }
            $this->load->view('Layout', $this->view_sp);
        } else {
            redirect(base_url() . 'index.php/Auth');
        }
    }

    public function delete($team_id, $remove = false) {
        if ($this->Authorizer->authorize(array(ADMIN, DIRECTOR))) {
            if ($_SESSION['user_type'] == ADMIN) {
                $this->view_sp['mainNav'] = 'Admin/adminNav';
                $this->view_sp['aside'] = array('_newsSlider', 'Admin/_sideNav1');
            } else if ($_SESSION['user_type'] == DIRECTOR) {
                $this->view_sp['mainNav'] = '';
            }
            $this->load->model('Team');
            $this->load->model('Staff');
            $this->load->model('Researcher');
            if ($remove) {
                if ($this->Team->delete($team_id)) {
                    $this->view_sp['mainPane'] = array('_removeSuccess');
                    $this->load->vars(array('back' => site_url('Teams/index')));
                } else {
                    echo 'Not Removed';
                }
            } else {
                $this->view_sp['mainPane'] = array('_remove');
                $team = $this->Team->find($team_id);
                $remv = array('data' => 'Team',
                    'yes_link' => base_url() . 'index.php/Teams/delete/' . $team_id . '/true',
                    'no_link' => base_url() . 'index.php/Teams',
                    'load' => 'Teams/team');

                $this->load->vars('remove', $remv);
                $this->load->vars('team', $team);
            }
            $this->load->view('Layout', $this->view_sp);
        } else {
            redirect(base_url() . 'index.php/Auth');
        }
    }

}
