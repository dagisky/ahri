<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Directors
 *
 * @author Dagi
 */
class Directors extends CI_Controller {

    private $view_sp = array('mainNav' => 'mainNav', 'mainPane' => array('Directors/director_profile'), 'jumbotron' => array(), 'aside' => array('_newsSlider', '_sideNav'));

    //put your code here
    public function __construct() {
        parent::__construct();
        session_start();
    }

    public function index() {
        if ($this->Authorizer->authorize(array(ADMIN, DIRECTOR))) {
            if ($_SESSION['user_type'] == ADMIN) {
                $this->view_sp['mainNav'] = 'Admin/adminNav';
                $this->view_sp['aside'] = array('_newsSlider', 'Admin/_sideNav1');
            } else if ($_SESSION['user_type'] == DIRECTOR) {
                $this->view_sp['mainNav'] = '';
            }
        }
        $this->load->model('Director');
        $this->load->model('Staff');
        $director = $this->Director->currentDirector();
        $this->load->vars('director', $director);
        $this->load->view('Layout', $this->view_sp);
    }

    public function past() {
        
    }

    public function add() {
        $this->view_sp['mainPane'] = array('Directors/add_director');
        $this->view['aside'] = array();
        $this->view_sp['scripts'] = array('moment', 'bootstrap-datetimepicker.min', 'wysihtml5-0.3.0.min', 'bootstrap3-wysihtml5');
        $this->view_sp['styles'] = array('bootstrap-datetimepicker.min', 'bootstrap3-wysiwyg5');


        if ($this->Authorizer->authorize(array(ADMIN, DIRECTOR))) {
            if ($_SESSION['user_type'] == ADMIN) {
                $this->view_sp['mainNav'] = 'Admin/adminNav';
                $this->view_sp['aside'] = array('_newsSlider', 'Admin/_sideNav1');
            } else if ($_SESSION['user_type'] == DIRECTOR) {
                $this->view_sp['mainNav'] = '';
            }
            $this->load->library('form_validation');
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

            $this->form_validation->set_rules('fname', 'First Name', 'trim|required');
            $this->form_validation->set_rules('lname', 'Last Name', 'trim|required');
            $this->form_validation->set_rules('sex', 'Sex', 'trim|required');
            $this->form_validation->set_rules('email', 'Email', 'trim|required');
            $this->form_validation->set_rules('phone1', 'Phone 1', 'trim|required');
            $this->form_validation->set_rules('dob', 'Date of Birth', 'trim|required');
            $this->form_validation->set_rules('appointed_date', 'Appointed Date', 'trim|required');
            $this->form_validation->set_rules('resignation_date', 'Resignation Date', 'trim|required');

            $this->form_validation->set_rules('title', 'Researchers Position', 'trim|required');


            $this->load->model('Staff');
            $this->load->model('Director');


            if ($this->form_validation->run() == FALSE) {
                $this->view_sp['mainPane'] = array('Directors/add_director');
            } else {
                $data = array(
                    'fname' => $this->input->post('fname'),
                    'lname' => $this->input->post('lname'),
                    'sex' => $this->input->post('sex'),
                    'email' => $this->input->post('email'),
                    'phone1' => $this->input->post('phone1'),
                    'dob' => $this->input->post('dob'),
                    'staff_type' => "Director",
                    'created' => date("Y-m-d h:i:s", time()),
                    'modified' => date("Y-m-d h:i:s", time())
                );



                if ($staff_id = $this->Staff->insert($data)) {
                    $staff = $this->Staff->find($staff_id);
                    $this->form_validation->set_rules('profile_img', 'Profile Image', 'required');
                   
                    $staff = $this->Staff->find($staff_id);

                    $this->load->model('TimeF');
                    $data = array(
                        'staff_id' => $staff_id,
                        'director_title' => $this->input->post('title'),
                        'edu_level' => $this->input->post('edu_level'),
                        'biography' => $this->input->post('biography'),
                        'from_date' => $this->TimeF->HumanToMysql($this->input->post('appointed_date')),
                        'to_date' => $this->input->post('resignation_date') != null ? $this->TimeF->HumanToMysql($this->input->post('resignation_date')) : null,
                        'status' => $this->input->post('status')
                    );
                    if (!$director_id = $this->Director->insert($data)) {
                        //roll back removing the staff added previosly
                    }
                    $director = $this->Director->find($director_id);
                    $this->load->vars('director', $director);
                    $config['upload_path'] = 'resource/img/director/';
                    $config['allowed_types'] = 'gif|jpg|png';
                    $config['file_name'] = $staff['fname'] . '_' . $staff_id;
                    //$config['max_size'] = '100';
                    //$config['max_width'] = '1024';
                    // $config['max_height'] = '768';
                    $this->load->library('upload', $config);
                    if (!$this->upload->do_upload('profile_img')) {
                        $error = array('error' => $this->upload->display_errors());
                        print_r($error);
                    } else {
                        $upload_data = $this->upload->data();
                        $config = null;
                        $config['image_library'] = 'gd2';
                        $config['source_image'] = $upload_data['full_path'];
                        $config['create_thumb'] = TRUE;
                        $config['maintain_ratio'] = TRUE;
                        $config['width'] = 75;
                        $config['height'] = 50;
                        $this->load->library('image_lib', $config);
                        if ($this->image_lib->resize()) {
                            echo '<h1>awesome</h1>';
                        } else {
                            echo '<h1>Not bad</h1>';
                        }

                        $this->Staff->edit($staff_id, array('profile_img' => base_url() . 'resource/img/director/' . $upload_data['file_name']));
                    }

                    $this->load->vars('staff', $staff);
                } else {
                    echo 'Failed';
                }
            }
            $this->load->view('Layout', $this->view_sp);
        } else {
            redirect(base_url() . 'index.php/Auth');
        }
    }

    public function edit($staff_id) {
        $this->view_sp['mainPane'] = array('Directors/edit_director');
        $this->view['aside'] = array();
        $this->view_sp['scripts'] = array('moment', 'bootstrap-datetimepicker.min', 'wysihtml5-0.3.0.min', 'bootstrap3-wysihtml5');
        $this->view_sp['styles'] = array('bootstrap-datetimepicker.min', 'bootstrap3-wysiwyg5');


        if ($this->Authorizer->authorize(array(ADMIN, DIRECTOR))) {
            if ($_SESSION['user_type'] == ADMIN) {
                $this->view_sp['mainNav'] = 'Admin/adminNav';
                $this->view_sp['aside'] = array('_newsSlider', 'Admin/_sideNav1');
            } else if ($_SESSION['user_type'] == DIRECTOR) {
                $this->view_sp['mainNav'] = '';
            }
            $this->load->library('form_validation');
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

            $this->form_validation->set_rules('fname', 'First Name', 'trim|required');
            $this->form_validation->set_rules('lname', 'Last Name', 'trim|required');
            $this->form_validation->set_rules('sex', 'Sex', 'trim|required');
            $this->form_validation->set_rules('email', 'Email', 'trim|required');
            $this->form_validation->set_rules('phone1', 'Phone 1', 'trim|required');
            $this->form_validation->set_rules('dob', 'Date of Birth', 'trim|required');
            $this->form_validation->set_rules('appointed_date', 'Appointed Date', 'trim|required');
            $this->form_validation->set_rules('resignation_date', 'Resignation Date', 'trim|required');

            $this->form_validation->set_rules('title', 'Researchers Position', 'trim|required');


            $this->load->model('Staff');
            $this->load->model('Director');
            $this->load->model('TimeF');
            $director = $this->Director->findStaff($staff_id);
            $this->load->vars('director', $director);

            if ($this->form_validation->run() == FALSE) {
                $this->view_sp['mainPane'] = array('Directors/edit_director');
            } else {
                $data = array(
                    'fname' => $this->input->post('fname'),
                    'lname' => $this->input->post('lname'),
                    'sex' => $this->input->post('sex'),
                    'email' => $this->input->post('email'),
                    'phone1' => $this->input->post('phone1'),
                    'dob' => $this->TimeF->HumanToMysql($this->input->post('dob')),
                    'staff_type' => "Director",
                    'created' => date("Y-m-d h:i:s", time()),
                    'modified' => date("Y-m-d h:i:s", time())
                );



                if ($this->Staff->edit($staff_id, $data)) {
                    $staff = $this->Staff->find($staff_id);
                    $this->form_validation->set_rules('profile_img', 'Profile Image', 'required');
                    
                    $staff = $this->Staff->find($staff_id);

                    $this->load->model('TimeF');
                    $data = array(
                        'staff_id' => $staff_id,
                        'director_title' => $this->input->post('title'),
                        'edu_level' => $this->input->post('edu_level'),
                        'biography' => $this->input->post('biography'),
                        'from_date' => $this->TimeF->HumanToMysql($this->input->post('appointed_date')),
                        'to_date' => $this->input->post('resignation_date') != null ? $this->TimeF->HumanToMysql($this->input->post('resignation_date')) : null,
                        'status' => $this->input->post('status')
                    );
                    if (!$this->Director->edit($director['director_id'], $data)) {
                        //roll back to the previous state
                    }
                    $director = $this->Director->find($director['director_id']);
                    $this->load->vars('director', $director);
                    $config['upload_path'] = 'resource/img/director/';
                    $config['allowed_types'] = 'gif|jpg|png';
                    $config['file_name'] = $staff['fname'] . '_' . $staff_id;
                    //$config['max_size'] = '100';
                    //$config['max_width'] = '1024';
                    // $config['max_height'] = '768';
                    $this->load->library('upload', $config);
                    if($_FILES['profile_img']['name'] != null)
                    if (!$this->upload->do_upload('profile_img')) {
                        $error = array('error' => $this->upload->display_errors());
                        print_r($error);
                    } else {
                        $upload_data = $this->upload->data();
                        $config = null;
                        $config['image_library'] = 'gd2';
                        $config['source_image'] = $upload_data['full_path'];
                        $config['create_thumb'] = TRUE;
                        $config['maintain_ratio'] = TRUE;
                        $config['width'] = 75;
                        $config['height'] = 50;
                        $this->load->library('image_lib', $config);
                        if ($this->image_lib->resize()) {
                           // echo '<h1>awesome</h1>';
                        } else {
                           // echo '<h1>Not bad</h1>';
                        }

                        $this->Staff->edit($staff_id, array('profile_img' => base_url() . 'resource/img/director/' . $upload_data['file_name']));
                    }
                    $this->load->vars('staff', $staff);
                } else {
                    echo 'Failed';
                }
            }
            $this->load->view('Layout', $this->view_sp);
        } else {
            redirect(base_url() . 'index.php/Auth');
        }
    }

}
