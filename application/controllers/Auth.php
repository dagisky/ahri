<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Auth
 *
 * @author Dagi
 */
class Auth extends CI_Controller{
    //put your code here
    private $view_sp = array('mainPane' => array(), 'jumbotron' => array(), 'aside' => array(), 'styles' => array('Resource/css/login'), 'scripts' => array());

    public function __construct() {
        parent::__construct();
        session_start();
    }
    
    public function index(){
        $this->load->model('Authorizer');
         if (!$this->Authorizer->authorize(array(ADMIN, DIRECTOR))) {
            $this->load->library('form_validation');
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
            $this->form_validation->set_rules('username', 'Username', 'trim|required');
            $this->form_validation->set_rules('password', 'Password', 'required');
            if ($this->form_validation->run() == FALSE) {
                $this->load->view('Auth/login', $this->view_sp);
            } else {
                $this->load->model('Authenticator');
               if($this->Authenticator->VerifyUser($this->input->post('username'), $this->input->post('password'))){
                   redirect('About/index');//lead the user to the appropriate page                  
               }else{
                   $this->load->view('Auth/login', $this->view_sp);
               }
            }
        }
    }
    
    public function logout(){
        session_destroy();
        redirect('Home/index');
    }
    
}
