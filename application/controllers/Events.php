<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Events
 *
 * @author Dagi
 */
class Events extends CI_Controller {

    //put your code here
    private $view_sp = array('mainNav' => 'mainNav', 'mainPane' => array('Events/event_list'), 'jumbotron' => array(), 'aside' => array('_newsSlider', '_sideNav'));

    public function __construct() {
        parent::__construct();
        session_start();
    }

    public function index($page = null) {
        if ($this->Authorizer->authorize(array(ADMIN, DIRECTOR))) {
            if ($_SESSION['user_type'] == ADMIN) {
                $this->view_sp['mainNav'] = 'Admin/adminNav';
                $this->view_sp['aside'] = array('_newsSlider', 'Admin/_sideNav1');
            } else if ($_SESSION['user_type'] == DIRECTOR) {
                $this->view_sp['mainNav'] = '';
            }
        }
        $per_page = 10;
        $this->load->model('Event');
        $this->load->library('pagination');
        if ($page != null) {
            $page_data = $this->Event->view($per_page, $page);
        } else {
            $page_data = $this->Event->view($per_page);
        }

        $config['base_url'] = site_url('Events/index');
        $config['total_rows'] = $page_data['num_rows'];
        $config['per_page'] = $per_page;
        $config['full_tag_open'] = '<ul class="pager">';
        $config['full_tag_close'] = '</ul>';

        $config['prev_link'] = 'Previous';
        $config['prev_tag_open'] = '<li style="margin-left:5px;">';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = 'Next';
        $config['next_tag_open'] = '<li style="margin-left:5px;">';
        $config['next_tag_close'] = '</li>';
        $this->pagination->initialize($config);

        if ($page_data['num_rows'] > $per_page) {
            $this->load->vars(array('paginator' => $this->pagination->create_links()));
        }

        $this->load->vars('events', $page_data['data']);
        $this->load->view('Layout', $this->view_sp);
    }

    public function add() {
        $this->view_sp['mainPane'] = array('Events/add_event');
        $this->view['aside'] = array();

        if ($this->Authorizer->authorize(array(ADMIN, DIRECTOR))) {
            if ($_SESSION['user_type'] == ADMIN) {
                $this->view_sp['mainNav'] = 'Admin/adminNav';
                $this->view_sp['aside'] = array('_newsSlider', 'Admin/_sideNav1');
            } else if ($_SESSION['user_type'] == DIRECTOR) {
                $this->view_sp['mainNav'] = '';
            }
            $this->load->library('form_validation');
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

            $this->form_validation->set_rules('event_title', 'Event Title', 'trim|required');
            $this->form_validation->set_rules('event_content', 'Event description', 'trim|required');

            $this->load->model('Event');

            if ($this->form_validation->run() == FALSE) {
                $this->view_sp['mainPane'] = array('Events/add_event');
            } else {
                $data = array(
                    'event_title' => $this->input->post('event_title'),
                    'event_content' => $this->input->post('event_content'),
                    'created' => date("Y-m-d h:i:s", time()),
                    'modified' => date("Y-m-d h:i:s", time())
                );

                if ($event_id = $this->Event->insert($data)) {
                    $event = $this->Event->find($event_id);
                    $this->load->vars('event', $event);
                } else {
                    echo 'Failed';
                }
            }
            $this->load->view('Layout', $this->view_sp);
        } else {
            redirect(base_url() . 'index.php/Auth');
        }
    }

    public function edit($event_id) {
        $this->view_sp['mainPane'] = array('Events/edit_event');
        $this->view['aside'] = array();

        if ($this->Authorizer->authorize(array(ADMIN, DIRECTOR))) {
            if ($_SESSION['user_type'] == ADMIN) {
                $this->view_sp['mainNav'] = 'Admin/adminNav';
                $this->view_sp['aside'] = array('_newsSlider', 'Admin/_sideNav1');
            } else if ($_SESSION['user_type'] == DIRECTOR) {
                $this->view_sp['mainNav'] = '';
            }
            $this->load->library('form_validation');
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

            $this->form_validation->set_rules('event_title', 'Event Title', 'trim|required');
            $this->form_validation->set_rules('event_content', 'Event description', 'trim|required');

            $this->load->model('Event');
            $event = $this->Event->find($event_id);

            if ($this->form_validation->run() == FALSE) {
                $this->view_sp['mainPane'] = array('Events/edit_event');
                $this->load->vars('event', $event);
            } else {
                $data = array(
                    'event_title' => $this->input->post('event_title'),
                    'event_content' => $this->input->post('event_content'),
                    'created' => $event['event_id'],
                    'modified' => date("Y-m-d h:i:s", time())
                );

                if ($event_id = $this->Event->insert($data)) {
                    $event = $this->Event->find($event_id);
                    $this->load->vars('event', $event);
                } else {
                    echo 'Failed';
                }
            }
            $this->load->view('Layout', $this->view_sp);
        } else {
            redirect(base_url() . 'index.php/Auth');
        }
    }
    
    public function delete($event_id, $remove = false){
        if ($this->Authorizer->authorize(array(ADMIN, DIRECTOR))) {
            if ($_SESSION['user_type'] == ADMIN) {
                $this->view_sp['mainNav'] = 'Admin/adminNav';
                $this->view_sp['aside'] = array('_newsSlider', 'Admin/_sideNav1');
            } else if ($_SESSION['user_type'] == DIRECTOR) {
                $this->view_sp['mainNav'] = '';
            }
            $this->load->model('Event');
            if ($remove) {
                if ($this->Event->delete($event_id)) {
                    $this->view_sp['mainPane'] = array('_removeSuccess');
                    $this->load->vars(array('back' => site_url('Events/index')));
                } else {
                    echo 'Not Removed';
                }
            } else {
                $this->view_sp['mainPane'] = array('_remove');
                $event = $this->Event->find($event_id);
                $remv = array('data' => 'Vacancy',
                    'yes_link' => base_url() . 'index.php/Events/delete/' . $event_id . '/true',
                    'no_link' => base_url() . 'index.php/Events',
                    'load' => 'Events/event');

                $this->load->vars('remove', $remv);
                $this->load->vars('event', $event);
            }
            $this->load->view('Layout', $this->view_sp);
        } else {
            redirect(base_url() . 'index.php/Auth');
        }
    }

}
