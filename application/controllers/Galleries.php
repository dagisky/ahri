<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Galleries
 *
 * @author Dagi
 */
class Galleries extends CI_Controller {

    //put your code here
    private $view_sp = array('mainNav' => 'mainNav', 'mainPane' => array('Galleries/gallery_display'), 'jumbotron' => array(), 'aside' => array('_newsSlider', '_sideNav'));

    public function __construct() {
        parent::__construct();
        session_start();
    }

    public function index($gallery_id = null) {
        if ($this->Authorizer->authorize(array(ADMIN, DIRECTOR))) {
            if ($_SESSION['user_type'] == ADMIN) {
                $this->view_sp['mainNav'] = 'Admin/adminNav';
                $this->view_sp['aside'] = array('_newsSlider', 'Admin/_sideNav1');
            } else if ($_SESSION['user_type'] == DIRECTOR) {
                $this->view_sp['mainNav'] = '';
            }
        }
        $this->view_sp['mainPane'] = array('Galleries/gallery_display');
        $this->load->model('Gallery');
        $this->load->model('Photo');
        $galleries = $this->Gallery->view();
        if ($gallery_id == null) {
            $gallery_id = $galleries['data'][0]['gallery_id'];
        }
        $gallery = $this->Gallery->find($gallery_id);
        $this->load->vars('galleries', $galleries['data']);
        $this->load->vars('gallery', $gallery);
        $this->load->view('Layout', $this->view_sp);
    }

    public function photo() {
        $this->view_sp['mainPane'] = array('Galleries/add_gallery');
        $this->model->load('Gallery');
    }

    public function add() {
        $this->view_sp['mainPane'] = array('Galleries/add_gallery');
        $this->view['aside'] = array();

        if ($this->Authorizer->authorize(array(ADMIN, DIRECTOR))) {
            if ($_SESSION['user_type'] == ADMIN) {
                $this->view_sp['mainNav'] = 'Admin/adminNav';
                $this->view_sp['aside'] = array('_newsSlider', 'Admin/_sideNav1');
            } else if ($_SESSION['user_type'] == DIRECTOR) {
                $this->view_sp['mainNav'] = '';
            }
            $this->load->library('form_validation');
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

            $this->form_validation->set_rules('gallery_title', 'Album Title', 'trim|required');
            $this->form_validation->set_rules('gallery_desc', 'Album description', 'trim|required');

            $this->load->model('Gallery');

            if ($this->form_validation->run() == FALSE) {
                $this->view_sp['mainPane'] = array('Galleries/add_gallery');
            } else {
                $data = array(
                    'gallery_title' => $this->input->post('gallery_title'),
                    'description' => $this->input->post('gallery_desc'),
                    'created' => date("Y-m-d h:i:s", time()),
                    'modified' => date("Y-m-d h:i:s", time())
                );

                if ($gallery_id = $this->Gallery->insert($data)) {
                    $gallery = $this->Gallery->find($gallery_id);
                    $this->load->vars('gallery', $gallery);
                } else {
                    echo 'Failed';
                }
            }
            $this->load->view('Layout', $this->view_sp);
        } else {
            redirect(base_url() . 'index.php/Auth');
        }
    }

    public function addPhoto($gallery_id = null) {
        $this->view_sp['mainPane'] = array('Galleries/add_photo');
        $this->view['aside'] = array();
        $this->load->model('Gallery');
        $galleries = $this->Gallery->view();


        if ($this->Authorizer->authorize(array(ADMIN, DIRECTOR))) {
            if ($_SESSION['user_type'] == ADMIN) {
                $this->view_sp['mainNav'] = 'Admin/adminNav';
                $this->view_sp['aside'] = array('_newsSlider', 'Admin/_sideNav1');
            } else if ($_SESSION['user_type'] == DIRECTOR) {
                $this->view_sp['mainNav'] = '';
            }

            $this->load->library('form_validation');
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

            if ($gallery_id == null) {
                $this->load->vars('galleries', $galleries['data']);
                $this->form_validation->set_rules('gallery', 'Gallery', 'trim|required');
            } else {
                $gallery = $this->Gallery->find($gallery_id);
                $this->load->vars('gallery', $gallery);
            }
            //$this->form_validation->set_rules('img', 'Photo', 'required');



            $this->load->model('Photo');
            
            if(isset($_FILES['gallery_img'])){
            $data = array('gallery_id' => $gallery_id, 'created' => date("Y-m-d h:i:s", time()));


            if ($photo_id = $this->Photo->insert($data)) {
                $photo = $this->Photo->find($photo_id);
                $this->load->model('Image');
                $img = $this->Image->uploadPhoto($data['gallery_id'] . '_' . $photo_id);
                $this->Photo->edit($photo_id, array('photo_url' => base_url() . 'resource/img/gallery/' . $img, 'photo_thumb' => base_url() . 'resource/img/gallery/thumb-' . $img));
                $photo = $this->Photo->find($photo_id);
                $this->load->vars('photo', $photo);
            } else {
                echo 'Failed';
            }
            }


            $this->load->view('Layout', $this->view_sp);
        } else {
            redirect(base_url() . 'index.php/Auth');
        }
    }

    public function deletePhoto($photo_id, $remove = false) {
        if ($this->Authorizer->authorize(array(ADMIN, DIRECTOR))) {
            if ($_SESSION['user_type'] == ADMIN) {
                $this->view_sp['mainNav'] = 'Admin/adminNav';
                $this->view_sp['aside'] = array('_newsSlider', 'Admin/_sideNav1');
            } else if ($_SESSION['user_type'] == DIRECTOR) {
                $this->view_sp['mainNav'] = '';
            }
            $this->load->model('Photo');
            if ($remove) {
                $photo = $this->Photo->find($photo_id);
                if ($this->Photo->delete($photo_id)) {
                    unlink(str_replace(base_url(), '', $photo['photo_url']));
                    unlink(str_replace(base_url(), '', $photo['photo_thumb']));
                    $this->view_sp['mainPane'] = array('_removeSuccess');
                    $this->load->vars(array('back' => site_url('Galleries/index')));
                } else {
                    echo 'Not Removed';
                }
            } else {
                $this->view_sp['mainPane'] = array('_remove');
                $photo = $this->Photo->find($photo_id);
                $remv = array('data' => 'Photo',
                    'yes_link' => base_url() . 'index.php/Galleries/deletePhoto/' . $photo_id . '/true',
                    'no_link' => base_url() . 'index.php/Galleries',
                    'load' => 'Galleries/photo');

                $this->load->vars('remove', $remv);
                $this->load->vars('photo', $photo);
            }
            $this->load->view('Layout', $this->view_sp);
        } else {
            redirect(base_url() . 'index.php/Auth');
        }
    }

    public function deleteGallery($gallery_id, $remove = false) {
        if ($this->Authorizer->authorize(array(ADMIN, DIRECTOR))) {
            if ($_SESSION['user_type'] == ADMIN) {
                $this->view_sp['mainNav'] = 'Admin/adminNav';
                $this->view_sp['aside'] = array('_newsSlider', 'Admin/_sideNav1');
            } else if ($_SESSION['user_type'] == DIRECTOR) {
                $this->view_sp['mainNav'] = '';
            }
            $this->load->model('Gallery');
            $this->load->model('Photo');
            $photos = $this->Photo->photoListOfGallery($gallery_id);
            if ($remove) {
                if ($photos != null)
                    foreach ($photos as $photo) {
                        unlink(str_replace(base_url(), '', $photo['photo_url']));
                        unlink(str_replace(base_url(), '', $photo['photo_thumb']));
                    }
                if ($this->Gallery->delete($gallery_id)) {
                    $this->view_sp['mainPane'] = array('_removeSuccess');
                    $this->load->vars(array('back' => site_url('Galleries/index')));
                } else {
                    echo 'Not Removed';
                }
            } else {
                $this->view_sp['mainPane'] = array('_remove');
                $gallery = $this->Gallery->find($gallery_id);
                $remv = array('data' => 'Gallery',
                    'yes_link' => base_url() . 'index.php/Galleries/deleteGallery/' . $gallery_id . '/true',
                    'no_link' => base_url() . 'index.php/Galleries',
                    'load' => 'Galleries/gallery');

                $this->load->vars('remove', $remv);
                $this->load->vars('gallery', $gallery);
            }
            $this->load->view('Layout', $this->view_sp);
        } else {
            redirect(base_url() . 'index.php/Auth');
        }
    }

}
