<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Admin
 *
 * @author Dagi
 */
class Admin extends CI_Controller{
    //put your code here
    private $view_sp = array('mainNav' => 'Admin/adminNav','mainPane' => array(), 'jumbotron' => array(), 'aside' => array());

    public function __construct() {
        parent::__construct();
        session_start();
    }
    
    public function index(){
        
        if($this->Authorizer->authorize(array('Administrator'))){
            
            $this->load->view('Layout', $this->view_sp);
        }else{
            
        }
    }
}
