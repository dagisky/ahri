<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Home
 *
 * @author Dagi
 */
class Home extends CI_Controller {

    //put your code here
    private $view_sp = array('mainNav' => 'mainNav','mainPane' => array('_defaultMain'), 'jumbotron' => array('_carousel1'), 'aside' => array('_newsSlider', '_sideNav'));

    function __construct() {
        parent::__construct();
        session_start();
    }

    public function index() {
       
        $this->load->model('Team');   
        $this->load->view('Layout', $this->view_sp);
        
    }

}
