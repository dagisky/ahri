<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MailingList
 *
 * @author Dagi
 */
class Mailing extends CI_Controller {

    private $view_sp = array('mainNav' => 'mainNav', 'mainPane' => array(), 'jumbotron' => array(), 'aside' => array('_newsSlider', '_sideNav'));

//put your code here
    public function __construct() {
        parent::__construct();
        session_start();
    }

    public function add() {

        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        $this->form_validation->set_rules('mailingAddress', 'Email', 'trim|required|valid_email');

        $this->load->model('Mailing_List');

        if ($this->form_validation->run() == FALSE) {
            redirect($_SERVER['HTTP_REFERER']);
        } else {
            $this->load->model('UserAddress');
            $data = array(
                'email' => $this->input->post('mailingAddress'),
                'ip_address' => $this->UserAddress->getUserIP(),
                'created' => date("Y-m-d h:i:s", time())
            );

            if ($email_id = $this->Mailing_List->insert($data)) {
                $email = $this->Mailing_List->find($email_id);
                $this->view_sp['mainPane'] = array('Email/emailAddSuccess');
                $this->load->vars('email', $email);
            } else {
                echo 'Failed';
            }
        }
        $this->load->view('Layout', $this->view_sp);
    }

    public function sendMail() {
        $this->view_sp['mainPane'] = array('Email/sendNewMail');
        $this->view['aside'] = array();
        $this->view_sp['styles'] = array('bootstrap-datetimepicker.min', 'bootstrap3-wysiwyg5');
        $this->view_sp['scripts'] = array('moment', 'bootstrap-datetimepicker.min', 'wysihtml5-0.3.0.min', 'bootstrap3-wysihtml5');

        if ($this->Authorizer->authorize(array(ADMIN, DIRECTOR))) {
            if ($_SESSION['user_type'] == ADMIN) {
                $this->view_sp['mainNav'] = 'Admin/adminNav';
                $this->view_sp['aside'] = array('_newsSlider', 'Admin/_sideNav1');
            } else if ($_SESSION['user_type'] == DIRECTOR) {
                $this->view_sp['mainNav'] = '';
            }
            $this->load->library('form_validation');
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

            $this->form_validation->set_rules('title', 'Research Title', 'trim|required');
            $this->form_validation->set_rules('content', 'Research description', 'trim|required');

            $this->load->library('email');

            if ($this->form_validation->run() == FALSE) {
                $this->view_sp['mainPane'] = array('Email/sendNewMail');
            } else {
                if ($this->input->post('sendAll')) {
                    //if the admin chooses to send this maill to all users of the application
                    $this->load->model('User');
                    $this->load->model('Staff');
                    $this->load->model('Mailing_List');
                    $users = $this->User->view();
                    $staffs = $this->Staff->view();
                    $customers = $this->Mailing_List->view();
                    foreach ($users as $user) {
                        $this->email->clear();
                        $this->email->to($user['email']);
                        $this->email->from('ahri@ethionet.et');
                        $this->email->subject($this->input->post('title'));
                        $this->email->message($this->input->post('content'));
                        if ($this->email->send()) {
                            //echo message sent 
                            $this->load->vars('sent', true);
                        } else {
                            $this->load->vars('sent', false);
                        }
                    }
                    foreach ($staffs as $staff) {
                        $this->email->clear();
                        $this->email->to($user['email']);
                        $this->email->from('ahri@ethionet.et');
                        $this->email->subject($this->input->post('title'));
                        $this->email->message($this->input->post('content'));
                        if ($this->email->send()) {
                            //echo message sent 
                            $this->load->vars('sent', true);
                        } else {
                            $this->load->vars('sent', false);
                        }
                    }
                    foreach ($customers as $customer) {
                        $this->email->clear();
                        $this->email->to($customer['email']);
                        $this->email->from('ahri@ethionet.et');
                        $this->email->subject($this->input->post('title'));
                        $this->email->message($this->input->post('content'));
                        if ($this->email->send()) {
                            //echo message sent 
                            $this->load->vars('sent', true);
                        } else {
                            $this->load->vars('sent', false);
                        }
                    }
                } else {
                    
                }


                if ($news_id = $this->News->insert($data)) {
                    $news = $this->News->find($news_id);
                    $this->load->vars('news', $news);
                } else {
                    echo 'Failed';
                }
            }
            $this->load->view('Layout', $this->view_sp);
        } else {
            redirect(base_url() . 'index.php/Auth');
        }
    }

}
