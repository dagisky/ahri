<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Laboratories
 *
 * @author Dagi
 */
class Laboratories extends CI_Controller {

    private $view_sp = array('mainNav' => 'mainNav', 'mainPane' => array('Laboratories/laboratory_list'), 'jumbotron' => array(), 'aside' => array('_newsSlider', '_sideNav'));

    //put your code here
    public function __construct() {
        parent::__construct();
        session_start();
    }

    public function index($page = null) {
        $per_page = 10;
        if ($this->Authorizer->authorize(array(ADMIN, DIRECTOR))) {
            if ($_SESSION['user_type'] == ADMIN) {
                $this->view_sp['mainNav'] = 'Admin/adminNav';
                $this->view_sp['aside'] = array('_newsSlider', 'Admin/_sideNav1');
            } else if ($_SESSION['user_type'] == DIRECTOR) {
                $this->view_sp['mainNav'] = '';
            }
        }
        $this->load->model('Laboratory');
        $this->load->library('pagination');
        if ($page != null) {
            $page_data = $this->Laboratory->view($per_page, $page);
        } else {
            $page_data = $this->Laboratory->view($per_page);
        }

        $config['base_url'] = site_url('Laboratories/index');
        $config['total_rows'] = $page_data['num_rows'];
        $config['per_page'] = $per_page;
        $config['full_tag_open'] = '<ul class="pager">';
        $config['full_tag_close'] = '</ul>';

        $config['prev_link'] = 'Previous';
        $config['prev_tag_open'] = '<li style="margin-left:5px;">';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = 'Next';
        $config['next_tag_open'] = '<li style="margin-left:5px;">';
        $config['next_tag_close'] = '</li>';
        $this->pagination->initialize($config);

        if ($page_data['num_rows'] > $per_page) {
            $this->load->vars(array('paginator' => $this->pagination->create_links()));
        }

        $this->load->vars('labs', $page_data['data']);
        $this->load->view('Layout', $this->view_sp);
    }

    public function add() {
        $this->view_sp['mainPane'] = array('Laboratories/add_laboratory');
        $this->view['aside'] = array();
        $this->view_sp['scripts'] = array('moment', 'bootstrap-datetimepicker.min', 'wysihtml5-0.3.0.min', 'bootstrap3-wysihtml5');
        $this->view_sp['styles'] = array('bootstrap-datetimepicker.min', 'bootstrap3-wysiwyg5');


        if ($this->Authorizer->authorize(array(ADMIN, DIRECTOR))) {
            if ($_SESSION['user_type'] == ADMIN) {
                $this->view_sp['mainNav'] = 'Admin/adminNav';
                $this->view_sp['aside'] = array('_newsSlider', 'Admin/_sideNav1');
            } else if ($_SESSION['user_type'] == DIRECTOR) {
                $this->view_sp['mainNav'] = '';
            }
            $this->load->library('form_validation');
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

            $this->form_validation->set_rules('lab_name', 'Laboratory Name', 'trim|required');
            $this->form_validation->set_rules('lab_desc', 'Labratory Description', 'trim|required');


            $this->load->model('Laboratory');

            if ($this->form_validation->run() == FALSE) {
                $this->view_sp['mainPane'] = array('Laboratories/add_laboratory');
            } else {
                $data = array(
                    'laboratory_name' => $this->input->post('lab_name'),
                    'description' => $this->input->post('lab_desc')
                );

                if ($lab_id = $this->Laboratory->insert($data)) {
                    $lab = $this->Laboratory->find($lab_id);
                    $this->load->model('Image');
                    $img_url = $this->Image->uploadPhoto($lab_id, 'resource/img/lab', 500);
                    $this->Laboratory->edit($lab_id, array('profile_img' => base_url() . 'resource/img/lab/' . $img_url));
                    $lab = $this->Laboratory->find($lab_id);
                    $this->load->vars('lab', $lab);
                } else {
                    echo 'Failed';
                }
            }
            $this->load->view('Layout', $this->view_sp);
        } else {
            redirect(base_url() . 'index.php/Auth');
        }
    }

    public function edit($lab_id) {
        $this->view_sp['mainPane'] = array('Laboratories/edit_laboratory');
        $this->view['aside'] = array();
        $this->view_sp['scripts'] = array('moment', 'bootstrap-datetimepicker.min', 'wysihtml5-0.3.0.min', 'bootstrap3-wysihtml5');
        $this->view_sp['styles'] = array('bootstrap-datetimepicker.min', 'bootstrap3-wysiwyg5');


        if ($this->Authorizer->authorize(array(ADMIN, DIRECTOR))) {
            if ($_SESSION['user_type'] == ADMIN) {
                $this->view_sp['mainNav'] = 'Admin/adminNav';
                $this->view_sp['aside'] = array('_newsSlider', 'Admin/_sideNav1');
            } else if ($_SESSION['user_type'] == DIRECTOR) {
                $this->view_sp['mainNav'] = '';
            }
            $this->load->library('form_validation');
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

            $this->form_validation->set_rules('labName', 'Laboratory Name', 'trim|required');
            $this->form_validation->set_rules('labDesc', 'Labratory Description', 'trim|required');


            $this->load->model('Laboratory');

            if ($this->form_validation->run() == FALSE) {
                $this->view_sp['mainPane'] = array('Laboratories/edit_laboratory');
                $lab = $this->Laboratory->find($lab_id);
                $this->load->vars('lab', $lab);
            } else {
                $data = array(
                    'laboratory_name' => $this->input->post('labName'),
                    'description' => $this->input->post('labDesc')
                );


                if ($this->Laboratory->edit($lab_id, $data)) {
                    $lab = $this->Laboratory->find($lab_id);

                    if (isset($_FILES['gallery_img']) && $_FILES['gallery_img']['name'] != null) {
                        if ($lab['profile_img'] != null) {
                            unlink(str_replace(base_url() . "resource/img/lab/", "resource/img/lab/", $lab['profile_img']));
                            unlink(str_replace(base_url() . "resource/img/lab/", "resource/img/lab/", str_replace("lab/", "lab/thumb-", $lab['profile_img'])));
                        }
                        //echo $_FILES['gallery_img']['error'];
                        //print_r($_FILES['gallery_img']);
                        $this->load->model('Image');
                        $img_url = $this->Image->uploadPhoto($lab_id, 'resource/img/lab', 500);
                        $this->Laboratory->edit($lab_id, array('profile_img' => base_url() . 'resource/img/lab/' . $img_url));
                        $this->Laboratory->edit($lab_id, array('profile_img' => base_url() . 'resource/img/lab/' . $img_url));
                        $lab = $this->Laboratory->find($lab_id);
                    }
                } else {
                    echo 'Failed';
                }
                $this->load->vars('lab', $lab);
            }
            $this->load->view('Layout', $this->view_sp);
        } else {
            redirect(base_url() . 'index.php/Auth');
        }
    }

    public function delete($lab_id, $remove = false) {
        if ($this->Authorizer->authorize(array(ADMIN, DIRECTOR))) {
            if ($_SESSION['user_type'] == ADMIN) {
                $this->view_sp['mainNav'] = 'Admin/adminNav';
                $this->view_sp['aside'] = array('_newsSlider', 'Admin/_sideNav1');
            } else if ($_SESSION['user_type'] == DIRECTOR) {
                $this->view_sp['mainNav'] = '';
            }
            $this->load->model('Laboratory');
            if ($remove) {
                $lab = $this->Laboratory->find($lab_id);
                if ($this->Laboratory->delete($lab_id)) {
                    unlink(str_replace(base_url(), "", $lab['profile_img']));
                    unlink(str_replace(base_url().'resource/img/lab/', "resource/img/lab/thumb-", $lab['profile_img']));
                    $this->view_sp['mainPane'] = array('_removeSuccess');
                    $this->load->vars(array('back' => site_url('Laboratories/index')));
                } else {
                    echo 'Not Removed';
                }
            } else {
                $this->view_sp['mainPane'] = array('_remove');
                $lab = $this->Laboratory->find($lab_id);
                $remv = array('data' => 'Laboratory',
                    'yes_link' => base_url() . 'index.php/Laboratories/delete/' . $lab_id . '/true',
                    'no_link' => base_url() . 'index.php/Laboratories',
                    'load' => 'Laboratories/laboratory');

                $this->load->vars('remove', $remv);
                $this->load->vars('lab', $lab);
            }
            $this->load->view('Layout', $this->view_sp);
        } else {
            redirect(base_url() . 'index.php/Auth');
        }
    }

}
