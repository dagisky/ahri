<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Publications
 *
 * @author Dagi
 */
class Publications extends CI_Controller {

    //put your code here
    private $view_sp = array('mainNav' => 'mainNav', 'mainPane' => array('Publications/publication_list'), 'jumbotron' => array(), 'aside' => array('_newsSlider', '_sideNav'));

    public function __construct() {
        parent::__construct();
        session_start();
    }

    public function index($page = null) {
        if ($this->Authorizer->authorize(array(ADMIN, DIRECTOR))) {
            if ($_SESSION['user_type'] == ADMIN) {
                $this->view_sp['mainNav'] = 'Admin/adminNav';
                $this->view_sp['aside'] = array('_newsSlider', 'Admin/_sideNav1');
            } else if ($_SESSION['user_type'] == DIRECTOR) {
                $this->view_sp['mainNav'] = '';
            }
        }
        $per_page = 10;
        $this->load->model('Publication');
        $this->load->library('pagination');
        if ($page != null) {
            $page_data = $this->Publication->view($per_page, $page);
        } else {
            $page_data = $this->Publication->view($per_page);
        }

        $config['base_url'] = site_url('Publications/index');
        $config['total_rows'] = $page_data['num_rows'];
        $config['per_page'] = $per_page;
        $config['full_tag_open'] = '<ul class="pager">';
        $config['full_tag_close'] = '</ul>';

        $config['prev_link'] = 'Previous';
        $config['prev_tag_open'] = '<li style="margin-left:5px;">';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = 'Next';
        $config['next_tag_open'] = '<li style="margin-left:5px;">';
        $config['next_tag_close'] = '</li>';
        $this->pagination->initialize($config);

        if ($page_data['num_rows'] > $per_page) {
            $this->load->vars(array('paginator' => $this->pagination->create_links()));
        }

        $this->load->vars('publications', $page_data['data']);
        $this->load->view('Layout', $this->view_sp);
    }

    public function add() {
        $this->view_sp['mainPane'] = array('Publications/add_publication');
        $this->view['aside'] = array();
        $this->view_sp['styles'] = array('bootstrap-datetimepicker.min', 'bootstrap3-wysiwyg5');
        $this->view_sp['scripts'] = array('moment', 'bootstrap-datetimepicker.min', 'wysihtml5-0.3.0.min', 'bootstrap3-wysihtml5');


        if ($this->Authorizer->authorize(array(ADMIN, DIRECTOR))) {
            if ($_SESSION['user_type'] == ADMIN) {
                $this->view_sp['mainNav'] = 'Admin/adminNav';
                $this->view_sp['aside'] = array('_newsSlider', 'Admin/_sideNav1');
            } else if ($_SESSION['user_type'] == DIRECTOR) {
                $this->view_sp['mainNav'] = '';
            }
            $this->load->library('form_validation');
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

            $this->form_validation->set_rules('publication_title', 'Publication Title', 'trim|required');
            $this->form_validation->set_rules('publication_url', 'Publication URL', 'trim|required');
            $this->form_validation->set_rules('pub_synapse', 'Publication Synapse', 'trim|required');
            $this->form_validation->set_rules('pubDate', 'Published Date', 'trim|required');

            $this->load->model('Publication');

            if ($this->form_validation->run() == FALSE) {
                $this->view_sp['mainPane'] = array('Publications/add_publication');
            } else {
                $this->load->model('TimeF');
                 $data = array(
                    'publication_title' => $this->input->post('publication_title'),
                    'publication_synapse' => $this->input->post('pub_synapse'),
                    'publication_url' => $this->input->post('publication_url'),
                    'published_date' => $this->TimeF->HumanToMysql($this->input->post('pubDate'))
                );

                if ($research_id = $this->Publication->insert($data)) {
                    $publication = $this->Publication->find($research_id);
                    $this->load->vars('publication', $publication);
                } else {
                    echo 'Failed';
                }
            }
            $this->load->view('Layout', $this->view_sp);
        } else {
            redirect(base_url() . 'index.php/Auth');
        }
    }

    public function edit($publication_id) {
        $this->view_sp['mainPane'] = array('Publications/edit_publication');
        $this->view['aside'] = array();
        $this->view_sp['styles'] = array('bootstrap-datetimepicker.min', 'bootstrap3-wysiwyg5');
        $this->view_sp['scripts'] = array('moment', 'bootstrap-datetimepicker.min', 'wysihtml5-0.3.0.min', 'bootstrap3-wysihtml5');


        if ($this->Authorizer->authorize(array(ADMIN, DIRECTOR))) {
            if ($_SESSION['user_type'] == ADMIN) {
                $this->view_sp['mainNav'] = 'Admin/adminNav';
                $this->view_sp['aside'] = array('_newsSlider', 'Admin/_sideNav1');
            } else if ($_SESSION['user_type'] == DIRECTOR) {
                $this->view_sp['mainNav'] = '';
            }
            $this->load->library('form_validation');
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

            $this->form_validation->set_rules('publication_title', 'Publication Title', 'trim|required');
            $this->form_validation->set_rules('publication_url', 'Publication URL', 'trim|required');
            $this->form_validation->set_rules('pub_synapse', 'Publication Synapse', 'trim|required');
            $this->form_validation->set_rules('pubDate', 'Publication Date', 'trim|required');

            $this->load->model('Publication');
            $this->load->model('TimeF');
            if ($this->form_validation->run() == FALSE) {
                $publication = $this->Publication->find($publication_id);
                $this->load->vars('publication', $publication);
                $this->view_sp['mainPane'] = array('Publications/edit_publication');
            } else {
                $data = array(
                    'publication_title' => $this->input->post('publication_title'),
                    'publication_synapse' => $this->input->post('pub_synapse'),
                    'publication_url' => $this->input->post('publication_url'),
                    'published_date' => $this->TimeF->HumanToMysql($this->input->post('pubDate'))
                );

                if ($this->Publication->edit($publication_id, $data)) {
                    $publication = $this->Publication->find($publication_id);
                    $this->load->vars('publication', $publication);
                } else {
                    echo 'Failed';
                }
            }
            $this->load->view('Layout', $this->view_sp);
        } else {
            redirect(base_url() . 'index.php/Auth');
        }
    }

    public function delete($publication_id, $remove = false) {
        if ($this->Authorizer->authorize(array(ADMIN, DIRECTOR))) {
            if ($_SESSION['user_type'] == ADMIN) {
                $this->view_sp['mainNav'] = 'Admin/adminNav';
                $this->view_sp['aside'] = array('_newsSlider', 'Admin/_sideNav1');
            } else if ($_SESSION['user_type'] == DIRECTOR) {
                $this->view_sp['mainNav'] = '';
            }
            $this->load->model('Publication');
            if ($remove) {
                if ($this->Publication->delete($publication_id)) {
                    $this->view_sp['mainPane'] = array('_removeSuccess');
                    $this->load->vars(array('back' => site_url('Publications/index')));
                } else {
                    echo 'Not Removed';
                }
            } else {
                $this->view_sp['mainPane'] = array('_remove');
                $publication = $this->Publication->find($publication_id);
                $remv = array('data' => 'Publication',
                    'yes_link' => base_url() . 'index.php/Publications/delete/' . $publication_id . '/true',
                    'no_link' => base_url() . 'index.php/Publications',
                    'load' => 'Publications/publication');

                $this->load->vars('remove', $remv);
                $this->load->vars('publication', $publication);
            }
            $this->load->view('Layout', $this->view_sp);
        } else {
            redirect(base_url() . 'index.php/Auth');
        }
    }

}
