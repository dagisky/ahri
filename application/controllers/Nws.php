<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of News
 *
 * @author Dagi
 */
class Nws extends CI_Controller {

    //put your code here
    private $view_sp = array('mainNav' => 'mainNav', 'mainPane' => array('News/news_list'), 'jumbotron' => array(), 'aside' => array('_newsSlider', '_sideNav'));

    public function __construct() {
        parent::__construct();
        session_start();
    }

    public function index($page = null) {
        if ($this->Authorizer->authorize(array(ADMIN, DIRECTOR))) {
            if ($_SESSION['user_type'] == ADMIN) {
                $this->view_sp['mainNav'] = 'Admin/adminNav';
                $this->view_sp['aside'] = array('_newsSlider', 'Admin/_sideNav1');
            } else if ($_SESSION['user_type'] == DIRECTOR) {
                $this->view_sp['mainNav'] = '';
            }
        }
        $per_page = 10;
        $this->load->model('News');
        $this->load->library('pagination');
        if ($page != null) {
            $page_data = $this->News->view($per_page, $page);
        } else {
            $page_data = $this->News->view($per_page);
        }

        $config['base_url'] = site_url('Nws/index');
        $config['total_rows'] = $page_data['num_rows'];
        $config['per_page'] = $per_page;
        $config['full_tag_open'] = '<ul class="pager">';
        $config['full_tag_close'] = '</ul>';

        $config['prev_link'] = 'Previous';
        $config['prev_tag_open'] = '<li style="margin-left:5px;">';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = 'Next';
        $config['next_tag_open'] = '<li style="margin-left:5px;">';
        $config['next_tag_close'] = '</li>';
        $this->pagination->initialize($config);

        if ($page_data['num_rows'] > $per_page) {
            $this->load->vars(array('paginator' => $this->pagination->create_links()));
        }

        $this->load->vars('news', $page_data['data']);
        $this->load->view('Layout', $this->view_sp);
    }

    public function add() {
        $this->view_sp['mainPane'] = array('News/add_news');
        $this->view['aside'] = array();
        $this->view_sp['styles'] = array('bootstrap-datetimepicker.min', 'bootstrap3-wysiwyg5');
        $this->view_sp['scripts'] = array('moment', 'bootstrap-datetimepicker.min', 'wysihtml5-0.3.0.min', 'bootstrap3-wysihtml5');

        if ($this->Authorizer->authorize(array(ADMIN, DIRECTOR))) {
            if ($_SESSION['user_type'] == ADMIN) {
                $this->view_sp['mainNav'] = 'Admin/adminNav';
                $this->view_sp['aside'] = array('_newsSlider', 'Admin/_sideNav1');
            } else if ($_SESSION['user_type'] == DIRECTOR) {
                $this->view_sp['mainNav'] = '';
            }
            $this->load->library('form_validation');
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

            $this->form_validation->set_rules('news_title', 'Research Title', 'trim|required');
            $this->form_validation->set_rules('news_content', 'Research description', 'trim|required');

            $this->load->model('News');

            if ($this->form_validation->run() == FALSE) {
                $this->view_sp['mainPane'] = array('News/add_news');
            } else {
                $data = array(
                    'news_title' => $this->input->post('news_title'),
                    'news_content' => $this->input->post('news_content'),
                    'created' => date("Y-m-d h:i:s", time()),
                    'modified' => date("Y-m-d h:i:s", time())
                );

                if ($news_id = $this->News->insert($data)) {
                    $news = $this->News->find($news_id);
                    $this->load->vars('news', $news);
                } else {
                    echo 'Failed';
                }
            }
            $this->load->view('Layout', $this->view_sp);
        } else {
            redirect(base_url() . 'index.php/Auth');
        }
    }

    public function edit($news_id) {
        $this->view_sp['mainPane'] = array('News/edit_news');
        $this->view['aside'] = array();
        $this->view_sp['styles'] = array('bootstrap-datetimepicker.min', 'bootstrap3-wysiwyg5');
        $this->view_sp['scripts'] = array('moment', 'bootstrap-datetimepicker.min', 'wysihtml5-0.3.0.min', 'bootstrap3-wysihtml5');

        if ($this->Authorizer->authorize(array(ADMIN, DIRECTOR))) {
            if ($_SESSION['user_type'] == ADMIN) {
                $this->view_sp['mainNav'] = 'Admin/adminNav';
                $this->view_sp['aside'] = array('_newsSlider', 'Admin/_sideNav1');
            } else if ($_SESSION['user_type'] == DIRECTOR) {
                $this->view_sp['mainNav'] = '';
            }
            $this->load->library('form_validation');
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

            $this->form_validation->set_rules('news_title', 'Research Title', 'trim|required');
            $this->form_validation->set_rules('news_content', 'Research description', 'trim|required');

            $this->load->model('News');
            $news = $this->News->find($news_id);
            
            if ($this->form_validation->run() == FALSE) {                
                $this->load->vars('news', $news);
                $this->view_sp['mainPane'] = array('News/edit_news');
            } else {
                $data = array(
                    'news_title' => $this->input->post('news_title'),
                    'news_content' => $this->input->post('news_content'),
                    'created' => $news['created'],
                    'modified' => date("Y-m-d h:i:s", time())
                );

                if ($this->News->edit($news_id, $data)) {
                    $news = $this->News->find($news_id);
                    $this->load->vars('news', $news);
                } else {
                    echo 'Failed';
                }
            }
            $this->load->view('Layout', $this->view_sp);
        } else {
            redirect(base_url() . 'index.php/Auth');
        }
    }
    
    public function delete($news_id, $remove = false){
        if ($this->Authorizer->authorize(array(ADMIN, DIRECTOR))) {
            if ($_SESSION['user_type'] == ADMIN) {
                $this->view_sp['mainNav'] = 'Admin/adminNav';
                $this->view_sp['aside'] = array('_newsSlider', 'Admin/_sideNav1');
            } else if ($_SESSION['user_type'] == DIRECTOR) {
                $this->view_sp['mainNav'] = '';
            }
            $this->load->model('News');
            if ($remove) {
                if ($this->News->delete($news_id)) {
                    $this->view_sp['mainPane'] = array('_removeSuccess');
                    $this->load->vars(array('back' => site_url('Nws/index')));
                } else {
                    echo 'Not Removed';
                }
            } else {
                $this->view_sp['mainPane'] = array('_remove');
                $news = $this->News->find($news_id);
                $remv = array('data' => 'News',
                    'yes_link' => base_url() . 'index.php/Nws/delete/' . $news_id . '/true',
                    'no_link' => base_url() . 'index.php/Nws',
                    'load' => 'News/news');

                $this->load->vars('remove', $remv);
                $this->load->vars('news', $news);
            }
            $this->load->view('Layout', $this->view_sp);
        } else {
            redirect(base_url() . 'index.php/Auth');
        }
    }

}
