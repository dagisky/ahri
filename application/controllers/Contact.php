<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Contact
 *
 * @author Dagi
 */
class Contact extends CI_Controller {

    private $view_sp = array('mainNav' => 'mainNav', 'mainPane' => array(), 'jumbotron' => array(), 'aside' => array('_newsSlider', '_sideNav'));

//put your code here
    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->view_sp['mainPane'] = array('Email/sendNewMail');
        $this->view['aside'] = array();
        $this->view_sp['styles'] = array('bootstrap-datetimepicker.min', 'bootstrap3-wysiwyg5');
        $this->view_sp['scripts'] = array('moment', 'bootstrap-datetimepicker.min', 'wysihtml5-0.3.0.min', 'bootstrap3-wysihtml5');



        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        $this->form_validation->set_rules('title', 'Research Title', 'trim|required');
        $this->form_validation->set_rules('content', 'Research description', 'trim|required');

        $this->load->library('email');

        if ($this->form_validation->run() == FALSE) {
            $this->view_sp['mainPane'] = array('Contact/sendMail');
        } else {

            //if the admin chooses to send this maill to all users of the application
            $this->load->model('User');            
            $users = $this->User->viewAdmins();
            
            foreach ($users as $user) {
                $this->email->clear();
                $this->email->to($user['email']);
                $this->email->from('ahri@ethionet.et');
                $this->email->subject($this->input->post('title'));
                $this->email->message($this->input->post('content'));
                if ($this->email->send()) {
                    //echo message sent 
                    $this->load->vars('sent', true);
                } else {
                    $this->load->vars('sent', false);
                }
            }




            if ($news_id = $this->News->insert($data)) {
                $news = $this->News->find($news_id);
                $this->load->vars('news', $news);
            } else {
                echo 'Failed';
            }
        }
        $this->load->view('Layout', $this->view_sp);
    }

}
