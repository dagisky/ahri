<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of About
 *
 * @author Dagi
 */
class About extends CI_Controller {

    //put your code here
    private $view_sp = array('mainNav' => 'mainNav', 'mainPane' => array('_about'), 'jumbotron' => array(), 'aside' => array('_newsSlider', '_sideNav'));

    public function __construct() {
        parent::__construct();
        session_start();
    }

    public function index($info_id = null) {
        if ($this->Authorizer->authorize(array(ADMIN, DIRECTOR))) {
            if ($_SESSION['user_type'] == ADMIN) {
                $this->view_sp['mainNav'] = 'Admin/adminNav';
                $this->view_sp['aside'] = array('_newsSlider', 'Admin/_sideNav1');
            } else if ($_SESSION['user_type'] == DIRECTOR) {
                $this->view_sp['mainNav'] = 'Admin/adminNav';
                $this->view_sp['aside'] = array('_newsSlider', 'Admin/_sideNav1');
            }
            $this->view_sp['aside'] = array('_newsSlider', 'Admin/_sideNav1');
        }
        $this->load->model('AboutAhri');
        if ($info_id == null || $info_id == 8) {
            $info_id = 8;
            $info = $this->AboutAhri->find($info_id);
            $this->view_sp['mainPane'] = array('About/ahriProfile', 'About/_ahriRes');
            $this->load->vars('info', $info);
        } else {
            $info = $this->AboutAhri->find($info_id);
            $this->view_sp['mainPane'] = array('About/ahriProfile');
            $this->load->vars('info', $info);
        }
        $this->load->view('Layout', $this->view_sp);
    }

    public function edit($id) {
        $this->view_sp['mainPane'] = array('About/edit');
        $this->view['aside'] = array();
        $this->view_sp['styles'] = array('bootstrap-datetimepicker.min', 'bootstrap3-wysiwyg5');
        $this->view_sp['scripts'] = array('moment', 'bootstrap-datetimepicker.min', 'wysihtml5-0.3.0.min', 'bootstrap3-wysihtml5');

        if ($this->Authorizer->authorize(array(ADMIN, DIRECTOR))) {
            if ($_SESSION['user_type'] == ADMIN) {
                $this->view_sp['mainNav'] = 'Admin/adminNav';
            } else if ($_SESSION['user_type'] == DIRECTOR) {
                $this->view_sp['mainNav'] = '';
            }
            $this->load->library('form_validation');
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

            $this->form_validation->set_rules('title', 'Title', 'trim|required');
            $this->form_validation->set_rules('detail', 'Detail', 'trim|required');

            $this->load->model('AboutAhri');
            $info = $this->AboutAhri->find($id);

            if ($this->form_validation->run() == FALSE) {
                $this->load->vars('info', $info);
                $this->view_sp['mainPane'] = array('About/edit');
            } else {
                $data = array(
                    'title' => $this->input->post('title'),
                    'detail' => $this->input->post('detail'),
                    'created' => $info['created'],
                    'modified' => date("Y-m-d h:i:s", time())
                );

                if ($this->AboutAhri->edit($id, $data)) {
                    $info = $this->AboutAhri->find($id);
                    $this->load->vars('info', $info);
                } else {
                    echo 'Failed';
                }
            }
            $this->load->view('Layout', $this->view_sp);
        } else {
            redirect(base_url() . 'index.php/Auth');
        }
    }

}
