<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Trainings
 *
 * @author Dagi
 */
class Trainings extends CI_Controller {

    //put your code here
    private $view_sp = array('mainNav' => 'mainNav', 'mainPane' => array(), 'jumbotron' => array(), 'aside' => array('_newsSlider', '_sideNav'));

    public function __construct() {
        parent::__construct();
        session_start();
    }

    public function index($page = null) {
        if ($this->Authorizer->authorize(array(ADMIN, DIRECTOR))) {
            if ($_SESSION['user_type'] == ADMIN) {
                $this->view_sp['mainNav'] = 'Admin/adminNav';
                $this->view_sp['aside'] = array('_newsSlider', 'Admin/_sideNav1');
            } else if ($_SESSION['user_type'] == DIRECTOR) {
                $this->view_sp['mainNav'] = '';
            }
        }
        $this->view_sp['mainPane'] = array('Trainings/training_list');
        $per_page = 10;
        $this->load->model('Training');
        $this->load->library('pagination');
        if ($page != null) {
            $page_data = $this->Training->view($per_page, $page);
        } else {
            $page_data = $this->Training->view($per_page);
        }

        $config['base_url'] = site_url('Trainings/index');
        $config['total_rows'] = $page_data['num_rows'];
        $config['per_page'] = $per_page;
        $config['full_tag_open'] = '<ul class="pager">';
        $config['full_tag_close'] = '</ul>';

        $config['prev_link'] = 'Previous';
        $config['prev_tag_open'] = '<li style="margin-left:5px;">';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = 'Next';
        $config['next_tag_open'] = '<li style="margin-left:5px;">';
        $config['next_tag_close'] = '</li>';
        $this->pagination->initialize($config);

        if ($page_data['num_rows'] > $per_page) {
            $this->load->vars(array('paginator' => $this->pagination->create_links()));
        }

        $this->load->vars('trainings', $page_data['data']);
        $this->load->view('Layout', $this->view_sp);
    }

    public function UpComingTraining($page = null) {
        if ($this->Authorizer->authorize(array(ADMIN, DIRECTOR))) {
            if ($_SESSION['user_type'] == ADMIN) {
                $this->view_sp['mainNav'] = 'Admin/adminNav';
                $this->view_sp['aside'] = array('_newsSlider', 'Admin/_sideNav1');
            } else if ($_SESSION['user_type'] == DIRECTOR) {
                $this->view_sp['mainNav'] = '';
            }
        }
        $this->view_sp['mainPane'] = array('Trainings/training_list');
        $per_page = 10;
        $this->load->model('Training');
        $this->load->library('pagination');
        $this->load->model('TimeF');
        if ($page != null) {
            $page_data = $this->Training->fromDate(date("Y-m-d h:i:s", time()), $per_page, $page);
        } else {
            $page_data = $this->Training->fromDate(date("Y-m-d h:i:s", time()),$per_page);
        }

        $config['base_url'] = site_url('Trainings/index');
        $config['total_rows'] = $page_data['num_rows'];
        $config['per_page'] = $per_page;
        $config['full_tag_open'] = '<ul class="pager">';
        $config['full_tag_close'] = '</ul>';

        $config['prev_link'] = 'Previous';
        $config['prev_tag_open'] = '<li style="margin-left:5px;">';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = 'Next';
        $config['next_tag_open'] = '<li style="margin-left:5px;">';
        $config['next_tag_close'] = '</li>';
        $this->pagination->initialize($config);

        if ($page_data['num_rows'] > $per_page) {
            $this->load->vars(array('paginator' => $this->pagination->create_links()));
        }

        $this->load->vars('trainings', $page_data['data']);
        $this->load->view('Layout', $this->view_sp);
    }

    public function add() {
        $this->view_sp['mainPane'] = array('Trainings/add_training');
        $this->view_sp['aside'] = array();
        $this->view_sp['scripts'] = array('moment', 'bootstrap-datetimepicker.min', 'wysihtml5-0.3.0.min', 'bootstrap3-wysihtml5');
        $this->view_sp['styles'] = array('bootstrap-datetimepicker.min', 'bootstrap3-wysiwyg5');

        if ($this->Authorizer->authorize(array(ADMIN, DIRECTOR))) {
            if ($_SESSION['user_type'] == ADMIN) {
                $this->view_sp['mainNav'] = 'Admin/adminNav';
                $this->view_sp['aside'] = array('_newsSlider', 'Admin/_sideNav1');
            } else if ($_SESSION['user_type'] == DIRECTOR) {
                $this->view_sp['mainNav'] = '';
            }
            $this->load->library('form_validation');
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

            $this->form_validation->set_rules('training_title', 'Training Title', 'trim|required');
            $this->form_validation->set_rules('training_name', 'Trainer Full Name', 'trim|required');
            $this->form_validation->set_rules('trainingDesc', 'Training Description', 'trim|required');
            $this->form_validation->set_rules('start_date', 'Training Starting Date', 'trim|required');
            $this->form_validation->set_rules('end_date', 'Training Ending Date', 'trim|required');

            $this->load->model('Training');

            if ($this->form_validation->run() == FALSE) {
                $this->view_sp['mainPane'] = array('Trainings/add_training');
            } else {
                $this->load->model('TimeF');
                $data = array(
                    'training_title' => $this->input->post('training_title'),
                    'trainer_name' => $this->input->post('training_name'),
                    'trainer_email' => $this->input->post('trainerEmail'),
                    'training_desc' => $this->input->post('trainingDesc'),
                    'training_starting_date' => $this->TimeF->HumanToMysql($this->input->post('start_date')),
                    'training_ending_date' => $this->TimeF->HumanToMysql($this->input->post('end_date')),
                    'created' => date("Y-m-d h:i:s", time()),
                    'modified' => date("Y-m-d h:i:s", time())
                );



                if ($training_id = $this->Training->insert($data)) {
                    $training = $this->Training->find($training_id);
                    $this->load->vars('training', $training);
                }
            }
            $this->load->view('Layout', $this->view_sp);
        } else {
            redirect(base_url() . 'index.php/Auth');
        }
    }

    public function edit($training_id) {
        $this->view_sp['mainPane'] = array('Trainings/edit_training');
        $this->view_sp['aside'] = array();
        $this->view_sp['scripts'] = array('moment', 'bootstrap-datetimepicker.min', 'wysihtml5-0.3.0.min', 'bootstrap3-wysihtml5');
        $this->view_sp['styles'] = array('bootstrap-datetimepicker.min', 'bootstrap3-wysiwyg5');
        if (isset($training_id) && is_numeric($training_id)) {

            if ($this->Authorizer->authorize(array(ADMIN, DIRECTOR))) {
                if ($_SESSION['user_type'] == ADMIN) {
                    $this->view_sp['mainNav'] = 'Admin/adminNav';
                    $this->view_sp['aside'] = array('_newsSlider', 'Admin/_sideNav1');
                } else if ($_SESSION['user_type'] == DIRECTOR) {
                    $this->view_sp['mainNav'] = '';
                }

                $this->load->library('form_validation');
                $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

                $this->form_validation->set_rules('training_title', 'Training Title', 'trim|required');
                $this->form_validation->set_rules('training_name', 'Trainer Full Name', 'trim|required');
                $this->form_validation->set_rules('trainingDesc', 'Training Description', 'trim|required');
                $this->form_validation->set_rules('start_date', 'Training Starting Date', 'trim|required');
                $this->form_validation->set_rules('end_date', 'Training Ending Date', 'trim|required');

                $this->load->model('TimeF');

                $this->load->model('Training');
                if ($this->form_validation->run() == FALSE) {
                    $training = $this->Training->find($training_id);

                    $this->load->vars('training', $training);
                    $this->view_sp['mainPane'] = array('Trainings/edit_training');
                } else {
                    $this->load->model('TimeF');
                    $data = array(
                        'training_title' => $this->input->post('training_title'),
                        'trainer_name' => $this->input->post('training_name'),
                        'trainer_email' => $this->input->post('trainerEmail'),
                        'training_desc' => $this->input->post('trainingDesc'),
                        'training_starting_date' => $this->TimeF->HumanToMysql($this->input->post('start_date')),
                        'training_ending_date' => $this->TimeF->HumanToMysql($this->input->post('end_date')),
                        'created' => date("Y-m-d h:i:s", time()),
                        'modified' => date("Y-m-d h:i:s", time())
                    );



                    if ($this->Training->edit($training_id, $data)) {
                        $training = $this->Training->find($training_id);
                        $this->load->vars('training', $training);
                    }
                }
                $this->load->view('Layout', $this->view_sp);
            } else {
                redirect(base_url() . 'index.php/Auth');
            }
        } else {
            //error you should set the training ID
        }
    }

    public function delete($training_id, $remove = false) {
        if ($this->Authorizer->authorize(array(ADMIN, DIRECTOR))) {
            if ($_SESSION['user_type'] == ADMIN) {
                $this->view_sp['mainNav'] = 'Admin/adminNav';
                $this->view_sp['aside'] = array('_newsSlider', 'Admin/_sideNav1');
            } else if ($_SESSION['user_type'] == DIRECTOR) {
                $this->view_sp['mainNav'] = '';
            }
            $this->load->model('Training');
            if ($remove) {
                if ($this->Training->delete($training_id)) {
                    $this->view_sp['mainPane'] = array('_removeSuccess');
                    $this->load->vars(array('back' => site_url('Trainings/index')));
                } else {
                    echo 'Not Removed';
                }
            } else {
                $this->view_sp['mainPane'] = array('_remove');
                $training = $this->Training->find($training_id);
                $remv = array('data' => 'Training',
                    'yes_link' => base_url() . 'index.php/Trainings/delete/' . $training_id . '/true',
                    'no_link' => base_url() . 'index.php/Trainings',
                    'load' => 'Trainings/training');

                $this->load->vars('remove', $remv);
                $this->load->vars('training', $training);
            }
            $this->load->view('Layout', $this->view_sp);
        } else {
            redirect(base_url() . 'index.php/Auth');
        }
    }

}
