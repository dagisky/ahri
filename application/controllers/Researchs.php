<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Research
 *
 * @author Dagi
 */
class Researchs extends CI_Controller {

    //put your code here
    private $view_sp = array('mainNav' => 'mainNav', 'mainPane' => array('Researchs/research_list'), 'jumbotron' => array(), 'aside' => array('_newsSlider', '_sideNav'));

    public function __construct() {
        parent::__construct();
        session_start();
    }

    public function index($page = null) {
        if ($this->Authorizer->authorize(array(ADMIN, DIRECTOR))) {
            if ($_SESSION['user_type'] == ADMIN) {
                $this->view_sp['mainNav'] = 'Admin/adminNav';
                $this->view_sp['aside'] = array('_newsSlider', 'Admin/_sideNav1');
            } else if ($_SESSION['user_type'] == DIRECTOR) {
                $this->view_sp['mainNav'] = '';
            }
        }
        $per_page = 10;
        $this->load->Model('Research');
        $this->load->library('pagination');
        if ($page != null) {
            $page_data = $this->Research->view($per_page, $page);
        } else {
            $page_data = $this->Research->view($per_page);
        }
        $config['base_url'] = site_url('Researchs/index');
        $config['total_rows'] = $page_data['num_rows'];
        $config['per_page'] = $per_page;
        $config['full_tag_open'] = '<ul class="pager">';
        $config['full_tag_close'] = '</ul>';

        $config['prev_link'] = 'Previous';
        $config['prev_tag_open'] = '<li style="margin-left:5px;">';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = 'Next';
        $config['next_tag_open'] = '<li style="margin-left:5px;">';
        $config['next_tag_close'] = '</li>';
        $this->pagination->initialize($config);

        if ($page_data['num_rows'] > $per_page) {
            $this->load->vars(array('paginator' => $this->pagination->create_links()));
        }

        $this->load->vars('researchs', $page_data['data']);
        $this->load->view('Layout', $this->view_sp);
    }

    public function research($research_id) {
        $this->view_sp['mainPane'] = array();
        $this->view['aside'] = array();
    }

    public function add() {
        $this->view_sp['mainPane'] = array('Researchs/add_research');
        $this->view['aside'] = array();
        $this->view_sp['scripts'] = array('moment', 'bootstrap-datetimepicker.min', 'wysihtml5-0.3.0.min', 'bootstrap3-wysihtml5');
        $this->view_sp['styles'] = array('bootstrap-datetimepicker.min', 'bootstrap3-wysiwyg5');

        if ($this->Authorizer->authorize(array(ADMIN, DIRECTOR))) {
            if ($_SESSION['user_type'] == ADMIN) {
                $this->view_sp['mainNav'] = 'Admin/adminNav';
                $this->view_sp['aside'] = array('_newsSlider', 'Admin/_sideNav1');
            } else if ($_SESSION['user_type'] == DIRECTOR) {
                $this->view_sp['mainNav'] = '';
            }
            $this->load->library('form_validation');
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

            $this->form_validation->set_rules('research_title', 'Research Title', 'trim|required');
            $this->form_validation->set_rules('description', 'Research description', 'trim|required');
            $this->form_validation->set_rules('initDate', 'Research Initiation Date', 'trim|required');
            $this->form_validation->set_rules('dueDate', 'Research Due Date', 'trim|required');

            $this->load->model('Research');
            $this->load->model('TimeF');
            $this->load->model('Research_category');
            $researchCategory = $this->Research_category->view();
            $this->load->vars('researchCatgories', $researchCategory['data']);

            if ($this->form_validation->run() == FALSE) {
                $this->view_sp['mainPane'] = array('Researchs/add_research');
            } else {
                $data = array(
                    'research_title' => $this->input->post('research_title'),
                    'category_id' => $this->input->post('research_category'),
                    'description' => $this->input->post('description'),
                    'inititation_date' => $this->TimeF->HumanToMysql($this->input->post('initDate')),
                    'due_date' => $this->TimeF->HumanToMysql($this->input->post('dueDate'))
                );

                if ($research_id = $this->Research->insert($data)) {
                    $research = $this->Research->find($research_id);
                    $this->load->vars('research', $research);
                } else {
                    echo 'Failed';
                }
            }
            $this->load->view('Layout', $this->view_sp);
        } else {
            redirect(base_url() . 'index.php/Auth');
        }
    }

    public function edit($research_id) {
        $this->view_sp['mainPane'] = array('Researchs/edit_research');
        $this->view['aside'] = array();
        $this->view_sp['scripts'] = array('moment', 'bootstrap-datetimepicker.min', 'wysihtml5-0.3.0.min', 'bootstrap3-wysihtml5');
        $this->view_sp['styles'] = array('bootstrap-datetimepicker.min', 'bootstrap3-wysiwyg5');

        if ($this->Authorizer->authorize(array(ADMIN, DIRECTOR))) {
            if ($_SESSION['user_type'] == ADMIN) {
                $this->view_sp['mainNav'] = 'Admin/adminNav';
                $this->view_sp['aside'] = array('_newsSlider', 'Admin/_sideNav1');
            } else if ($_SESSION['user_type'] == DIRECTOR) {
                $this->view_sp['mainNav'] = '';
            }
            $this->load->library('form_validation');
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

            $this->form_validation->set_rules('research_title', 'Research Title', 'trim|required');
            $this->form_validation->set_rules('description', 'Research description', 'trim|required');
            $this->form_validation->set_rules('initDate', 'Research Initiation Date', 'trim|required');
            $this->form_validation->set_rules('dueDate', 'Research Due Date', 'trim|required');

            $this->load->model('Research');
            $this->load->model('Research_category');
            $researchCategory = $this->Research_category->view();
            $this->load->vars('researchCatgories', $researchCategory['data']);
            $this->load->model('TimeF');

            if ($this->form_validation->run() == FALSE) {
                $research = $this->Research->find($research_id);
                $this->load->vars('research', $research);
                $this->view_sp['mainPane'] = array('Researchs/edit_research');
            } else {
                $data = array(
                    'research_title' => $this->input->post('research_title'),
                    'category_id' => $this->input->post('research_category'),
                    'description' => $this->input->post('description'),
                    'inititation_date' => $this->TimeF->HumanToMysql($this->input->post('initDate')),
                    'due_date' => $this->TimeF->HumanToMysql($this->input->post('dueDate'))
                );

                if ($this->Research->edit($research_id, $data)) {
                    $research = $this->Research->find($research_id);
                    $this->load->vars('research', $research);
                } else {
                    echo 'Failed';
                }
            }
            $this->load->view('Layout', $this->view_sp);
        } else {
            redirect(base_url() . 'index.php/Auth');
        }
    }

    public function delete($research_id, $remove = false) {
        if ($this->Authorizer->authorize(array(ADMIN, DIRECTOR))) {
            if ($_SESSION['user_type'] == ADMIN) {
                $this->view_sp['mainNav'] = 'Admin/adminNav';
                $this->view_sp['aside'] = array('_newsSlider', 'Admin/_sideNav1');
            } else if ($_SESSION['user_type'] == DIRECTOR) {
                $this->view_sp['mainNav'] = '';
            }
            $this->load->model('Research');
            $this->load->model('Research_category');
            if ($remove) {
                if ($this->Research->delete($research_id)) {
                    $this->view_sp['mainPane'] = array('_removeSuccess');
                    $this->load->vars(array('back' => site_url('Researchs/index')));
                } else {
                    echo 'Not Removed';
                }
            } else {
                $this->view_sp['mainPane'] = array('_remove');
                $research = $this->Research->find($research_id);
                $remv = array('data' => 'Training',
                    'yes_link' => base_url() . 'index.php/Researchs/delete/' . $research_id . '/true',
                    'no_link' => base_url() . 'index.php/Researchs',
                    'load' => 'Researchs/research');

                $this->load->vars('remove', $remv);
                $this->load->vars('research', $research);
            }
            $this->load->view('Layout', $this->view_sp);
        } else {
            redirect(base_url() . 'index.php/Auth');
        }
    }

}
