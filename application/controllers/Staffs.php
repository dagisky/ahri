<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Staff
 *
 * @author Dagi
 */
class Staffs extends CI_Controller {

    //put your code here
    private $view_sp = array('mainNav' => 'mainNav', 'mainPane' => array('Staffs/researcher_list'), 'jumbotron' => array(), 'aside' => array('_newsSlider', '_sideNav'),
        'styles' => array('bootstrap-datetimepicker.min', 'bootstrap3-wysiwyg5'), 'scripts' => array('moment', 'bootstrap-datetimepicker.min', 'wysihtml5-0.3.0.min', 'bootstrap3-wysihtml5'));

    public function __construct() {
        parent::__construct();
        session_start();
    }

    public function index() {
        $this->load->model('Staff');
        $staffs = $this->Staff->view();

        $this->load->view('Layout', $this->view_sp);
    }

    public function researchers() {
        if ($this->Authorizer->authorize(array(ADMIN, DIRECTOR))) {
            if ($_SESSION['user_type'] == ADMIN) {
                $this->view_sp['mainNav'] = 'Admin/adminNav';
                $this->view_sp['aside'] = array('_newsSlider', 'Admin/_sideNav1');
            } else if ($_SESSION['user_type'] == DIRECTOR) {
                $this->view_sp['mainNav'] = 'Admin/adminNav';
                $this->view_sp['aside'] = array('_newsSlider', 'Admin/_sideNav1');
            }
        }
        $this->load->model('Researcher');
        $seniorScientist = $this->Researcher->viewSeniorScientist();
        $postdoctoralScientist = $this->Researcher->viewPostdoctoralScientist();
        $researchers = $this->Researcher->viewResearchers();
        $assistantResearchers = $this->Researcher->viewAssistantResearchers();
        $researchAssistant = $this->Researcher->viewResearchAssistant();
        $this->load->model('Staff');
        $this->load->vars('seniorScientists', $seniorScientist);
        $this->load->vars('postdoctoralScientists', $postdoctoralScientist);
        $this->load->vars('researchers', $researchers);
        $this->load->vars('assistantResearchers', $assistantResearchers);
        $this->load->vars('researchAssistant', $researchAssistant);

        $this->load->view('Layout', $this->view_sp);
    }

    public function Laboratory() {
        if ($this->Authorizer->authorize(array(ADMIN, DIRECTOR))) {
            if ($_SESSION['user_type'] == ADMIN) {
                $this->view_sp['mainNav'] = 'Admin/adminNav';
                $this->view_sp['aside'] = array('_newsSlider', 'Admin/_sideNav1');
            } else if ($_SESSION['user_type'] == DIRECTOR) {
                $this->view_sp['mainNav'] = '';
            }
        }
        $this->view_sp['mainPane'] = array('Staffs/laboratory_staff_list');
        $this->load->model('Staff');
        $this->load->model('Laboratory_staff');
        $lab_staffs = $this->Laboratory_staff->view();
        $this->load->vars('lab_staffs', $lab_staffs);
        $this->load->view('Layout', $this->view_sp);
    }

    public function DataStat() {
        if ($this->Authorizer->authorize(array(ADMIN, DIRECTOR))) {
            if ($_SESSION['user_type'] == ADMIN) {
                $this->view_sp['mainNav'] = 'Admin/adminNav';
                $this->view_sp['aside'] = array('_newsSlider', 'Admin/_sideNav1');
            } else if ($_SESSION['user_type'] == DIRECTOR) {
                $this->view_sp['mainNav'] = '';
            }
        }
        $this->view_sp['mainPane'] = array('Staffs/data_stat_staff_list');
        $this->load->model('Staff');
        $this->load->model('DataAndStatistics_staff');
        $data_stat_staffs = $this->DataAndStatistics_staff->view();
        $this->load->vars('data_stat_staffs', $data_stat_staffs);
        $this->load->view('Layout', $this->view_sp);
    }

    public function Other() {
        if ($this->Authorizer->authorize(array(ADMIN, DIRECTOR))) {
            if ($_SESSION['user_type'] == ADMIN) {
                $this->view_sp['mainNav'] = 'Admin/adminNav';
                $this->view_sp['aside'] = array('_newsSlider', 'Admin/_sideNav1');
            } else if ($_SESSION['user_type'] == DIRECTOR) {
                $this->view_sp['mainNav'] = '';
            }
        }
        $this->view_sp['mainPane'] = array('Staffs/other_staff_list');
        $this->load->model('Staff');
        $this->load->model('Other_staff');
        $other_staff = $this->Other_staff->view();
        $this->load->vars('other_staffs', $other_staff);
        $this->load->view('Layout', $this->view_sp);
    }

    public function add() {
        $this->view_sp['mainPane'] = array('Staffs/add_staff');
        $this->view['aside'] = array();

        if ($this->Authorizer->authorize(array(ADMIN, DIRECTOR))) {
            if ($_SESSION['user_type'] == ADMIN) {
                $this->view_sp['mainNav'] = 'Admin/adminNav';
                $this->view_sp['aside'] = array('_newsSlider', 'Admin/_sideNav1');
            } else if ($_SESSION['user_type'] == DIRECTOR) {
                $this->view_sp['mainNav'] = '';
            }
            $this->load->library('form_validation');
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

            $this->form_validation->set_rules('fname', 'First Name', 'trim|required');
            $this->form_validation->set_rules('lname', 'Last Name', 'trim|required');
            $this->form_validation->set_rules('sex', 'Sex', 'trim|required');
            $this->form_validation->set_rules('email', 'Email', 'trim|required');
            $this->form_validation->set_rules('dob', 'Date of Birth', 'trim|required');
            $this->form_validation->set_rules('staffType', 'Staff Type', 'trim|required');


            $this->load->model('Staff');


            if ($this->form_validation->run() == FALSE) {
                $this->view_sp['mainPane'] = array('Staffs/add_staff');
            } else {
                $this->load->model('TimeF');
                $data = array(
                    'fname' => $this->input->post('fname'),
                    'lname' => $this->input->post('lname'),
                    'sex' => $this->input->post('sex'),
                    'email' => $this->input->post('email'),
                    'dob' => $this->TimeF->HumanToMysql($this->input->post('dob')),
                    'staff_type' => $this->input->post('staffType'),
                    'created' => date("Y-m-d h:i:s", time()),
                    'modified' => date("Y-m-d h:i:s", time())
                );
                if ($staff_id = $this->Staff->insert($data)) {
                    $staff = $this->Staff->find($staff_id);
                    if ($this->input->post('staffType') == STAFF_RESEARCHER) {
                        $this->form_validation->set_rules('researcher_level', 'Researchers Position', 'trim|required');
                        $this->form_validation->set_rules('researcher_title', 'Researchers Title', 'trim|required');
                        $this->form_validation->set_rules('edu_level', 'Education Level', 'trim|required');
                        if ($this->form_validation->run() == FALSE) {
                            $this->view_sp['mainPane'] = array('Staffs/add_staff');
                        } else {
                            $data = array(
                                'researcher_title' => $this->input->post('researcher_title'),
                                'staff_id' => $staff_id,
                                'researcher_level' => $this->input->post('researcher_level'),
                                'edu_level' => $this->input->post('edu_level'),
                                'biography' => $this->input->post('biography')
                            );
                            $this->load->model('Researcher');
                            if (!$this->Researcher->insert($data)) {
                                // if the researcher insertion failed you should rol back 
                            }
                            $config['upload_path'] = 'resource/img/staff/';
                            $config['allowed_types'] = 'gif|jpg|png';
                            $config['file_name'] = $staff['fname'] . '_' . $staff_id;
                            //$config['max_size'] = '100';
                            //$config['max_width'] = '1024';
                            // $config['max_height'] = '768';
                            $this->load->library('upload', $config);
                            if ($_FILES['profile_img']['name'] != null)
                                if (!$this->upload->do_upload('profile_img')) {
                                    $error = array('error' => $this->upload->display_errors());
                                    print_r($error);
                                } else {
                                    $upload_data = $this->upload->data();
                                    $config = null;
                                    $config['image_library'] = 'gd2';
                                    $config['source_image'] = $upload_data['full_path'];
                                    $config['create_thumb'] = TRUE;
                                    $config['maintain_ratio'] = TRUE;
                                    $config['width'] = 75;
                                    $config['height'] = 50;
                                    $this->load->library('image_lib', $config);
                                    $this->image_lib->resize();
                                    $this->Staff->edit($staff_id, array('profile_img' => base_url() . 'resource/img/staff/' . $upload_data['file_name']));
                                }
                            $staff = $this->Staff->find($staff_id);
                            $this->load->vars('staff', $staff);
                        }
                    } else if ($this->input->post('staffType') == STAFF_LABORATORY) {
                        $this->form_validation->set_rules('position', 'Work Position', 'trim|required');
                        if ($this->form_validation->run() == FALSE) {
                            $this->view_sp['mainPane'] = array('Staffs/add_staff');
                        } else {
                            $data = array(
                                'position' => $this->input->post('position'),
                                'staff_id' => $staff_id
                            );
                            $this->load->model('Laboratory_staff');
                            if (!$this->Laboratory_staff->insert($data)) {
                                // if the researcher insertion failed you should rol back 
                            }
                            $config['upload_path'] = 'resource/img/staff/';
                            $config['allowed_types'] = 'gif|jpg|png';
                            $config['file_name'] = $staff['fname'] . '_' . $staff_id;
                            //$config['max_size'] = '100';
                            //$config['max_width'] = '1024';
                            // $config['max_height'] = '768';
                            $this->load->library('upload', $config);
                            if ($_FILES['profile_img']['name'] != null)
                                if (!$this->upload->do_upload('profile_img')) {
                                    $error = array('error' => $this->upload->display_errors());
                                    print_r($error);
                                } else {
                                    $upload_data = $this->upload->data();
                                    $config = null;
                                    $config['image_library'] = 'gd2';
                                    $config['source_image'] = $upload_data['full_path'];
                                    $config['create_thumb'] = TRUE;
                                    $config['maintain_ratio'] = TRUE;
                                    $config['width'] = 75;
                                    $config['height'] = 50;
                                    $this->load->library('image_lib', $config);
                                    $this->image_lib->resize();
                                    $this->Staff->edit($staff_id, array('profile_img' => base_url() . 'resource/img/staff/' . $upload_data['file_name']));
                                }
                            $staff = $this->Staff->find($staff_id);
                            $this->load->vars('staff', $staff);
                        }
                    } else if ($this->input->post('staffType') == STAFF_DATA_STATISTICS) {
                        $this->form_validation->set_rules('position', 'Work Position', 'trim|required');
                        if ($this->form_validation->run() == FALSE) {
                            $this->view_sp['mainPane'] = array('Staffs/add_staff');
                        } else {
                            $data = array(
                                'position' => $this->input->post('position'),
                                'staff_id' => $staff_id
                            );
                            $this->load->model('DataAndStatistics_staff');
                            if (!$this->DataAndStatistics_staff->insert($data)) {
                                // if the researcher insertion failed you should rol back 
                            }
                            $config['upload_path'] = 'resource/img/staff/';
                            $config['allowed_types'] = 'gif|jpg|png';
                            $config['file_name'] = $staff['fname'] . '_' . $staff_id;
                            //$config['max_size'] = '100';
                            //$config['max_width'] = '1024';
                            // $config['max_height'] = '768';
                            $this->load->library('upload', $config);
                            if ($_FILES['profile_img']['name'] != null)
                                if (!$this->upload->do_upload('profile_img')) {
                                    $error = array('error' => $this->upload->display_errors());
                                    print_r($error);
                                } else {
                                    $upload_data = $this->upload->data();
                                    $config = null;
                                    $config['image_library'] = 'gd2';
                                    $config['source_image'] = $upload_data['full_path'];
                                    $config['create_thumb'] = TRUE;
                                    $config['maintain_ratio'] = TRUE;
                                    $config['width'] = 75;
                                    $config['height'] = 50;
                                    $this->load->library('image_lib', $config);
                                    $this->image_lib->resize();

                                    $this->Staff->edit($staff_id, array('profile_img' => base_url() . 'resource/img/staff/' . $upload_data['file_name']));
                                }
                            $staff = $this->Staff->find($staff_id);
                            $this->load->vars('staff', $staff);
                        }
                    } else if ($this->input->post('staffType') == STAFF_OTHER) {
                        $this->form_validation->set_rules('position', 'Work Position', 'trim|required');
                        if ($this->form_validation->run() == FALSE) {
                            $this->view_sp['mainPane'] = array('Staffs/add_staff');
                        } else {
                            $data = array(
                                'position' => $this->input->post('position'),
                                'staff_id' => $staff_id
                            );
                            $this->load->model('Other_staff');
                            if ($_FILES['profile_img']['name'] != null)
                                if (!$this->Other_staff->insert($data)) {
                                    // if the researcher insertion failed you should rol back 
                                }
                            $config['upload_path'] = 'resource/img/staff/';
                            $config['allowed_types'] = 'gif|jpg|png';
                            $config['file_name'] = $staff['fname'] . '_' . $staff_id;
                            //$config['max_size'] = '100';
                            //$config['max_width'] = '1024';
                            // $config['max_height'] = '768';
                            $this->load->library('upload', $config);
                            if (!$this->upload->do_upload('profile_img')) {
                                $error = array('error' => $this->upload->display_errors());
                                print_r($error);
                            } else {
                                $upload_data = $this->upload->data();
                                $config = null;
                                $config['image_library'] = 'gd2';
                                $config['source_image'] = $upload_data['full_path'];
                                $config['create_thumb'] = TRUE;
                                $config['maintain_ratio'] = TRUE;
                                $config['width'] = 75;
                                $config['height'] = 50;
                                $this->load->library('image_lib', $config);
                                if ($this->image_lib->resize()) {
                                    echo '<h1>awesome</h1>';
                                } else {
                                    echo '<h1>Not bad</h1>';
                                }

                                $this->Staff->edit($staff_id, array('profile_img' => base_url() . 'resource/img/staff/' . $upload_data['file_name']));
                            }
                            $staff = $this->Staff->find($staff_id);
                            $this->load->vars('staff', $staff);
                        }
                    }
                }
            }
            $this->load->view('Layout', $this->view_sp);
        } else {
            redirect(base_url() . 'index.php/Auth');
        }
    }

    public function edit($staff_id) {
        $this->view_sp['mainPane'] = array('Staffs/edit_staff');
        $this->view['aside'] = array();

        if ($this->Authorizer->authorize(array(ADMIN, DIRECTOR))) {
            if ($_SESSION['user_type'] == ADMIN) {
                $this->view_sp['mainNav'] = 'Admin/adminNav';
                $this->view_sp['aside'] = array('_newsSlider', 'Admin/_sideNav1');
            } else if ($_SESSION['user_type'] == DIRECTOR) {
                $this->view_sp['mainNav'] = '';
            }
            $this->load->library('form_validation');
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

            $this->form_validation->set_rules('fname', 'First Name', 'trim|required');
            $this->form_validation->set_rules('lname', 'Last Name', 'trim|required');
            $this->form_validation->set_rules('sex', 'Sex', 'trim|required');
            $this->form_validation->set_rules('email', 'Email', 'trim|required');
            $this->form_validation->set_rules('dob', 'Date of Birth', 'trim|required');
            $this->form_validation->set_rules('staffType', 'Staff Type', 'trim|required');


            $this->load->model('Staff');
            $staff = $this->Staff->find($staff_id);
            $this->load->vars('staff', $staff);
            $this->load->model('TimeF');
            $this->load->model('Researcher');
            $this->load->model('Laboratory_staff');
            $this->load->model('DataAndStatistics_staff');
            $this->load->model('Other_staff');

            if ($this->form_validation->run() == FALSE) {
                $this->view_sp['mainPane'] = array('Staffs/edit_staff');
            } else {

                $data = array(
                    'fname' => $this->input->post('fname'),
                    'lname' => $this->input->post('lname'),
                    'sex' => $this->input->post('sex'),
                    'email' => $this->input->post('email'),
                    'dob' => $this->TimeF->HumanToMysql($this->input->post('dob')),
                    'staff_type' => $this->input->post('staffType'),
                    'created' => date("Y-m-d h:i:s", time()),
                    'modified' => date("Y-m-d h:i:s", time())
                );
                if ($this->Staff->edit($staff_id, $data)) {
                    $staff = $this->Staff->find($staff_id);
                    if ($this->input->post('staffType') == STAFF_RESEARCHER) {
                        $this->form_validation->set_rules('researcher_level', 'Researchers Position', 'trim|required');
                        $this->form_validation->set_rules('researcher_title', 'Researchers Title', 'trim|required');
                        $this->form_validation->set_rules('edu_level', 'Education Level', 'trim|required');
                        if ($this->form_validation->run() == FALSE) {
                            $this->view_sp['mainPane'] = array('Staffs/edit_staff');
                        } else {
                            $researcher = $this->Researcher->findStaff($staff_id);
                            $data = array(
                                'researcher_title' => $this->input->post('researcher_title'),
                                'staff_id' => $staff_id,
                                'researcher_level' => $this->input->post('researcher_level'),
                                'edu_level' => $this->input->post('edu_level'),
                                'biography' => $this->input->post('biography')
                            );

                            if (!$this->Researcher->edit($researcher['researcher_id'], $data)) {
                                // if the researcher insertion failed you should rol back 
                                echo '<h2>Researcher edited</h2>';
                            }
                            $config['upload_path'] = 'resource/img/staff/';
                            $config['allowed_types'] = 'gif|jpg|png';
                            $config['file_name'] = $staff['fname'] . '_' . $staff_id;
                            //$config['max_size'] = '100';
                            //$config['max_width'] = '1024';
                            // $config['max_height'] = '768';
                            $this->load->library('upload', $config);

                            if ($_FILES['profile_img']['name'] != null)
                                if (!$this->upload->do_upload('profile_img')) {
                                    $error = array('error' => $this->upload->display_errors());
                                    print_r($error);
                                } else {
                                    $upload_data = $this->upload->data();
                                    $config = null;
                                    $config['image_library'] = 'gd2';
                                    $config['source_image'] = $upload_data['full_path'];
                                    $config['create_thumb'] = TRUE;
                                    $config['maintain_ratio'] = TRUE;
                                    $config['width'] = 75;
                                    $config['height'] = 50;
                                    $this->load->library('image_lib', $config);
                                    $this->image_lib->resize();
                                    $this->Staff->edit($staff_id, array('profile_img' => base_url() . 'resource/img/staff/' . $upload_data['file_name']));
                                }
                            $staff = $this->Staff->find($staff_id);
                            $this->load->vars('staff', $staff);
                        }
                    } else if ($this->input->post('staffType') == STAFF_LABORATORY) {
                        $this->form_validation->set_rules('position', 'Work Position', 'trim|required');
                        if ($this->form_validation->run() == FALSE) {
                            $this->view_sp['mainPane'] = array('Staffs/edit_staff');
                        } else {
                            $data = array(
                                'position' => $this->input->post('position'),
                                'staff_id' => $staff_id
                            );
                            $lab_staff = $this->Laboratory_staff->findStaff($staff_id);
                            if (!$this->Laboratory_staff->edit($lab_staff['lab_staff_id'], $data)) {
                                // if the researcher insertion failed you should rol back 
                            }
                            $config['upload_path'] = 'resource/img/staff/';
                            $config['allowed_types'] = 'gif|jpg|png';
                            $config['file_name'] = $staff['fname'] . '_' . $staff_id;
                            //$config['max_size'] = '100';
                            //$config['max_width'] = '1024';
                            // $config['max_height'] = '768';
                            $this->load->library('upload', $config);
                            if ($_FILES['profile_img']['name'] != null)
                                if (!$this->upload->do_upload('profile_img')) {
                                    $error = array('error' => $this->upload->display_errors());
                                    print_r($error);
                                } else {
                                    $upload_data = $this->upload->data();
                                    $config = null;
                                    $config['image_library'] = 'gd2';
                                    $config['source_image'] = $upload_data['full_path'];
                                    $config['create_thumb'] = TRUE;
                                    $config['maintain_ratio'] = TRUE;
                                    $config['width'] = 75;
                                    $config['height'] = 50;
                                    $this->load->library('image_lib', $config);
                                    $this->image_lib->resize();
                                    $this->Staff->edit($staff_id, array('profile_img' => base_url() . 'resource/img/staff/' . $upload_data['file_name']));
                                }
                            $staff = $this->Staff->find($staff_id);
                            $this->load->vars('staff', $staff);
                        }
                    } else if ($this->input->post('staffType') == STAFF_DATA_STATISTICS) {
                        $this->form_validation->set_rules('position', 'Work Position', 'trim|required');
                        if ($this->form_validation->run() == FALSE) {
                            $this->view_sp['mainPane'] = array('Staffs/edit_staff');
                        } else {
                            $data = array(
                                'position' => $this->input->post('position'),
                                'staff_id' => $staff_id
                            );
                            $data_stat_staff = $this->DataAndStatistics_staff->findStaff($staff_id);
                            if (!$this->DataAndStatistics_staff->edit($data_stat_staff['data_stat_staff_id'], $data)) {
                                // if the researcher insertion failed you should rol back 
                            }
                            $config['upload_path'] = 'resource/img/staff/';
                            $config['allowed_types'] = 'gif|jpg|png';
                            $config['file_name'] = $staff['fname'] . '_' . $staff_id;
                            //$config['max_size'] = '100';
                            //$config['max_width'] = '1024';
                            // $config['max_height'] = '768';
                            $this->load->library('upload', $config);
                            if ($_FILES['profile_img']['name'] != null)
                                if (!$this->upload->do_upload('profile_img')) {
                                    $error = array('error' => $this->upload->display_errors());
                                    print_r($error);
                                } else {
                                    $upload_data = $this->upload->data();
                                    $config = null;
                                    $config['image_library'] = 'gd2';
                                    $config['source_image'] = $upload_data['full_path'];
                                    $config['create_thumb'] = TRUE;
                                    $config['maintain_ratio'] = TRUE;
                                    $config['width'] = 75;
                                    $config['height'] = 50;
                                    $this->load->library('image_lib', $config);
                                    $this->image_lib->resize();

                                    $this->Staff->edit($staff_id, array('profile_img' => base_url() . 'resource/img/staff/' . $upload_data['file_name']));
                                }
                            $staff = $this->Staff->find($staff_id);
                            $this->load->vars('staff', $staff);
                        }
                    } else if ($this->input->post('staffType') == STAFF_OTHER) {
                        $this->form_validation->set_rules('position', 'Work Position', 'trim|required');
                        if ($this->form_validation->run() == FALSE) {
                            $this->view_sp['mainPane'] = array('Staffs/edit_staff');
                        } else {
                            $data = array(
                                'position' => $this->input->post('position'),
                                'staff_id' => $staff_id
                            );

                            $other_staff = $this->Other_staff->findStaff($staff_id);
                            if (!$this->Other_staff->edit($other_staff['other_staff_id'], $data)) {
                                // if the researcher insertion failed you should rol back 
                            }
                            $config['upload_path'] = 'resource/img/staff/';
                            $config['allowed_types'] = 'gif|jpg|png';
                            $config['file_name'] = $staff['fname'] . '_' . $staff_id;
                            //$config['max_size'] = '100';
                            //$config['max_width'] = '1024';
                            // $config['max_height'] = '768';
                            $this->load->library('upload', $config);
                            print_r($_FILES['profile_img']);
                            if ($_FILES['profile_img']['name'] != null) {
                                if (file_exists($config['upload_path'] . $config['file_name'])) {
                                    unlink($config['upload_path'] . $config['file_name']);
                                }
                                if (!$this->upload->do_upload('profile_img')) {
                                    $error = array('error' => $this->upload->display_errors());
                                    print_r($error);
                                } else {
                                    $upload_data = $this->upload->data();
                                    $config = null;
                                    $config['image_library'] = 'gd2';
                                    $config['source_image'] = $upload_data['full_path'];
                                    $config['create_thumb'] = TRUE;
                                    $config['maintain_ratio'] = TRUE;
                                    $config['width'] = 75;
                                    $config['height'] = 50;
                                    $this->load->library('image_lib', $config);
                                    if ($this->image_lib->resize()) {
                                        echo '<h1>awesome</h1>';
                                    } else {
                                        echo '<h1>Not bad</h1>';
                                    }

                                    $this->Staff->edit($staff_id, array('profile_img' => base_url() . 'resource/img/staff/' . $upload_data['file_name']));
                                }
                            }
                            $staff = $this->Staff->find($staff_id);
                            $this->load->vars('staff', $staff);
                        }
                    }
                }
            }
            $this->load->view('Layout', $this->view_sp);
        } else {
            redirect(base_url() . 'index.php/Auth');
        }
    }

    public function delete($staff_id, $remove = false) {
        if ($this->Authorizer->authorize(array(ADMIN, DIRECTOR))) {
            if ($_SESSION['user_type'] == ADMIN) {
                $this->view_sp['mainNav'] = 'Admin/adminNav';
                $this->view_sp['aside'] = array('_newsSlider', 'Admin/_sideNav1');
            } else if ($_SESSION['user_type'] == DIRECTOR) {
                $this->view_sp['mainNav'] = '';
            }
            $this->load->model('Staff');
            $staff = $this->Staff->find($staff_id);
            if ($staff['staff_type'] == 'Researcher') {
                $this->load->model('Researcher');
                $researcher = $this->Researcher->findStaff($staff_id);
                if ($remove) {
                    $this->Researcher->delete($researcher['researcher_id']);
                    $this->load->model('Team');
                    $teams = $this->Team->ledTeams($researcher['researcher_id']);
                    if ($teams != null) {
                        foreach ($teams as $team) {
                            $this->Team->edit($team['team_id'], array('team_leader' => 0));
                        }
                    }
                    if ($staff['profile_img'] != null || $staff['profile_img'] != '') {
                        $img = str_replace(base_url(), '', $staff['profile_img']);
                        $img_thumb = str_replace($staff['fname'] . $staff['staff_id'], $staff['fname'] . $staff['staff_id'] . '_thumb', $img);
                        echo $img_thumb;
                        unlink($img);
                        unlink($img_thumb);
                    }
                    $this->Staff->delete($staff_id);
                    $this->view_sp['mainPane'] = array('_removeSuccess');
                    $this->load->vars(array('back' => site_url('Staffs/researchers')));
                } else {
                    $this->view_sp['mainPane'] = array('_remove');
                    $remv = array('data' => 'Researcher Staff ',
                        'yes_link' => base_url() . 'index.php/Staffs/delete/' . $staff_id . '/true',
                        'no_link' => base_url() . 'index.php/Staffs',
                        'load' => 'Staffs/staff');
                    $this->load->vars('staff', $staff);
                    $this->load->vars('remove', $remv);
                }
            } else if ($staff['staff_type'] == 'Data_Statistics') {
                $this->load->model('DataAndStatistics_staff');
                $dataStat = $this->DataAndStatistics_staff->findStaff($staff_id);
                if ($remove) {
                    $this->DataAndStatistics_staff->delete($dataStat['data_stat_staff_id']);
                    if ($staff['profile_img'] != null || $staff['profile_img'] != '') {
                        $img = str_replace(base_url(), '', $staff['profile_img']);
                        $img_thumb = str_replace($staff['fname'] . $staff['staff_id'], $staff['fname'] . $staff['staff_id'] . '_thumb', $img);
                        echo $img_thumb;
                        unlink($img);
                        unlink($img_thumb);
                    }
                    $this->Staff->delete($staff_id);
                    $this->view_sp['mainPane'] = array('_removeSuccess');
                    $this->load->vars(array('back' => site_url('Staffs/DataStat')));
                } else {
                    $this->view_sp['mainPane'] = array('_remove');
                    $remv = array('data' => 'Data and Statistics Staff ',
                        'yes_link' => base_url() . 'index.php/Staffs/delete/' . $staff_id . '/true',
                        'no_link' => base_url() . 'index.php/Staffs',
                        'load' => 'Staffs/staff');
                    $this->load->vars('staff', $staff);
                    $this->load->vars('remove', $remv);
                }
            } else if ($staff['staff_type'] == 'Laboratory') {
                $this->load->model('Laboratory_staff');
                $lab_staff = $this->Laboratory_staff->findStaff($staff_id);
                if ($remove) {
                    $this->Laboratory_staff->delete($lab_staff['lab_staff_id']);
                    if ($staff['profile_img'] != null || $staff['profile_img'] != '') {
                        $img = str_replace(base_url(), '', $staff['profile_img']);
                        $img_thumb = str_replace($staff['fname'] . $staff['staff_id'], $staff['fname'] . $staff['staff_id'] . '_thumb', $img);
                        echo $img_thumb;
                        unlink($img);
                        unlink($img_thumb);
                    }
                    $this->Staff->delete($staff_id);
                    $this->view_sp['mainPane'] = array('_removeSuccess');
                    $this->load->vars(array('back' => site_url('Staffs/Laboratory')));
                } else {
                    $this->view_sp['mainPane'] = array('_remove');
                    $remv = array('data' => 'Laboratory Staff ',
                        'yes_link' => base_url() . 'index.php/Staffs/delete/' . $staff_id . '/true',
                        'no_link' => base_url() . 'index.php/Staffs',
                        'load' => 'Staffs/staff');
                    $this->load->vars('staff', $staff);
                    $this->load->vars('remove', $remv);
                }
            } else if ($staff['staff_type'] == 'Director') {
                $this->load->model('Director');
                $director = $this->Director->findStaff($staff_id);
                if ($remove) {
                    $this->Director->delete($director['director_id']);
                    $this->Staff->delete($staff_id);
                    $this->view_sp['mainPane'] = array('_removeSuccess');
                    $this->load->vars(array('back' => site_url('Staffs/researchers')));
                } else {
                    $this->view_sp['mainPane'] = array('_remove');
                    $remv = array('data' => 'Director Staff ',
                        'yes_link' => base_url() . 'index.php/Staffs/delete/' . $staff_id . '/true',
                        'no_link' => base_url() . 'index.php/Staffs/directors',
                        'load' => 'Staffs/staff');
                    $this->load->vars('staff', $staff);
                    $this->load->vars('remove', $remv);
                }
            } else if ($staff['staff_type'] == 'Other') {
                $this->load->model('Other_staff');
                $other_staff = $this->Other_staff->findStaff($staff_id);
                if ($remove) {
                    $this->Other_staff->delete($other_staff['other_staff_id']);
                    if ($staff['profile_img'] != null || $staff['profile_img'] != '') {
                        $img = str_replace(base_url(), '', $staff['profile_img']);
                        $img_thumb = str_replace($staff['fname'] . $staff['staff_id'], $staff['fname'] . $staff['staff_id'] . '_thumb', $img);
                        unlink($img_thumb);
                        unlink($img);
                    }
                    $this->Staff->delete($staff_id);
                    $this->view_sp['mainPane'] = array('_removeSuccess');
                    $this->load->vars(array('back' => site_url('Staffs/Other')));
                } else {
                    $this->view_sp['mainPane'] = array('_remove');
                    $remv = array('data' => 'Staff ',
                        'yes_link' => base_url() . 'index.php/Staffs/delete/' . $staff_id . '/true',
                        'no_link' => base_url() . 'index.php/Staffs',
                        'load' => 'Staffs/staff');
                    $this->load->vars('staff', $staff);
                    $this->load->vars('remove', $remv);
                }
            } else {
                echo '<h1>Not chosen ' . $staff['staff_type'] . '</h1>';
            }
            $this->load->view('Layout', $this->view_sp);
        }
    }

}
