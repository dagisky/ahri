<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Vacancies
 *
 * @author Dagi
 */
class Vacancies extends CI_Controller {

    //put your code here
    private $view_sp = array('mainNav' => 'mainNav', 'mainPane' => array('Vacancies/vacancy_list'), 'jumbotron' => array(), 'aside' => array('_newsSlider', '_sideNav'));

    public function __construct() {
        parent::__construct();
        session_start();
    }

    public function index($page = null) {
        if ($this->Authorizer->authorize(array(ADMIN, DIRECTOR))) {
            if ($_SESSION['user_type'] == ADMIN) {
                $this->view_sp['mainNav'] = 'Admin/adminNav';
                $this->view_sp['aside'] = array('_newsSlider', 'Admin/_sideNav1');
            } else if ($_SESSION['user_type'] == DIRECTOR) {
                $this->view_sp['mainNav'] = '';
            }
        }
        $per_page = 10;
        $this->load->model('Vacancy');
        $this->load->library('pagination');
        if ($page != null) {
            $page_data = $this->Vacancy->view($per_page, $page);
        } else {
            $page_data = $this->Vacancy->view($per_page);
        }

        $config['base_url'] = site_url('Vacancies/index');
        $config['total_rows'] = $page_data['num_rows'];
        $config['per_page'] = $per_page;
        $config['full_tag_open'] = '<ul class="pager">';
        $config['full_tag_close'] = '</ul>';

        $config['prev_link'] = 'Previous';
        $config['prev_tag_open'] = '<li style="margin-left:5px;">';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = 'Next';
        $config['next_tag_open'] = '<li style="margin-left:5px;">';
        $config['next_tag_close'] = '</li>';
        $this->pagination->initialize($config);

        if ($page_data['num_rows'] > $per_page) {
            $this->load->vars(array('paginator' => $this->pagination->create_links()));
        }

        $this->load->vars('vacancies', $page_data['data']);
        $this->load->view('Layout', $this->view_sp);
    }

    public function add() {
        $this->view_sp['mainPane'] = array('Vacancies/add_vacancy');
        $this->view['aside'] = array();
        $this->view_sp['styles'] = array('bootstrap-datetimepicker.min', 'bootstrap3-wysiwyg5');
        $this->view_sp['scripts'] = array('moment', 'bootstrap-datetimepicker.min', 'wysihtml5-0.3.0.min', 'bootstrap3-wysihtml5');

        if ($this->Authorizer->authorize(array(ADMIN, DIRECTOR))) {
            if ($_SESSION['user_type'] == ADMIN) {
                $this->view_sp['mainNav'] = 'Admin/adminNav';
                $this->view_sp['aside'] = array('_newsSlider', 'Admin/_sideNav1');
            } else if ($_SESSION['user_type'] == DIRECTOR) {
                $this->view_sp['mainNav'] = '';
            }
            $this->load->library('form_validation');
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

            $this->form_validation->set_rules('position', 'Vacancy Position', 'trim|required');
            $this->form_validation->set_rules('eligibility', 'Vacancy Eligibility', 'trim|required');


            $this->load->model('Vacancy');

            if ($this->form_validation->run() == FALSE) {
                $this->view_sp['mainPane'] = array('Vacancies/add_vacancy');
            } else {
                $data = array(
                    'position' => $this->input->post('position'),
                    'deadline' => $this->TimeF->HumanToMysql($this->input->post('deadline')),
                    'eligibility' => $this->input->post('eligibility'),
                    'responsibilities' => $this->input->post('responsibilities'),
                    'created' => date("Y-m-d h:i:s", time()),
                    'modified' => date("Y-m-d h:i:s", time())
                );

                if ($vacancy_id = $this->Vacancy->insert($data)) {
                    $vacancy = $this->Vacancy->find($vacancy_id);
                    $this->load->vars('vacancy', $vacancy);
                } else {
                    echo 'Failed';
                }
            }
            $this->load->view('Layout', $this->view_sp);
        } else {
            redirect(base_url() . 'index.php/Auth');
        }
    }

    public function edit($vacancy_id) {
        $this->view_sp['mainPane'] = array('Vacancies/edit_vacancy');
        $this->view['aside'] = array();
        $this->view_sp['styles'] = array('bootstrap-datetimepicker.min', 'bootstrap3-wysiwyg5');
        $this->view_sp['scripts'] = array('moment', 'bootstrap-datetimepicker.min', 'wysihtml5-0.3.0.min', 'bootstrap3-wysihtml5');

        if ($this->Authorizer->authorize(array(ADMIN, DIRECTOR))) {
            if ($_SESSION['user_type'] == ADMIN) {
                $this->view_sp['mainNav'] = 'Admin/adminNav';
                $this->view_sp['aside'] = array('_newsSlider', 'Admin/_sideNav1');
            } else if ($_SESSION['user_type'] == DIRECTOR) {
                $this->view_sp['mainNav'] = '';
            }
            $this->load->library('form_validation');
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

            $this->form_validation->set_rules('position', 'Vacancy Position', 'trim|required');
            $this->form_validation->set_rules('eligibility', 'Vacancy Eligibility', 'trim|required');

            $this->load->model('TimeF');
            $this->load->model('Vacancy');
            $vacancy = $this->Vacancy->find($vacancy_id);

            if ($this->form_validation->run() == FALSE) {
                $this->view_sp['mainPane'] = array('Vacancies/edit_vacancy');
                $this->load->vars('vacancy', $vacancy);
            } else {
                $data = array(
                    'position' => $this->input->post('position'),
                    'deadline' => $this->TimeF->HumanToMysql($this->input->post('deadline')),
                    'eligibility' => $this->input->post('eligibility'),
                    'responsibilities' => $this->input->post('responsibilities'),
                    'created' => $vacancy['created'],
                    'modified' => date("Y-m-d h:i:s", time())
                );

                if ($this->Vacancy->edit($vacancy_id, $data)) {
                    $vacancy = $this->Vacancy->find($vacancy_id);
                    $this->load->vars('vacancy', $vacancy);
                } else {
                    echo 'Failed';
                }
            }
            $this->load->view('Layout', $this->view_sp);
        } else {
            redirect(base_url() . 'index.php/Auth');
        }
    }

    public function delete($vacancy_id, $remove = false) {
        if ($this->Authorizer->authorize(array(ADMIN, DIRECTOR))) {
            if ($_SESSION['user_type'] == ADMIN) {
                $this->view_sp['mainNav'] = 'Admin/adminNav';
                $this->view_sp['aside'] = array('_newsSlider', 'Admin/_sideNav1');
            } else if ($_SESSION['user_type'] == DIRECTOR) {
                $this->view_sp['mainNav'] = '';
            }
            $this->load->model('Vacancy');
            if ($remove) {
                if ($this->Vacancy->delete($vacancy_id)) {
                    $this->view_sp['mainPane'] = array('_removeSuccess');
                    $this->load->vars(array('back' => site_url('Vacancies/index')));
                } else {
                    echo 'Not Removed';
                }
            } else {
                $this->view_sp['mainPane'] = array('_remove');
                $vacancy = $this->Vacancy->find($vacancy_id);
                $remv = array('data' => 'Vacancy',
                    'yes_link' => base_url() . 'index.php/Vacancies/delete/' . $vacancy_id . '/true',
                    'no_link' => base_url() . 'index.php/Vacancies',
                    'load' => 'Vacancies/vacancy');

                $this->load->vars('remove', $remv);
                $this->load->vars('vacancy', $vacancy);
            }
            $this->load->view('Layout', $this->view_sp);
        } else {
            redirect(base_url() . 'index.php/Auth');
        }
    }

}
