<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Downloads
 *
 * @author Dagi
 */
class Downloads extends CI_Controller {

    private $view_sp = array('mainNav' => 'mainNav', 'mainPane' => array('Laboratories/laboratory_list'), 'jumbotron' => array(), 'aside' => array('_newsSlider', '_sideNav'));

//put your code here
    public function __construct() {
        parent::__construct();
        session_start();
    }

    public function index() {
        $this->load->model('Download');
        $downloads = $this->Download->view();
        $this->load->vars('downloads', $downloads);
    }

    public function upload() {
        $this->view_sp['mainPane'] = array('Downloads/add_file');
        $this->view['aside'] = array();
        $this->load->model('Gallery');
        $galleries = $this->Gallery->view();


        if ($this->Authorizer->authorize(array(ADMIN, DIRECTOR))) {
            if ($_SESSION['user_type'] == ADMIN) {
                $this->view_sp['mainNav'] = 'Admin/adminNav';
            } else if ($_SESSION['user_type'] == DIRECTOR) {
                $this->view_sp['mainNav'] = '';
            }

            $this->load->library('form_validation');
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

            if ($gallery_id == null) {
                $this->load->vars('galleries', $galleries['data']);
                $this->form_validation->set_rules('gallery', 'Gallery', 'trim|required');
            } else {
                $gallery = $this->Gallery->find($gallery_id);
                $this->load->vars('gallery', $gallery);
            }
            //$this->form_validation->set_rules('img', 'Photo', 'required');



            $this->load->model('Photo');

            if ($gallery_id != null) {
                $data = array(
                    'gallery_id' => $gallery_id != null ? $gallery_id : $this->input->post('gallery'),
                    'synapse' => $this->input->post('synapse')
                );
                if (isset($_FILES['gallery_img']) && $photo_id = $this->Photo->insert($data)) {
                    $photo = $this->Photo->find($photo_id);
                    $this->load->model('Image');
                    $img = $this->Image->uploadPhoto($data['gallery_id'] . '_' . $photo_id);
                    $this->Photo->edit($photo_id, array('photo_url' => base_url() . 'resource/img/gallery/' . $img, 'photo_thumb' => base_url() . 'resource/img/gallery/thumb-' . $img));
                    $photo = $this->Photo->find($photo_id);
                    $this->load->vars('photo', $photo);
                } else {
                    
                }
            } else {
                if ($this->form_validation->run() == FALSE) {
                    $this->view_sp['mainPane'] = array('Galleries/add_photo');
                } else {
                    $data = array(
                        'gallery_id' => $gallery_id != null ? $gallery_id : $this->input->post('gallery'),
                        'synapse' => $this->input->post('synapse')
                    );
                    if ($photo_id = $this->Photo->insert($data)) {
                        $photo = $this->Photo->find($photo_id);
                        $this->load->model('Image');
                        $img = $this->Image->uploadPhoto($data['gallery_id'] . '_' . $photo_id);
                        $this->Photo->edit($photo_id, array('photo_url' => base_url() . 'resource/img/gallery/' . $img, 'photo_thumb' => base_url() . 'resource/img/gallery/thumb-' . $img));
                        $photo = $this->Photo->find($photo_id);
                        $this->load->vars('photo', $photo);
                    } else {
                        echo 'Failed';
                    }
                }
            }
            $this->load->view('Layout', $this->view_sp);
        } else {
            redirect(base_url() . 'index.php/Auth');
        }
    }

    public function download($file_id) {
        
    }

}
