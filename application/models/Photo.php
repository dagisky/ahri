<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Photo
 *
 * @author Dagi
 */
include_once 'AppModel.php';

class Photo extends AppModel {

    //put your code here
    public function __construct() {
        parent::__construct();
        $this->table = "photos";
        $this->id = "photo_id";
        $this->displayField = "synapse";
        $this->order_by = "created";
    }

    public function photoListOfGallery($gallery_id) {
        $photos = array();
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->where("gallery_id", $gallery_id);
        $Q = $this->db->get();
        if ($Q->num_rows() > 0) {
            foreach ($Q->result_array() as $row) {
                $photos[] = $row;
            }
            return $photos;
        } else {
            return null;
        }
        $Q->free_result();
    }

    public function photoCount($gallery_id) {
        $photos = array();
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->where("gallery_id", $gallery_id);
        $Q = $this->db->get();
        return $Q->num_rows();
        $Q->free_result();
    }

}
