<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Mailing_List
 *
 * @author Dagi
 */
include_once 'AppModel.php';
class Mailing_List extends AppModel{
    //put your code here
    public function __construct() {
        parent::__construct();
        $this->table = "mailing_list";
        $this->id = "id";
        $this->displayField = "email";
    }    
    
}
