<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AppModel
 *
 * @author Dagi
 */
class AppModel extends CI_Model{
    //put your code here
    protected  $table;
    protected  $id;
    protected  $displayField;
    protected  $order_by;
    protected  $order;


    public function __construct() {
        parent::__construct(); 
        $this->order = "asc"
                . "";
    }
    
     public function view($limit = null, $offset = null, $pagenation = true) {         
        $data = array();
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->order_by($this->order_by, $this->order);

        if ($limit == null && $offset == null) {
            $Q = $this->db->get();
        } else if ($limit != null && $offset == null) {
            $Q = $this->db->get(null, $limit);
        } else if ($limit != null && $offset != null) {
            $Q = $this->db->get(null, $limit, $offset);
        }

        if (isset($Q) && $Q->num_rows() > 0) {

            foreach ($Q->result_array() as $row) {
                $data[] = $row;
            }
        } else {
            $data = null;
        }
        if(!$pagenation){
            return $data;
        }
        $this->db->select('*');
        $this->db->from($this->table);
        $QC = $this->db->get();
        $result['num_rows'] = $QC->num_rows();
        $result['data'] = $data;
        return $result;
        $Q->free_result();
        $QC->free_result();
    }

    public function find($id = null, $find_str = null, $limit = null, $offset = null) {
        if ($id != null) {
            $this->db->select('*');
            $this->db->from($this->table);
            $this->db->where($this->id, $id);
            $Q = $this->db->get();
            if ($Q->num_rows() > 0) {
                return $Q->row_array();
            } else {
                return null;
            }
            $Q->free_result();
        } else if ($find_str != null) {
            $data = null;
            $this->db->select('*');
            $this->db->from($this->table);

            $this->db->like($this->displayField, $search_str);
            $this->db->order_by($this->displayField, 'desc');

            if ($limit == null && $offset == null) {
                $Q = $this->db->get();
            } else if ($limit != null && $offset == null) {
                $Q = $this->db->get(null, $limit);
            } else if ($limit != null && $offset != null) {
                $Q = $this->db->get(null, $limit, $offset);
            }
            if ($Q->num_rows() > 0) {

                foreach ($Q->result_array() as $row) {
                    $data[] = $row;
                }
            } else {
                $data = null;
            }
            $this->db->select('*');
            $this->db->from($this->table);

            $this->db->like($this->displayField, $search_str);

            $QC = $this->db->get();
            $result['num_rows'] = $QC->num_rows();
            $result['data'] = $data;
            $Q->free_result();
            $QC->free_result();
            return $result;            
        }
    }
    
    public function insert($data){
        if ($this->db->insert($this->table, $data)) {
            $id = $this->db->insert_id();

            return $id;
        } else {
            return null;
        }
    }

    public function edit($id, $data){
        $this->db->where($this->id, $id);
        if ($this->db->update($this->table, $data)) {
            return true;
        } else {
            return false;
        }
    }

    public function delete($id) {
        $this->db->where($this->id, $id);
        $this->db->limit(1);
        if($this->db->delete($this->table)){
            return true;
        }  else {
            return false;
        }
    }
}
