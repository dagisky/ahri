<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of User
 *
 * @author Dagi
 */
include_once 'AppModel.php';

class User extends AppModel {

    //put your code here
    public function __construct() {
        parent::__construct();
        $this->table = "users";
        $this->id = "user_id";
        $this->displayField = "fname";
        $this->order_by = "fname";
    }

    public function viewAdmins() {
        $researchers = array();
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->where("user_type", "Administrator");
        $Q = $this->db->get();
        if ($Q->num_rows() > 0) {
            foreach ($Q->result_array() as $row) {
                $researchers[] = $row;
            }
            return $researchers;
        } else {
            return null;
        }
        $Q->free_result();
    }

}
