<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Vacancy
 *
 * @author Dagi
 */
include_once 'AppModel.php';
class Vacancy extends AppModel{
    //put your code here
    public function __construct() {
        parent::__construct();
        $this->table = "Vacancies";
        $this->id = "vacancy_id";
        $this->displayField = "vacancy_title";
        $this->order_by = "deadline";
    }
}
