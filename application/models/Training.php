<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Training
 *
 * @author Dagi
 */
include_once 'AppModel.php';

class Training extends AppModel {

    //put your code here
    public function __construct() {
        parent::__construct();
        $this->table = "Trainings";
        $this->id = "Training_id";
        $this->displayField = "training_title";
        $this->order_by = "training_starting_date";
    }

    public function fromDate($date, $limit = null, $offset = null) {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->where('training_starting_date', $date);
        
        if ($limit == null && $offset == null) {
            $Q = $this->db->get();
        } else if ($limit != null && $offset == null) {
            $Q = $this->db->get(null, $limit);
        } else if ($limit != null && $offset != null) {
            $Q = $this->db->get(null, $limit, $offset);
        }

        if (isset($Q) && $Q->num_rows() > 0) {

            foreach ($Q->result_array() as $row) {
                $data[] = $row;
            }
        } else {
            $data = null;
        }
        
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->where('training_starting_date', $date);
        
        $QC = $this->db->get();
        $result['num_rows'] = $QC->num_rows();
        $result['data'] = $data;
        return $result;
        $Q->free_result();
        $QC->free_result();
    }

}
