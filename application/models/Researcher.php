<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Researcher
 *
 * @author Dagi
 */
include_once 'AppModel.php';

class Researcher extends AppModel {

    //put your code here
    public function __construct() {
        parent::__construct();
        $this->table = "Researchers";
        $this->id = "researcher_id";
        $this->displayField = "edu_level";
        $this->order_by = "researcher_id";
    }
    
    public function findStaff($staff_id){
        $researchers = array();
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->where("staff_id", $staff_id);
        $Q = $this->db->get();
        if ($Q->num_rows() > 0) {            
            return $Q->row_array();
        } else {
            return null;
        }
        $Q->free_result();
    }

    public function viewSeniorScientist() {
        $researchers = array();
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->where("researcher_level", "Senior Scientist");
        $Q = $this->db->get();
        if ($Q->num_rows() > 0) {
            foreach ($Q->result_array() as $row) {
                $researchers[] = $row;
            }
            return $researchers;
        } else {
            return null;
        }
        $Q->free_result();
    }

    public function viewResearchers() {
        $researchers = array();
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->where("researcher_level", "Researcher");
        $Q = $this->db->get();
        if ($Q->num_rows() > 0) {
            foreach ($Q->result_array() as $row) {
                $researchers[] = $row;
            }
            return $researchers;
        } else {
            return null;
        }
        $Q->free_result();
    }

    public function viewAssistantResearchers() {
        $researchers = array();
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->where("researcher_level", "Assistant Researcher");
        $Q = $this->db->get();
        if ($Q->num_rows() > 0) {
            foreach ($Q->result_array() as $row) {
                $researchers[] = $row;
            }
            return $researchers;
        } else {
            return null;
        }
        $Q->free_result();
    }

    public function viewPostdoctoralScientist() {
        $researchers = array();
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->where("researcher_level", "Postdoctoral Scientist");
        $Q = $this->db->get();
        if ($Q->num_rows() > 0) {
            foreach ($Q->result_array() as $row) {
                $researchers[] = $row;
            }
            return $researchers;
        } else {
            return null;
        }
        $Q->free_result();
    }

    public function viewResearchAssistant() {
        $researchers = array();
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->where("researcher_level", "Research Assistant");
        $Q = $this->db->get();
        if ($Q->num_rows() > 0) {
            foreach ($Q->result_array() as $row) {
                $researchers[] = $row;
            }
            return $researchers;
        } else {
            return null;
        }
        $Q->free_result();
    }

}
