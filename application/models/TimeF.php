<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TimeFormater
 *
 * @author Dagi
 */
class TimeF {

    //put your code here
    public function HumanToMysql($date_time, $simple = true) {
        $date_time = str_replace("/", "-", $date_time);

        $mm = substr($date_time, 0, strpos($date_time, "-"));
        $d = substr($date_time, strpos($date_time, "-") + 1, 2);
        $yy = substr($date_time, strpos($date_time, "-", strpos($date_time, "-") + 1) + 1, 4);


        if ($simple) {
             $result = $yy . '-' . $mm . '-' . $d;
             
        } else {
            $t = substr($date_time, strpos($date_time, " ") + 1, (strpos($date_time, " ", strpos($date_time, " ") + 1) - strpos($date_time, " ")));
            $am_pm = substr($date_time, strpos($date_time, " ", strpos($date_time, " ") + 1) + 1, 2);
            $h = substr($t, 0, strpos($t, ":"));
            $m = substr($t, strpos($t, ":") + 1);
            if ($am_pm == "AM") {
                $h += 12;
            }

            $result = $yy . '-' . $mm . '-' . $d . ' ' . $h . ':' . $m;
        }
        return $result;
    }

    public function MysqlToHuman($date_time, $simple = true) {
        $yy = substr($date_time, 0, strpos($date_time, "-"));
        $mm = substr($date_time, strpos($date_time, "-") + 1, 2);
        $d = substr($date_time, strpos($date_time, "-", strpos($date_time, "-") + 1) + 1, 2);


        if ($simple) {
            $result = $mm . '/' . $d . '/' . $yy;
        } else {
            $h = substr($date_time, strpos($date_time, " ") + 1, 2);
            $m = substr($date_time, strpos($date_time, ":") + 1, 2);
            $am_pm = "AM";
            if ($h > 12) {
                $am_pm = 'AM';
                $h -= 12;
            }
            $result = $mm . '/' . $d . '/' . $yy . ' ' . $h . ':' . $m . ' ' . $am_pm;
        }
        return $result;
    }

}
