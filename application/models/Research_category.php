<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Research_categories
 *
 * @author Dagi
 */
include_once 'AppModel.php';
class Research_category extends AppModel{
    //put your code here
    public function __construct() {
        parent::__construct();
        $this->table = "Research_categories";
        $this->id = "category_id";
        $this->displayField = "category_title";
        $this->order_by = "category_title";
    }
}
