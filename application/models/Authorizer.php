<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Authorizer
 *
 * @author Dagi
 */
class Authorizer {

    //put your code here
    public function authorize($stuffs) {
        if (isset($_SESSION['user_id'])) {
            $authorized = false;
            foreach ($stuffs as $stuff) {
                if (trim($stuff) == $_SESSION['user_type']) {
                    $authorized = true;
                }
            }
            return $authorized;
        } else {
            return false;
        }
    }

}
