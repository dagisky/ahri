<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AboutAhri
 *
 * @author Dagi
 */
include_once 'AppModel.php';

class AboutAhri extends AppModel {

    //put your code here
    public function __construct() {
        parent::__construct();
        $this->table = "about_ahri";
        $this->id = "id";
        $this->displayField = "title";
        $this->order_by = "title";        
    }

}
