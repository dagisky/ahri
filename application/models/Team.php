<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Team
 *
 * @author Dagi
 */
include_once 'AppModel.php';

class Team extends AppModel {

    //put your code here
    public function __construct() {
        parent::__construct();
        $this->table = "teams";
        $this->id = "team_id";
        $this->displayField = "team_name";
        $this->order_by = "team_name";
    }   

      public function ledTeams($researcher_id){
        $researchers = array();
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->where("team_leader", $researcher_id);
        $Q = $this->db->get();
        if ($Q->num_rows() > 0) {
            $data = array();
            foreach ($Q->result_array() as $row){
                $data[] = $row;
            }
            return $data;
        } else {
            return null;
        }
        $Q->free_result();
    }
    

}
