<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Events
 *
 * @author Dagi
 */
include_once 'AppModel.php';
class Event extends AppModel{
    //put your code here
    public function __construct() {
        parent::__construct();
        $this->table = "Events";
        $this->id = "event_id";
        $this->displayField = "event_title";
        $this->order_by = "created";
    }
}
