<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of m_image
 *
 * @author Dagi
 */
class Image extends CI_Model {

    //put your code here
    private $imageName;

    function __construct() {
        parent::__construct();
    }

    public function setImage($image_name) {
        $this->imageName = $image_name;
    }

    public function changeToJpgThumb($thumb_width, $thumb_height, $newName) {
        $newName .= '.jpg';
        list($width, $height, $type, $attr) = getimagesize($this->imageName);
        /* if ($width >= $height) {
          $ratio = $width;
          } else {
          $ratio = $height;
          }
          $new_width = ($width * $thumb_width) / $ratio;
          $new_height = ($height * $thumb_height) / $ratio; */
        $new_width = $thumb_width;
        $new_height = $thumb_height;
        switch ($type) {
            case 1:
                $ext = '.gif';
                $imageOld = imagecreatefromgif($this->imageName);
                break;
            case 2:
                $imageOld = imagecreatefromjpeg($this->imageName);
                $ext = 'jpg';
                break;
            case 3:
                $ext = '.png';
                $imageOld = imagecreatefrompng($this->imageName);
                break;
        }

        $image_thumb = imagecreatetruecolor($new_width, $new_height);
        imagecopyresampled($image_thumb, $imageOld, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
        if (imagejpeg($image_thumb, $newName)) {
            return true;
        } else {
            return false;
        }
        imagedestroy($imageOld);
        imagedestroy($image_thumb);
    }

    public function resizeImage($xSrc = 0, $ySrc = 0, $new_width, $new_height, $newName) {
        $newName .= '.jpg';
        list($width, $height, $type, $attr) = getimagesize($this->imageName);
        switch ($type) {
            case 1:
                $ext = '.gif';
                $imageOld = imagecreatefromgif($this->imageName);
                break;
            case 2:
                $imageOld = imagecreatefromjpeg($this->imageName);
                $ext = 'jpg';
                break;
            case 3:
                $ext = '.png';
                $imageOld = imagecreatefrompng($this->imageName);
                break;
        }

        $image_thumb = imagecreatetruecolor($new_width, $new_height);
        imagecopyresampled($image_thumb, $imageOld, 0, 0, $xSrc, $ySrc, $new_width, $new_height, $width, $height);

        if (imagejpeg($image_thumb, $newName)) {
            return true;
        } else {
            return false;
        }
        imagedestroy($imageOld);
        imagedestroy($image_thumb);
    }

    public function upload($stuff, $upload_path = 'resource/img/user/', $thumb_frame_size = 200) {
        $file_name = $stuff['fname'] . '_' . $stuff['stuff_id'];
        $config['upload_path'] = $upload_path;
        $config['allowed_types'] = 'gif|jpg|png';
        //$config['max_size'] = '4000';
        // $config['max_width'] = '1024';
        // $config['max_height'] = '768';
        $config['file_name'] = $file_name;

        $this->load->library('upload', $config);
        if (!$this->upload->do_upload('profile_img')) {
            $error = array('error' => $this->upload->display_errors());
            print_r($error);
            // $this->load->view('upload_form', $error);
        } else {
            $data = array('upload_data' => $this->upload->data());
            $this->setImage($data['upload_data']['full_path']);
            $width = $data['upload_data']['image_width'];
            $height = $data['upload_data']['image_height'];
            $w1 = $thumb_frame_size;
            $h1 = $thumb_frame_size * $height / $width;

            if ($this->resizeImage(0, 0, $w1, $h1, './' . $upload_path . $file_name)) {
                $img_url = base_url() . $upload_path . $file_name . '.jpg';
                if ($this->changeToJpgThumb($w1, $h1, './' . $upload_path . 'thumb-' . $file_name)) {
                    $thumb_url = base_url() . $upload_path . 'thumb-' . $file_name . 'jpg';
                    return $img_url;
                } else {
                    $thumb_url ['thumb_url'] = null;
                    return $img_url;
                }
            }

            //  $this->load->view('upload_success', $data);
        }
    }

    public function uploadPhoto($file_name, $upload_path = 'resource/img/gallery', $thumb_frame_size = 200) {

        $config['upload_path'] = $upload_path;
        $config['allowed_types'] = 'gif|jpg|png';
        //$config['max_size'] = '4000';
        // $config['max_width'] = '1024';
        // $config['max_height'] = '768';
        $config['file_name'] = $file_name;

        $this->load->library('upload', $config);
        if (!$this->upload->do_upload('gallery_img')) {
            $error = array('error' => $this->upload->display_errors());
            print_r($error);
            // $this->load->view('upload_form', $error);
        } else {
            $data = array('upload_data' => $this->upload->data());
            $this->setImage($data['upload_data']['full_path']);
            $width = $data['upload_data']['image_width'];
            $height = $data['upload_data']['image_height'];

            if ($width > $height) {
                $w1 = $thumb_frame_size;
                $h1 = $thumb_frame_size * $height / $width;
            } else {
                $h1 = $thumb_frame_size;
                $w1 = $thumb_frame_size * $width / $height;
            }

            $img_url = base_url() . $upload_path . '/' . $file_name . '.jpg';
            if ($this->changeToJpgThumb($w1, $h1, './' . $upload_path . '/thumb-' . $file_name)) {
                $thumb_url = base_url() . $upload_path . 'thumb-' . $file_name . 'jpg';
                return $file_name . '.jpg';
            } else {
                return null;
            }
        }
    }

}

?>
