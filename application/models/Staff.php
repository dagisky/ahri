<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Staff
 *
 * @author Dagi
 */
include_once 'AppModel.php';

class Staff extends AppModel {

    //put your code here
    public function __construct() {
        parent::__construct();
        $this->table = "Staffs";
        $this->id = "staff_id";
        $this->displayField = "fname";
        $this->order_by = "fname";
    }

}
