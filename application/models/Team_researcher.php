<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Team_researcher
 *
 * @author Dagi
 */
include_once 'AppModel.php';
class Team_researcher extends AppModel{
    //put your code here
    public function __construct() {
        parent::__construct();
        $this->table = "teams_researchers";
        $this->id = "team_researcher_id";
        $this->displayField = "";
        $this->order_by = "team_researcher_id";
    }
}
