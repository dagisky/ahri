<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of File
 *
 * @author Dagi
 */
class File extends CI_Model {

    //put your code here
    public function upload($file_name, $upload_path) {
        $config['upload_path'] = $upload_path;
        $config['allowed_types'] = 'doc|docx|pdf';

        $config['file_name'] = $file_name;

        $this->load->library('upload', $config);
        if (!$this->upload->do_upload('uploadFiles')) {
            $error = array('error' => $this->upload->display_errors());
            print_r($error);
        } else {
            $data = array('upload_data' => $this->upload->data());
            return $upload_path.$file_name.$data['upload_data']['file_ext'];
        }
    }

}
