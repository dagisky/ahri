<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Auth
 *
 * @author Dagi
 */
class Authenticator extends CI_Model {

    //put your code here
    public function __construct() {
        parent::__construct();
    }

    public function VerifyUser($username, $password) {
        if (!empty($username) && !empty($password)) {
            $this->db->select('*');
            $this->db->from('users');
            $this->db->where('username', $username);
            $this->db->where('password', $password);
            $Q = $this->db->get();
            if ($Q->num_rows() > 0) {
                $stuff = $Q->row_array();
                if ($stuff['status'] != 'Inactive') {
                    $_SESSION['username'] = $stuff['username'];
                    $_SESSION['name'] = $stuff['fname'] . ' ' . $stuff['lname'];                    
                    $_SESSION['user_id'] = $stuff['user_id'];
                    $_SESSION['user_type'] = $stuff['user_type'];
                    $_SESSION['profile_img'] = $stuff['profile_img'];
                    // $_SESSION['profile_photo_thumb'] = $stuff['profile_photo_thumb'];
                } else {
                    redirect('user/inactive/' . $stuff['user_id']);
                }

                return TRUE;
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
        $Q->free_result();
    }

    public function isAdmin($stuff_id) {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('user_id', $stuff_id);
        $this->db->where('user_type', 'Administrator');
        $Q = $this->db->get();
        if ($Q->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function isDirector($stuff_id) {
        $this->db->select('*');
        $this->db->from('stuffs');
        $this->db->where('stuff_id', $stuff_id);
        $this->db->where('stuff_type', 'Administrator');
        $Q = $this->db->get();
        if ($Q->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

  

}
