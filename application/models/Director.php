<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Director
 *
 * @author Dagi
 */
include_once 'AppModel.php';

class Director extends AppModel {

    //put your code here
    public function __construct() {
        parent::__construct();
        $this->table = "Directors";
        $this->id = "director_id";
        $this->displayField = "biography";
        $this->order_by = "staff_id";
    }

    public function findStaff($staff_id) {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->where("staff_id", $staff_id);
        $Q = $this->db->get();
        if ($Q->num_rows() > 0) {
            return $Q->row_array();
        } else {
            return null;
        }
        $Q->free_result();
    }

    public function currentDirector() {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->where('status', 'active');
        $Q = $this->db->get();
        if ($Q->num_rows() > 0) {
            return $Q->row_array();
        } else {
            return null;
        }
        $Q->free_result();
    }

}
