<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Publication
 *
 * @author Dagi
 */
include_once 'AppModel.php';
class Publication extends AppModel{
    //put your code here
    public function __construct() {
        parent::__construct();
        $this->table = "Publications";
        $this->id = "publication_id";
        $this->displayField = "publication_title";
        $this->order_by = "published_date";
    }
}
