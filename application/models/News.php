<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of News
 *
 * @author Dagi
 */
include_once 'AppModel.php';
class News extends AppModel{
    //put your code here
    public function __construct() {
        parent::__construct();
        $this->table = "News";
        $this->id = "news_id";
        $this->displayField = "news_title";
        $this->order_by = "created";
    }
}
