<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Download
 *
 * @author Dagi
 */
include_once 'AppModel.php';
class Download extends AppModel{
    //put your code here
    public function __construct() {
        parent::__construct();
        $this->table = "Downloads";
        $this->id = "download_id";
        $this->displayField = "content";
        $this->order_by = "created";
    }
}
