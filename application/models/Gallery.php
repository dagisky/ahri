<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Gallery
 *
 * @author Dagi
 */
include_once 'AppModel.php';
class Gallery extends AppModel{
    //put your code here
    public function __construct() {
        parent::__construct();
        $this->table = "galleries";
        $this->id = "gallery_id";
        $this->displayField = "gallery_title";
        $this->order_by ="gallery_title";
    }
    
}
