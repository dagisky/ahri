<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Laboratory
 *
 * @author Dagi
 */
include_once 'AppModel.php';
class Laboratory extends AppModel{
    //put your code here
    public function __construct() {
        parent::__construct();
        $this->table = "Laboratories";
        $this->id = "laboratory_id";
        $this->displayField = "laboratory_name";
        $this->order_by = "laboratory_name";
    }
}
