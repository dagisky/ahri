<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Other_staff
 *
 * @author Dagi
 */
include_once 'AppModel.php';

class Other_staff extends AppModel {

    //put your code here
    public function __construct() {
        parent::__construct();
        $this->table = "other_staff";
        $this->id = "other_staff_id";
        $this->displayField = "position";
        $this->order_by = "other_staff_id";
    }
     public function findStaff($staff_id){        
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->where("staff_id", $staff_id);
        $Q = $this->db->get();
        if ($Q->num_rows() > 0) {            
            return $Q->row_array();
        } else {
            return null;
        }
        $Q->free_result();
    }

}
