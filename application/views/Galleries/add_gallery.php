<?php
if ($this->Authorizer->authorize(array(ADMIN, DIRECTOR))) {
    echo '<a href="' . base_url() . 'index.php/Galleries/add" style="margin-right:15px;"><span class="glyphicon glyphicon-plus-sign"><span> Add Album</a>';
    echo '<a href="' . base_url() . 'index.php/Galleries"><span class="glyphicon glyphicon-list"><span> Album List</a>';
}
?>
<div id="regForm">
    <?php
    $attributes = array('class' => 'sign', 'role' => 'form');
    echo form_open('Galleries/add', $attributes);
    ?>
    <div class="row">
        <div class="col-lg-6 form-group">  
            <h3>Add New Album</h3>
            <lebel for="gallery_title">Album Title</lebel>
            <input type="text" class="form-control" id="researchTitle" name="gallery_title" placeholder="Album Title" value="<?php echo set_value('gallery_title');?>">
            
            <lebel for="gallery_desc">Album Description</lebel>
            <textarea type="text" class="form-control" id="researchTitle" name="gallery_desc" placeholder="Album Description">
                <?php echo set_value('gallery_desc');?>
            </textarea>
          

        </div>
        <div class="col-lg-6" style="padding-top: 77px;">
            <?php if (isset($gallery)) { ?>
                <div class="panel-group" id="accordion">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title has-success">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $gallery['gallery_id']; ?>">
                                    <?php echo $gallery['gallery_title']; ?>
                                </a>
                            </h4>
                            <span style="font-size: 12px; color: #999;">Posted Date : <?php echo $gallery['created']; ?></span>
                        </div>
                        <div id="collapse<?php echo $gallery['gallery_id']; ?>" class="panel-collapse collapse out">
                            <div class="panel-body">
                                <?php echo $gallery['description']; ?>
                            </div>
                        </div>
                    </div>
                    <span class="has-success">Success!</span>>
                </div>
            <?php } ?>
        </div>       
    </div>
    <button type="submit" class="btn btn-default">Add</button>


    <!----------end bottom-section----------->
    <?php echo form_close(); ?>
    <!----------end form----------->
</div>
<?php
echo validation_errors();
?>
