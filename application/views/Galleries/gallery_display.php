<?php
if ($this->Authorizer->authorize(array(ADMIN, DIRECTOR))) {
    echo '<a href="' . base_url() . 'index.php/Galleries/add" style="margin-right:15px;"><span class="glyphicon glyphicon-plus-sign"><span> Add Album</a>';
    echo '<a href="' . base_url() . 'index.php/Galleries"><span class="glyphicon glyphicon-list"><span> Album List</a>';
}
?>
<div class="text-right col-lg-12 gallery_nav">
    <?php if (isset($galleries)) { ?>
        <?php
        if ($this->Authorizer->authorize(array(ADMIN, DIRECTOR))) {
            echo '<a href="' . base_url() . 'index.php/Galleries/addPhoto/' . $gallery['gallery_id'] . '" style="margin-left:15px;"><span class="glyphicon glyphicon-picture"><span> Add Photo</a>';
        }
        ?>
        <div class="btn-group">
            <button type="button" class="btn btn-default">1</button>
            <button type="button" class="btn btn-default">2</button>

            <div class="btn-group">
                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                    <span class="glyphicon glyphicon-th"></span> Album
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu" role="menu">
                    <?php foreach ($galleries as $gal) { ?>
                        <li><a href="<?php echo base_url() . 'index.php/Galleries/index/' . $gal['gallery_id']; ?>"><?php echo $gal['gallery_title']; ?></a></li>
                    <?php } ?>
                </ul>
            </div>
        </div>
    <?php } ?>        
</div>

<div class="row gallery_container">
    <div class="col-lg-12">
        <?php if (isset($gallery)) { ?>
            <h3>
                <?php echo $gallery['gallery_title']; ?>
                <?php if ($this->Authorizer->authorize(array(ADMIN, DIRECTOR))) { ?>                    
                    <a href="<?php echo site_url('galleries/deleteGallery') . '/' . $gallery['gallery_id']; ?>" >
                        <span style="margin-left: 50px;text-decoration: underline;" class="small glyphicon glyphicon-trash"> Remove Gallery</span>
                    </a>
                <?php } ?>
            </h3>
            <?php $photos = $this->Photo->photoListOfGallery($gallery['gallery_id']) ?>
            <?php if ($photos != null) { ?>
                <div>
                    <ul class="gallery_imgs">
                        <?php foreach ($photos as $photo): ?>
                            <?php
                            list($width, $height, $type, $attr) = getimagesize($photo['photo_thumb']);
                            $size_diff = abs($width - $height);
                            $size_diff /= 4;
                            $min = $width;
                            if ($width > $height)
                                $min = $height;
                            ?>
                            <li>
                                <a href="#"  data-lightbox="gallery">   
                                    <img class="thumbnail img-responsive" style="height: 30vh;" src="<?php echo $photo['photo_thumb']; ?>">                                       
                                </a>
                                <?php if ($this->Authorizer->authorize(array(ADMIN, DIRECTOR))) { ?>
                                    <a href="<?php echo site_url('galleries/deletePhoto') . '/' . $photo['photo_id']; ?>" style="position: relative; top: -25px;">
                                        <span class="glyphicon glyphicon-trash"> Remove Photo</span>
                                    </a>
                                <?php } ?>
                            </li>
                        <?php endforeach; ?>
                    </ul>

                </div>
            <?php } else { ?>
                <h4>No Photo's Found</h4>
                <?php
            }
        }
        ?>
    </div>
    <div class="col-lg-12 gallery_nav">
        <?php if (isset($galleries)) { ?>
            <div class="btn-group">
                <button type="button" class="btn btn-default">1</button>
                <button type="button" class="btn btn-default">2</button>

                <div class="btn-group">
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                        <span class="glyphicon glyphicon-th"></span> Album
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                        <?php foreach ($galleries as $gal) { ?>                            
                            <li><a href="<?php echo base_url() . 'index.php/Galleries/index/' . $gal['gallery_id']; ?>"><?php echo $gal['gallery_title']; ?></a></li>

                        <?php } ?>
                    </ul>
                </div>
            </div>
        <?php } ?>
        <?php
        if ($this->Authorizer->authorize(array(ADMIN, DIRECTOR))) {
            echo '<a href="' . base_url() . 'index.php/Galleries/addPhoto/' . $gallery['gallery_id'] . '" style="margin-left:15px;"><span class="glyphicon glyphicon-picture"><span> Add Photo</a>';
        }
        ?>
    </div>

</div>
<style type="text/css">
    .gal_imgs{        
        background-repeat: no-repeat;
    }
</style>

