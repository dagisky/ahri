<?php if (isset($gallery)) { ?>
    <h3><?php echo $gallery['gallery_title']; ?></h3>
    <h3><?php echo 'Note: '.$this->Photo->photoCount($gallery['gallery_id']).' Photos will be Deleted'; ?></h3>
    <p><?php echo $gallery['description']; ?></p>
    <?php if ($this->Authorizer->authorize(array(ADMIN, DIRECTOR))) { ?>
        <span class="small_faded">Created: <?php echo $gallery['created']; ?></span>
        <span class="small_faded" style="margin-left: 50px;">Modified : <?php echo $gallery['modified']; ?></span>
    <?php } ?>
<?php } ?>
