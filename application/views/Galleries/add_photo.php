<?php
if ($this->Authorizer->authorize(array(ADMIN, DIRECTOR))) {
    echo '<a href="' . base_url() . 'index.php/Galleries/add" style="margin-right:15px;"><span class="glyphicon glyphicon-plus-sign"><span> Add Album</a>';
    echo '<a href="' . base_url() . 'index.php/Galleries"><span class="glyphicon glyphicon-list"><span> Album List</a>';
}
?>
<div id="regForm">
    <?php
    $attributes = array('class' => 'sign', 'role' => 'form');
    echo form_open_multipart('Galleries/addPhoto/'.$gallery['gallery_id'], $attributes);
    ?>
    <div class="row">
        <div class="col-lg-6 form-group">  
            <h3>Add New Photo</h3>

            <?php if (isset($gallery)) { ?>
                <h3><?php echo $gallery['gallery_title']; ?></h3>
                <?php } else{ ?>
                <lebel for="gallery" >Album</lebel>
                <select name="gallery" id="gallery" class="form-control">
                    <?php foreach ($galleries as $gallery): ?>
                        <option value="<?php echo $gallery['gallery_id']; ?>"><?php echo $gallery['gallery_title']; ?></option>
                    <?php endforeach; ?>                
                </select>
            <?php } ?>

            <lebel>Upload Profile Image</lebel>
            <input type="file" class="form-control" name="gallery_img" />

            <lebel for="synapse">Photo Synapse</lebel>
            <textarea id="synapse" class="form-control" name="synapse" >
                <?php echo set_value('synapse'); ?>
            </textarea>

        </div>
        <div class="col-lg-6" style="padding-top: 77px;">
            <?php if (isset($photo)) { ?>
            <img class="thumbnail" src="<?php echo  $photo['photo_thumb']; ?>">
            <?php } ?>
        </div>       
    </div>
    <button type="submit" class="btn btn-default">Add</button>


    <!----------end bottom-section----------->
    <?php echo form_close(); ?>
    <!----------end form----------->
</div>
<?php
echo validation_errors();
?>
