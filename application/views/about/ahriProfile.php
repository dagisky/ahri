<?php if (isset($info)) { ?>
    <h3><?php echo $info['title'].' '; 
    if ($this->Authorizer->authorize(array(ADMIN, DIRECTOR))){ 
        echo '<a href="' . base_url() . 'index.php/About/edit/'.$info['id'].'" style="margin-right:15px;"><span class="glyphicon glyphicon-edit"><span></a>';        
    } ?></h3>
    <?php echo $info['detail']; ?>
    <?php if ($this->Authorizer->authorize(array(ADMIN, DIRECTOR))) { ?>
        <span style="font-size: 12px; color: #999;">Created : <?php echo $info['created']; ?></span>
        <span style="font-size: 12px; color: #999; margin-left: 50px;">Modified : <?php echo $info['modified']; ?></span>
        <br/>
        <br/>
    <?php } ?>
<?php } ?>
