<div class="row">

    <div class="col-lg-4 col-sm-6 col-xs-8">
        <p><img src="<?php echo base_url(); ?>resource/img/biomedical-lab.jpg" alt=""></p>
        <h4>Laboratory</h4>
        <blockquote>When Samantha, our sweet kitten, began sleeping all the time and urinating excessively, we brought her to see the specialists at Wisdom.   </blockquote>
        <p><a href="<?php echo base_url();?>index.php/Staffs/Laboratory" class="btn btn-default">Read more >></a></p>
    </div>
    <div class="col-lg-4 col-sm-6 col-xs-8">
        <p><img src="<?php echo base_url();?>resource/img/researchers.jpg" alt=""></p>
        <h4>Research Stuff</h4>
        <blockquote>Wisdom Pet Medicine is the only clinic around that will even book pet fish for appointments. When our 13-year old goldfish, McAllister.  </blockquote>
        <p><a href="<?php echo base_url();?>index.php/Staffs/researchers" class="btn btn-default">Read more >></a></p>
    </div>
    <div class="col-lg-4 col-sm-6 col-xs-8">
        <p><img src="<?php echo base_url(); ?>resource/img/dataStuff.jpg" alt=""></p>
        <h4>Data and Statistics</h4>
        <blockquote>The staff at Wisdom worked tirelessly to determine why our three-year old bulldog, Roxie, started going into sudden kidney failure.  </blockquote>
        <p><a href="<?php echo base_url();?>index.php/Staffs/DataStat" class="btn btn-default">Read more >></a></p>
    </div>

</div>