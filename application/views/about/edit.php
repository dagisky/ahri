

<div id="regForm">
    <?php
    $attributes = array('class' => 'sign', 'role' => 'form');
    if (isset($info)) {
        echo form_open('About/edit/' . $info['id'], $attributes);
        ?>
        <div class="row">
            <div class="col-lg-12 form-group">  
                <h3>Edit <?php echo $info['title']; ?></h3>
                <lebel for="news_title">Title</lebel>
                <input type="text" class="form-control" id="researchTitle" name="title" placeholder="Title" value="<?php echo $info['title']; ?>">

                <div class="hero-unit" style="margin-top:40px">
                    <lebel for="biogr">Content</lebel>
                    <hr/>
                    <textarea name="detail" class="textarea form-control" placeholder="Enter text ..." style="width: 810px; height: 200px"><?php
                        echo $info['detail'];
                        ;
                        ?></textarea>
                </div>

            </div>

        </div>

        <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-floppy-save"></span> Save</button>
        <div class="row">
            <div class="col-lg-12" style="padding-top: 77px;">

                <div class="panel-group" id="accordion">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title has-success">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $info['id']; ?>">
    <?php echo $info['title']; ?>
                                </a>
                            </h4>
                            <span style="font-size: 12px; color: #999;">Modified : <?php echo $info['modified']; ?></span>
                        </div>
                        <div id="collapse<?php echo $info['id']; ?>" class="panel-collapse collapse out">
                            <div class="panel-body">
    <?php echo $info['detail']; ?>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
<?php } ?>

    <!----------end bottom-section----------->
<?php echo form_close(); ?>
    <!----------end form----------->
</div>
<?php
echo validation_errors();
?>
