<h4>Recent News<img height="100px" src="<?php echo base_url();?>resource/img/gifAnim/<?php echo rand(1, 8);?>.gif"></h4>
<div id="news_carousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
        <li data-target="#carousel-news-generic" data-slide-to="3" class="active"></li>
        <li data-target="#carousel-news-generic" data-slide-to="4"></li>
        <li data-target="#carousel-news-generic" data-slide-to="5"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">
        <div class="item active">
            <h2>recent projects</h2>
            <blockquote>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has 
                been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley
                of type and scrambled it to make a type specimen book.</blockquote>
            <a class="btn btn-default" href="details.html">meet our works </a>
        </div>
        <div class="item">
            <h2>Up coming Confer..</h2>
            <blockquote>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has 
                been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley
                of type and scrambled it to make a type specimen book.</blockquote>
            <a class="btn btn-default" href="details.html">Read More</a>
        </div>

        <div class="item">
            <h2>Publications...</h2>
            <blockquote>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has 
                been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley
                of type and scrambled it to make a type specimen book..</blockquote>
            <a class="btn btn-default" href="details.html">meet our works </a>
        </div>
    </div>

    <!-- Controls -->
    <a class="left carousel-control" href="#carousel-news-generic" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left"></span>
    </a>
    <a class="right carousel-control" href="#carousel-news-generic" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right"></span>
    </a>
</div>
<script type="text/javascript">
    $('.carousel').carousel({
        interval: 3000
    })
</script>



