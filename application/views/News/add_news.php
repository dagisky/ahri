<?php
if ($this->Authorizer->authorize(array(ADMIN, DIRECTOR))) {
    echo '<a href="' . base_url() . 'index.php/Nws/add" style="margin-right:15px;"><span class="glyphicon glyphicon-plus-sign"><span> Add News</a>';
    echo '<a href="' . base_url() . 'index.php/Nws"><span class="glyphicon glyphicon-list"><span> News List</a>';
    
}
?>

<div id="regForm">
    <?php
    $attributes = array('class' => 'sign', 'role' => 'form');
    echo form_open('Nws/add', $attributes);
    ?>
    <div class="row">
        <div class="col-lg-6 form-group">  
            <h3>Add New News</h3>
            <lebel for="news_title">News Title</lebel>
            <input type="text" class="form-control" id="researchTitle" name="news_title" placeholder="News Title" value="<?php echo set_value('news_title');?>">

            <div class="hero-unit" style="margin-top:40px">
                <lebel for="biogr">Content</lebel>
                <hr/>
                <textarea name="news_content" class="textarea form-control" placeholder="Enter text ..." style="width: 810px; height: 200px"><?php echo set_value('biography'); ?></textarea>
            </div>

        </div>
        <div class="col-lg-6" style="padding-top: 77px;">
            <?php if (isset($news)) { ?>
                <div class="panel-group" id="accordion">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title has-success">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $news['news_id']; ?>">
                                    <?php echo $news['news_title']; ?>
                                </a>
                            </h4>
                            <span style="font-size: 12px; color: #999;">Posted Date : <?php echo $news['created']; ?></span>
                        </div>
                        <div id="collapse<?php echo $news['news_id']; ?>" class="panel-collapse collapse out">
                            <div class="panel-body">
                                <?php echo $news['news_content']; ?>
                            </div>
                        </div>
                    </div>
                    <span class="has-success">Success!</span>>
                </div>
            <?php } ?>
        </div>       
    </div>
    <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-floppy-save"></span> Save</button>


    <!----------end bottom-section----------->
    <?php echo form_close(); ?>
    <!----------end form----------->
</div>
<?php
echo validation_errors();
?>
