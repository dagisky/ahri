<?php if (isset($news)) { ?>
    <h3><?php echo $news['news_title']; ?></h3>
    <p><?php echo $news['news_content']; ?></p>
    <?php if ($this->Authorizer->authorize(array(ADMIN, DIRECTOR))) { ?>
        <span class="small_faded">Created: <?php echo $news['created']; ?></span>
        <span class="small_faded" style="margin-left: 50px;">Modified : <?php echo $news['modified']; ?></span>
    <?php } ?>
<?php } ?>
