<?php
if ($this->Authorizer->authorize(array(ADMIN, DIRECTOR))) {
    echo '<a href="' . base_url() . 'index.php/Nws/add" style="margin-right:15px;"><span class="glyphicon glyphicon-plus-sign"><span> Add News</a>';
    echo '<a href="' . base_url() . 'index.php/Nws"><span class="glyphicon glyphicon-list"><span> News List</a>';
    
}
?>
<br/>
<br/>
<div class="panel-group" id="accordion">

    <?php if (isset($news)) foreach ($news as $nws): ?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $nws['news_id']; ?>">
                            <?php echo $nws['news_title']; ?>
                        </a>
                    </h4>
                    <span class="small_faded">posted Date : <?php echo $nws['created']; ?></span>
                     <?php
                    if ($this->Authorizer->authorize(array(ADMIN, DIRECTOR))) {
                        echo '<a href="' . base_url() . 'index.php/Nws/edit/' . $nws['news_id'] . '" style="margin-right:20px; margin-left:20px;"><small><span class="glyphicon glyphicon-edit"></span> Edit News</small></a>';
                        echo '<a href="' . base_url() . 'index.php/Nws/delete/' . $nws['news_id'] . '"><small><span class="glyphicon glyphicon-trash"></span> Remove News</small></a>';
                    }
                    ?>
                </div>
                <div id="collapse<?php echo $nws['news_id']; ?>" class="panel-collapse collapse out">
                    <div class="panel-body">
                        <?php echo $nws['news_content']; ?>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>


</div>
<?php
if (isset($paginator))
    echo $paginator;
?>