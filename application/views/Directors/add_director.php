<div id="regForm">
    <?php
    $attributes = array('class' => 'sign', 'role' => 'form');
    echo form_open_multipart('Directors/add', $attributes);
    ?>
    <div class="row">
        <div class="col-lg-6 form-group">  
            <h3>Add New Director</h3>
            <lebel for="fname">First Name</lebel>
            <input type="text" class="form-control" id="fname" name="fname" value="<?php echo set_value('fname'); ?>" placeholder="First Name">

            <lebel for="lname">Last Name</lebel>
            <input type="text" class="form-control" id="lname" value="<?php echo set_value('lname'); ?>" name="lname" placeholder="Last Name">

            <lebel for="sex" >Sex</lebel>
            <select name="sex" id="sex" class="form-control">
                <option value="Male">Male</option>
                <option value="Female">Female</option>
            </select>

            <lebel for="dob">Date Of Birth</lebel>
            <div class="form-group">
                <div class='input-group date' id='datetimepicker1'>
                    <input type='text' name="dob" value="<?php echo set_value('dob'); ?>" id="dob" class="form-control"/>
                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>

            <lebel for="email">Email Address</lebel>
            <input type="text" class="form-control" value="<?php echo set_value('email'); ?>" id="email" name="email" placeholder="E-Mail"/> 
            
            <lebel for="phone1">Phone Number-1</lebel>
            <input type="text" class="form-control" value="<?php echo set_value('phone1'); ?>" id="email" name="phone1" placeholder="Phone Number"/> 
            
            <lebel for="phone2">Phone Number-2(optional)</lebel>
            <input type="text" class="form-control" value="<?php echo set_value('phone2'); ?>" id="email" name="phone2" placeholder="Phone Number"/> 

            <lebel for="profileImg">Upload Profile Image</lebel>
            <input type="file" class="form-control" name="profile_img" />
            
                       
               
                <lebel for="eduLevel">Title</lebel>
                <select class="form-control" name="title" id="researcherLevel">
                    <option value="Mr.">Mr.</option>
                    <option value="Mr.">Mrs.</option>
                    <option value="Mis.">Mis.</option>
                    <option value="Dr.">Dr.</option>
                    <option value="Prof.">Prof.</option>                    
                </select>

                <lebel for="eduLevel">Education Status (Optional)</lebel>
                <input type="text" class="form-control" value="<?php echo set_value('edu_level'); ?>" id="eduLevel" name="edu_level" placeholder="Phd in sth, MSC in Sth, etc..."/> 
                
                <lebel for="appointed">Appointed Date</lebel>
            <div class="form-group">
                <div class='input-group date' id='datetimepicker2'>
                    <input type='text' name="appointed_date" value="<?php echo set_value('appointed_date'); ?>" id="appointed" class="form-control"/>
                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
            
            <lebel for="resignation">Resignation Date</lebel>
            <div class="form-group">
                <div class='input-group date' id='datetimepicker3'>
                    <input type='text' name="resignation_date" value="<?php echo set_value('resignation_date'); ?>" id="resignation" class="form-control"/>
                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
            
            <lebel for="status">Status</lebel>
            <select class="form-control" name="status">
                <option value="active">Active</option>
                <option value="inactive">Inactive</option>
            </select>

                <div class="hero-unit" style="margin-top:40px">
                    <lebel for="biogr">Biography</lebel>
                    <hr/>
                    <textarea name="biography" class="textarea form-control" placeholder="Enter text ..." style="width: 810px; height: 200px"><?php echo set_value('biography'); ?></textarea>
                </div>
            

        </div>
        <div class="col-lg-6" style="padding-top: 77px;">
            <?php if (isset($staff)) { ?>
                <div class="panel-group" id="accordion">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-lg-3">
                                    <a  href="#" >
                                        <img  class="thumb img-circle" alt="Profile Image" src="<?php echo $staff['profile_img'] != null ? $staff['profile_img'] : DEFAULT_RPOFILE_IMG; ?>">
                                    </a>
                                </div>
                                <div class="col-lg-9">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $staff['staff_id']; ?>">
                                        <h4><?php echo $staff['fname'] . ' ' . $staff['lname'] ?></h4>
                                    </a>
                                    <span style="font-size: 12px; color: #999;">Email:  <?php echo $staff['email']; ?></span>
                                </div>
                            </div>
                        </div>
                        <div id="collapse<?php echo $staff['staff_id']; ?>" class="panel-collapse collapse out">
                            <div class="panel-body">
                                <p>
                                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry
                                    richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor
                                    brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor,
                                    sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch
                                    et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt
                                    sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat
                                    craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't
                                    heard of them accusamus labore sustainable VHS.
                                </p>
                            </div>
                        </div>

                    </div>
                    <span class="has-success">Success!</span>
                </div>
            <?php } ?>
        </div>       
    </div>
    <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-floppy-save"></span> Save</button>


    <!----------end bottom-section----------->
    <?php echo form_close(); ?>
    <!----------end form----------->
</div>
<?php
echo validation_errors();
?>
<script type="text/javascript">
    ele = document.getElementById('staffType');
    ele.onchange = function() {
        foo = document.getElementById('researcherField');
        if (ele.value == 'Researcher') {
            foo.style["display"] = "block";
        } else {
            foo.style["display"] = "none";
        }
    }

</script>