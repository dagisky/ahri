<?php if(isset($director)){?>
<?php
if ($this->Authorizer->authorize(array(ADMIN, DIRECTOR))) {
    echo '<a href="' . base_url() . 'index.php/Directors/add" style="margin-right:15px;"><span class="glyphicon glyphicon-plus-sign"><span> Add Director</a>';
    echo '<a href="' . base_url() . 'index.php/Directors/edit/'.$director['staff_id'].'"><span class="glyphicon glyphicon-edit"><span> Edit Director</a>';
    
}
?>
<?php $staff = $this->Staff->find($director['staff_id']);?>
<div class="row">
    <div class="col-lg-6">
        <img class="img-thumbnail" src="<?php echo $staff['profile_img'];?>">
    </div>
    <div class="col-lg-6 about_director">
        <ul>
            <li><h3 style="font-size: 20px;">Name: <?php echo $staff['fname'].' '.$staff['lname'];?></h3></li>
            <li><h3 style="font-size: 20px;">E-Mail: <?php echo $staff['email'];?></h3></li>
            <li><h3 style="font-size: 20px;">Phone: <?php echo $staff['phone1'].', '.$staff['phone2'];?></h3></li>
        </ul>
    </div>    
</div>
<div class="row">
    <div class="col-lg-12">
        <h4>Biography</h4>
        <?php echo $director['biography'];?>
    </div>
</div>
<?php };?>