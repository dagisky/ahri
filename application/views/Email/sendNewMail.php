
<div id="regForm">
    <?php
    $attributes = array('class' => 'sign', 'role' => 'form');
    echo form_open('Mailing/sendMail', $attributes);
    ?>
    <div class="row">
        <div class="col-lg-6 form-group">  
            <h3>Compose New Mail</h3>
            <lebel for="title">Title</lebel>
            <input type="text" class="form-control" id="researchTitle" name="title" placeholder="Title" value="<?php echo set_value('title'); ?>">

            <div class="hero-unit" style="margin-top:40px">
                <lebel for="biogr">Content</lebel>
                <hr/>
                <textarea name="content" class="textarea form-control" placeholder="Enter text ..." style="width: 810px; height: 200px"><?php echo set_value('biography'); ?></textarea>
            </div>
            
            <div>
                <input type="radio" name="sendAll" value="true" checked/> Send To All<br/> 
                <input type="radio" name="sendAll" value="false"/> Send To Selected 
            </div>

        </div>
        <div class="col-lg-6" style="padding-top: 77px;">
            <?php if (isset($sent) && $sent) { ?>
                <div class="alert alert-success" role="alert">
                    <a href="#" class="alert-link">Your Email Successfully Sent!</a>  
                </div>
                <a  class="btn btn-success" href="<?php echo base_url(); ?>">Go TO Home</a>
            <?php } ?>
        </div>       
    </div>
    <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon glyphicon-send"></span> Send</button>


    <!----------end bottom-section----------->
    <?php echo form_close(); ?>
    <!----------end form----------->
</div>
<?php
echo validation_errors();
?>
