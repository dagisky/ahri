<?php
if ($this->Authorizer->authorize(array(ADMIN, DIRECTOR))) {
    echo '<a href="' . base_url() . 'index.php/Researchs/add" style="margin-right:15px;"><span class="glyphicon glyphicon-plus-sign"><span> Add Research</a>';
    echo '<a href="' . base_url() . 'index.php/Researchs"><span class="glyphicon glyphicon-list"><span> Research List</a>';
    
}
?>
<br/>
<br/>
<div class="panel-group" id="accordion">
    <?php
    if (isset($researchs))
        foreach ($researchs as $research):
            ?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $research['research_id'];?>">
                            <?php echo $research['research_title'];?>
                        </a>
                    </h4>
                    <span style="font-size: 12px; color: #999;">Initiation Date : <?php echo $research['inititation_date'];?></span>
                    <span style="font-size: 12px; color: #999; margin-left: 50px;">Due Date : <?php echo $research['due_date'];?></span>
                    <?php
                    if ($this->Authorizer->authorize(array(ADMIN, DIRECTOR))) {
                        echo '<a href="' . base_url() . 'index.php/Researchs/edit/' . $research['research_id'] . '" style="margin-right:20px; margin-left:20px;"><small><span class="glyphicon glyphicon-edit"></span> Edit Research</small></a>';
                        echo '<a href="' . base_url() . 'index.php/Researchs/delete/' . $research['research_id'] . '"><small><span class="glyphicon glyphicon-trash"></span> Remove Research</small></a>';
                    }
                    ?>
                </div>
                <div id="collapse<?php echo $research['research_id'];?>" class="panel-collapse collapse out">
                    <div class="panel-body">
                        <?php echo $research['description'];?>
                    </div>
                </div>
            </div>


        <?php endforeach; ?>

    
</div>
<?php
if (isset($paginator))
    echo $paginator;
?>