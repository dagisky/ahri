<?php
if ($this->Authorizer->authorize(array(ADMIN, DIRECTOR))) {
    echo '<a href="' . base_url() . 'index.php/Researchs/add" style="margin-right:15px;"><span class="glyphicon glyphicon-plus-sign"><span> Add Reseaerch</a>';
    echo '<a href="' . base_url() . 'index.php/Researchs"><span class="glyphicon glyphicon-list"><span> Research List</a>';
}
?>

<div id="regForm">
    <?php
    $attributes = array('class' => 'sign', 'role' => 'form');
    if (isset($research)) {
        echo form_open('Researchs/edit/' . $research['research_id'], $attributes);
        ?>
        <div class="row">
            <div class="col-lg-6 form-group">  
                <h3>Add New Research</h3>
                <lebel for="researchTitle">Research Title</lebel>
                <input type="text" class="form-control" id="researchTitle" name="research_title" value="<?php echo $research['research_title']; ?>" placeholder="Research Title">
                <lebel for="researchCategory">Research Title</lebel>
                <select name="research_category" class="form-control">
                    <?php                     
                    if (isset($researchCatgories)) {
                        foreach ($researchCatgories as $category) {
                            if ($research['category_id'] != $category['category_id']) {
                                echo '<option value="' . $category['category_id'] . '"  >' . $category['category_title'] . '</option>';
                            } else {
                                echo '<option value="' . $category['category_id'] . '"  selected >' . $category['category_title'] . '</option>';
                            }
                        }
                    }
                    ?>
                </select>
                <lebel for="researchDesc">Research Description</lebel>
                <textarea rows="10" class="form-control" id="researchDesc" name="description"><?php echo $research['description']; ?></textarea>

                <lebel for="initDate">Initiation Date</lebel>
                <div class="form-group">
                    <div class='input-group date' id='datetimepicker1'>
                        <input class="form-control" type="text" id="initDate" value="<?php echo $this->TimeF->MysqlToHuman($research['inititation_date']); ?>" name="initDate" />
                        <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </div>


                <lebel for="dueDate">Due Date</lebel>
                <div class="form-group">
                    <div class='input-group date' id='datetimepicker2'>
                        <input class="form-control" type="text" id="dueDate" name="dueDate" value="<?php echo $this->TimeF->MysqlToHuman($research['due_date']); ?>"/>
                        <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </div>

            </div>
            <div class="col-lg-6" style="padding-top: 77px;">

                <div class="panel-group" id="accordion">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title has-success">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $research['research_id']; ?>">
                                    <?php echo $research['research_title']; ?>
                                </a>
                            </h4>
                            <span style="font-size: 12px; color: #999;">Initiation Date : <?php echo $research['inititation_date']; ?></span>
                            <span style="font-size: 12px; color: #999; margin-left: 50px;">Due Date : <?php echo $research['due_date']; ?></span>
                        </div>
                        <div id="collapse<?php echo $research['research_id']; ?>" class="panel-collapse collapse out">
                            <div class="panel-body">
                                <?php echo $research['description']; ?>
                            </div>
                        </div>
                    </div>
                    
                </div>
            <?php } ?>
        </div>       
    </div>
    <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-floppy-save"></span> Save</button>


    <!----------end bottom-section----------->
    <?php echo form_close(); ?>
    <!----------end form----------->
</div>
<?php
echo validation_errors();
?>
