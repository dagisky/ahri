<?php if (isset($research)) { ?>
    <article>
        <header><h3><u><?php echo $research['research_title']; ?></u></h3></header>
        <?php $research_cat = $this->Research_category->find($research['category_id']); ?>
        <span class="small_faded">Category: <?php $research_cat['category_title']?></span>
        <span class="small_faded" style="margin-left: 50px;">Initiation Date: <?php echo $research['inititation_date']; ?></span>
        <span class="small_faded" style="margin-left: 50px;">Due Date: <?php echo $research['due_date']; ?></span>
        
        <p><?php echo $research['description']; ?></p>
        <?php if ($this->Authorizer->authorize(array(ADMIN, DIRECTOR))) { ?>
            <span class="small_faded">Created: <?php echo $research['created']; ?></span>
            <span class="small_faded" style="margin-left: 50px;">Modified : <?php echo $research['modified']; ?></span>
        <?php } ?>
    </article>
<?php } ?>