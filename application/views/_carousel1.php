<div class="jumbotron">
    <div class="pull-right" style="margin-left: 50px;">
        <div id="front_carousel" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                <li data-target="#carousel-example-generic" data-slide-to="2"></li>
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner">
                <div class="item active">
                    <img src="<?php echo base_url(); ?>resource/img/slider/01.jpg" alt="...">
                    <div class="carousel-caption">
                        who cares men
                    </div>
                </div>
                <div class="item">
                    <img src="<?php echo base_url(); ?>resource/img/slider/02.jpg" alt="...">
                    <div class="carousel-caption">
                        wats up
                    </div>
                </div>

                <div class="item">
                    <img src="<?php echo base_url(); ?>resource/img/slider/03.jpg" alt="...">
                    <div class="carousel-caption">
                        hey kill that virus
                    </div>
                </div>
            </div>

            <!-- Controls -->
            <a class="left carousel-control" href="#" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left"></span>
            </a>
            <a class="right carousel-control" href="#" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right"></span>
            </a>
        </div>
    </div>
    <h2>Armauer Hansen Research Institute</h2>
    <p>AHRI is a Biomedical research Institute in Ethiopia which is working in tuberculosis, HIV, malaria, Leishmaniasis, training and more...</p>
</div>

