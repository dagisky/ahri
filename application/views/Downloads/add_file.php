
<div id="regForm">
    <?php
    $attributes = array('class' => 'sign', 'role' => 'form');
    echo form_open_multipart('Galleries/addPhoto/'.$gallery['gallery_id'], $attributes);
    ?>
    <div class="row">
        <div class="col-lg-6 form-group">  
            <h3>Upload a File</h3>
            <lebel>File Name</lebel>
            <input type="file" class="form-control" name="fileName">

            <lebel>Upload Profile Image</lebel>
            <input type="file" class="form-control" name="fileUp" />

            <lebel for="synapse">File Synapse</lebel>
            <textarea id="synapse" class="form-control" name="synapse" placeholder="Entger Your Text" >                
            </textarea>

        </div>
        <div class="col-lg-6" style="padding-top: 77px;">
            <?php if (isset($photo)) { ?>
            <img class="thumbnail" src="<?php echo  $photo['photo_thumb']; ?>">
            <?php } ?>
        </div>       
    </div>
    <button type="submit" class="btn btn-default">Add</button>


    <!----------end bottom-section----------->
    <?php echo form_close(); ?>
    <!----------end form----------->
</div>
<?php
echo validation_errors();
?>
