<div id="regForm">
    <?php
    $attributes = array('class' => 'sign', 'role' => 'form');
    if (isset($staff)) {
    echo form_open_multipart('Staffs/edit/'.$staff['staff_id'], $attributes);    
        ?>
        <div class="row">
            <div class="col-lg-6 form-group">  
                <h3>Edit Staffs</h3>
                <lebel for="fname">First Name</lebel>
                <input type="text" class="form-control" id="fname" name="fname" value="<?php echo $staff['fname']; ?>" placeholder="First Name">

                <lebel for="lname">Last Name</lebel>
                <input type="text" class="form-control" id="lname" value="<?php echo $staff['lname']; ?>" name="lname" placeholder="Last Name">

                <lebel for="sex" >Sex</lebel>
                <select name="sex" id="sex" class="form-control">
                    <option value="Male" <?php if($staff['sex'] == 'Male') echo 'selected';?>>Male</option>
                    <option value="Female" <?php if($staff['sex'] == 'Female') echo 'selected';?>>Female</option>
                </select>

                <lebel for="dob">Date Of Birth (mm/dd/yy)</lebel>
                <div class="form-group">
                    <div class='input-group date' id='datetimepicker1'>
                        <input type='text' name="dob" value="<?php echo $this->TimeF->MysqlToHuman($staff['dob']);?>" id="dob" class="form-control"/>
                        <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </div>

                <lebel for="email">Email Address</lebel>
                <input type="text" class="form-control" value="<?php echo $staff['email']; ?>" id="email" name="email" placeholder="E-Mail"/> 


                <lebel for="staffType">Staff Type</lebel>
                <select name="staffType" onchange="staffType()" id="staffType" class="form-control">                 
                    <option value="Researcher" <?php if($staff['staff_type'] == 'Researcher') echo 'selected';?>>Researcher</option>  
                    <option value="Laboratory" <?php if($staff['staff_type'] == 'Laboratory') echo 'selected';?>>Laboratory Staff</option> 
                    <option value="Data_Statistics" <?php if($staff['staff_type'] == 'Data_Statistics') echo 'selected';?> >Data and Statistics Staff</option> 
                    <option value="Other" <?php if($staff['staff_type'] == 'Other') echo 'selected';?> >Other Staff</option> 
                </select>

                <lebel for="staffType">Upload Profile Image</lebel>
                <input type="file" class="form-control" name="profile_img" />
                <!--++++++++++++++++++++++++++++++++++RESEARCHER FIELD++++++++++++++++++++++++++++++++++++++++++++++++++++-->
               <?php if($staff['staff_type'] == 'Researcher'){                   
                   $researcher = $this->Researcher->findStaff($staff['staff_id']);                   
               }?>
               
                <div id="researcherField" <?php echo $staff['staff_type'] != 'Researcher' ? 'style="display:none;"': ''?> > 
                    <lebel for="researcherLevel">Researcher Level</lebel>
                    <select class="form-control" name="researcher_level" id="researcherLevel">
                        <option value="Assistant Researcher"  <?php if($staff['staff_type'] == 'Researcher' && $researcher['researcher_level'] == 'Assistant Researcher' ) echo 'selected';?> >Assistant Researcher</option>
                        <option value="Research Assistant" <?php if($staff['staff_type'] == 'Researcher' && $researcher['researcher_level'] == 'Research Assistant' ) echo 'selected';?> >Research Assistant</option>
                        <option value="Researcher" <?php if($staff['staff_type'] == 'Researcher' && $researcher['researcher_level'] == 'Researcher' ) echo 'selected';?> >Researcher</option>
                        <option value="Postdoctoral Scientist" <?php if($staff['staff_type'] == 'Researcher' && $researcher['researcher_level'] == 'Postdoctoral Scientist' ) echo 'selected';?> >Postdoctoral Scientist</option>
                        <option value="Senior Scientist" <?php if($staff['staff_type'] == 'Researcher' && $researcher['researcher_level'] == 'Senior Scientist' ) echo 'selected';?> >Senior Scientist</option>
                    </select>

                    <lebel for="eduLevel">Researcher's Title</lebel>
                    <select class="form-control" name="researcher_title" id="researcherLevel">
                        <option value="Mr." <?php if($staff['staff_type'] == 'Researcher' && $researcher['researcher_title'] =='Mr.') echo 'selected'; ;?> >Mr.</option>
                        <option value="Miss." <?php if($staff['staff_type'] == 'Researcher' && $researcher['researcher_title'] =='Mis.') echo 'selected'; ;?> >Mis.</option>
                        <option value="Mrs." <?php if($staff['staff_type'] == 'Researcher' && $researcher['researcher_title'] =='Mrs.') echo 'selected'; ;?> >Mrs.</option>
                        <option value="Dr." <?php if($staff['staff_type'] == 'Researcher' && $researcher['researcher_title'] =='Dr.') echo 'selected'; ;?> >Dr.</option>
                        <option value="Prof." <?php if($staff['staff_type'] == 'Researcher' && $researcher['researcher_title'] =='Prof.') echo 'selected'; ;?> >Prof.</option>                    
                    </select>

                    <lebel for="eduLevel">Researcher's Education</lebel>
                    <input type="text" class="form-control" value="<?php if($staff['staff_type'] == 'Researcher') echo $researcher['edu_level']; ;?>" id="researcherLevel" name="edu_level" placeholder="Phd in sth, MSC in Sth, etc..."/> 

                    <div class="hero-unit" style="margin-top:40px">
                        <lebel for="biogr">Biography</lebel>
                        <hr/>
                        <textarea name="biography" class="textarea form-control" placeholder="Enter text ..." style="width: 810px; height: 200px"><?php if($staff['staff_type'] == 'Researcher') echo $researcher['biography']; ;?></textarea>
                    </div>
                </div>
                <!--++++++++++++++++++++++++++++++++++++++END OF RESEARCHER FIELD++++++++++++++++++++++++++++++++++++++++++++-->
                <div id="positionDetailField" <?php echo $staff['staff_type'] == 'Researcher' ? 'style="display:none;"': ''?> >
                    <lebel for="position">Position</lebel>
                    <input type="text" class="form-control" value="<?php echo set_value('position'); ?>" id="position" name="position" placeholder="Please enter your work position etc..."/> 
                </div>

            </div>
            <div class="col-lg-6" style="padding-top: 77px;">
                <?php if (isset($staff)) { ?>

                    <div class="panel-group" id="accordion">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-lg-3">
                                        <a  href="#" >
                                            <img  class="thumb img-circle" alt="Profile Image" src="<?php echo $staff['profile_img'] != null ? str_replace($staff['fname'] . '_' . $staff['staff_id'], $staff['fname'] . '_' . $staff['staff_id'] . '_thumb', $staff['profile_img']) : DEFAULT_RPOFILE_IMG; ?>">
                                        </a>
                                    </div>
                                    <div class="col-lg-9">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $staff['staff_id']; ?>">
                                            <h4><?php echo $staff['fname'] . ' ' . $staff['lname'] ?></h4>
                                        </a>
                                        <span style="font-size: 12px; color: #999;">Email:  <?php echo $staff['email']; ?></span>
                                    </div>
                                </div>
                            </div>
                            <div id="collapse<?php echo $staff['staff_id']; ?>" class="panel-collapse collapse out">
                                <div class="panel-body">
                                    <p>
                                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry
                                        richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor
                                        brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor,
                                        sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch
                                        et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt
                                        sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat
                                        craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't
                                        heard of them accusamus labore sustainable VHS.
                                    </p>
                                </div>
                            </div>

                        </div>
                        
                    </div>
                <?php } ?>
            </div>       
        </div>
        <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-floppy-save"></span> Save</button>

    
    <!----------end bottom-section----------->
    <?php echo form_close(); ?>
    <?php } ?>
    <!----------end form----------->
</div>
<?php
echo validation_errors();
?>
<script type="text/javascript">
    ele = document.getElementById('staffType');
    ele.onchange = function() {
        res = document.getElementById('researcherField');
        pos = document.getElementById('positionDetailField')
        if (ele.value == 'Researcher') {
            res.style["display"] = "block";
            pos.style["display"] = "none";
        } else {
            res.style["display"] = "none";
            pos.style["display"] = "block";
        }
    }

</script>