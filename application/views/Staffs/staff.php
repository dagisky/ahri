<?php if (isset($staff)) { ?>
    <article>
        <header><h3><u><?php echo $staff['fname'].' '. $staff['lname']; ?></u></h3></header> 
        <p>
        <span class="small_faded">Email: <?php $staff['email']?></span>
        <span class="small_faded" style="margin-left: 50px;">Phone: <?php echo $staff['phone1']; ?></span>
        </p>
    
        <?php if ($this->Authorizer->authorize(array(ADMIN, DIRECTOR))) { ?>
            <span class="small_faded">Created: <?php echo $staff['created']; ?></span>
            <span class="small_faded" style="margin-left: 50px;">Modified : <?php echo $staff['modified']; ?></span>
        <?php } ?>
    </article>
<?php } ?>