<div class="panel-group" id="accordion">
    <?php if (isset($staffs)) foreach ($staffs as $staff): ?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-lg-3">
                            <a  href="#" >
                                <img  class="thumb img-circle" alt="Profile Image" src="<?php echo $staff['profile_img'] != null ? $staff['profile_img'] : DEFAULT_RPOFILE_IMG; ?>">
                            </a>
                        </div>
                        <div class="col-lg-9">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $staff['staff_id']; ?>">
                                <h4><?php echo $staff['fname'] . ' ' . $staff['lname'] ?></h4>
                            </a>
                            <span style="font-size: 12px; color: #999;">Email:  <?php echo $staff['email'];?></span>
                        </div>
                    </div>
                </div>
                <div id="collapse<?php echo $staff['staff_id']; ?>" class="panel-collapse collapse out">
                    <div class="panel-body">
                        <p>
                            Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry
                            richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor
                            brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor,
                            sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch
                            et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt
                            sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat
                            craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't
                            heard of them accusamus labore sustainable VHS.
                        </p>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
</div>
