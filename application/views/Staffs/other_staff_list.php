<h3>Other Staffs</h3>
<?php
if ($this->Authorizer->authorize(array(ADMIN, DIRECTOR))) {
    echo '<a href="' . base_url() . 'index.php/Staffs/add" style="margin-right:15px;"><span class="glyphicon glyphicon-plus-sign"><span> Add Staff</a><br/><br/>';
}
?>
<div class="row">
    <div class="panel-group" id="accordion">
        <?php if (isset($other_staffs) && $other_staffs['num_rows'] > 0) foreach ($other_staffs['data'] as $other_staff): ?>
                <?php
                $staff = $this->Staff->find($other_staff['staff_id'])
                ?>
                <div class="col-lg-5" style="margin-bottom: 10px;">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="row" >
                                <div class="col-lg-3">
                                    <a  href="#" >
                                        <img  class="thumb img-circle" alt="Profile Image" src="<?php echo $staff['profile_img'] != null ? $staff['profile_img'] : DEFAULT_RPOFILE_IMG; ?>">
                                    </a>
                                    <?php
                                    if ($this->Authorizer->authorize(array(ADMIN, DIRECTOR))) {
                                        echo ' <a href="' . base_url() . 'index.php/Staffs/edit/' . $staff['staff_id'] . '" style="margin-right:15px;"><span class="glyphicon glyphicon-edit"><span></a>';
                                        echo '<a href="' . base_url() . 'index.php/Staffs/delete/' . $staff['staff_id'] . '"><small><span class="glyphicon glyphicon-trash"></span></small></a>';
                                    }
                                    ?>
                                </div>
                                <div class="col-lg-9">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $staff['staff_id']; ?>">
                                        <h4><?php echo $staff['fname'] . ' ' . $staff['lname'] ?></h4>
                                    </a>
                                    <span style="font-size: 12px; color: #999;">Email:  <?php echo $staff['email']; ?></span>
                                    <span style="font-size: 12px; color: #999;">Position:  <?php echo $other_staff['Position']; ?></span>
                                </div>
                            </div>
                        </div>
                        <div id="collapse<?php echo $staff['staff_id']; ?>" class="panel-collapse collapse out">
                            <div class="panel-body">
                                <p>
                                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry
                                    richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor
                                    brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor,
                                    sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch
                                    et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt
                                    sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat
                                    craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't
                                    heard of them accusamus labore sustainable VHS.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
    </div>
</div><?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

