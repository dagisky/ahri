<?php
if ($this->Authorizer->authorize(array(ADMIN, DIRECTOR))) {
    echo '<a href="' . base_url() . 'index.php/Teams/add" style="margin-right:15px;"><span class="glyphicon glyphicon-plus-sign"><span> Add Team</a>';
    echo '<a href="' . base_url() . 'index.php/Teams"><span class="glyphicon glyphicon-list"><span> Team List</a>';
    
}
?>
<br/>
<br/>
<?php if(isset($team)){?>
<?php $team_leader = $this->Researcher->find($team['team_leader'])?>
<h3><?php echo $team['team_name'];?></h3>
<div class="row">
    <div class="col-lg-12">
        <h4>Team Leader</h4>
    </div>
    <div class="col-lg-6 col-xm-12">
        <?php $team_leader_staff = $this->Staff->find($team_leader['staff_id'])?>
        <img src="<?php echo $team_leader_staff['profile_img'];?>">
    </div>
    <div class="col-lg-6 col-xs-12">
        <ul>
            <li>Name: <?php echo $team_leader_staff['fname'].' '.$team_leader_staff['lname'];?></li>
            <li>Phone: <?php echo $team_leader_staff['phone1'] != null ? $team_leader_staff['phone1']: "-";?></li>
            <li><?php echo $team_leader_staff['phone2'] != null ? ", ".$team_leader_staff['phone2']: ", -";?></li>
            <li>Email: <?php echo $team_leader_staff['email'] != null ? $team_leader_staff['email']: "-";?></li>
        </ul>
    </div>
</div>
<p><?php echo $team['description'];?></p>
<p><?php echo $team['focus_area'];?></p>
<?php }?>