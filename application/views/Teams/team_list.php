<?php
if ($this->Authorizer->authorize(array(ADMIN, DIRECTOR))) {
    echo '<a href="' . base_url() . 'index.php/Teams/add" style="margin-right:15px;"><span class="glyphicon glyphicon-plus-sign"><span> Add Team</a>';
    echo '<a href="' . base_url() . 'index.php/Teams"><span class="glyphicon glyphicon-list"><span> Team List</a>';
    
}
?>
<br/>
<br/>
<div class="panel-group" id="accordion">

    <?php if (isset($teams)) foreach ($teams as $team): ?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $team['team_id']; ?>">
                            <?php echo $team['team_name']; ?>
                        </a>
                    </h4>
                    <span class="small_faded">Created Date : <?php echo $team['created']; ?></span>
                    <?php
                    if ($this->Authorizer->authorize(array(ADMIN, DIRECTOR))) {
                        echo '<a href="' . base_url() . 'index.php/Teams/edit/' . $team['team_id'] . '" style="margin-right:20px; margin-left:20px;"><small><span class="glyphicon glyphicon-edit"></span> Edit Teams</small></a>';
                        echo '<a href="' . base_url() . 'index.php/Teams/delete/' . $team['team_id'] . '"><small><span class="glyphicon glyphicon-trash"></span> Remove Teams</small></a>';
                    }
                    ?>
                </div>
                <div id="collapse<?php echo $team['team_id']; ?>" class="panel-collapse collapse out">
                    <div class="panel-body">
                        <?php echo $team['focus_area']; ?>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>


</div>
<?php
if (isset($paginator))
    echo $paginator;
?>