
<?php if(isset($team)){?>
<?php $team_leader = $this->Researcher->find($team['team_leader'])?>
<h3><?php echo $team['team_name'];?></h3>
<div class="row">
    <div class="col-lg-12">
        <h4>Team Leader</h4>
    </div>
    <div class="col-lg-6 col-xm-12">
        <?php $team_leader_stuff = $this->Stuff->find($team_leader['stuff_id'])?>
        <img src="<?php echo $team_leader_stuff['profile_img'];?>">
    </div>
    <div class="col-lg-6 col-xs-12">
        <ul>
            <li>Name: <?php echo $team_leader_stuff['fname'].' '.$team_leader_stuff['lname'];?></li>
            <li>Phone: <?php echo $team_leader_stuff['phone1'] != null ? $team_leader_stuff['phone1']: "-";?></li>
            <li><?php echo $team_leader_stuff['phone2'] != null ? ", ".$team_leader_stuff['phone2']: ", -";?></li>
            <li>Email: <?php echo $team_leader_stuff['email'] != null ? $team_leader_stuff['email']: "-";?></li>
        </ul>
    </div>
</div>
<p><?php echo $team['description'];?></p>
<p><?php echo $team['focus_area'];?></p>
<?php }?>