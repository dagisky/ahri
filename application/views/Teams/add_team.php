<?php
if ($this->Authorizer->authorize(array(ADMIN, DIRECTOR))) {
    echo '<a href="' . base_url() . 'index.php/Teams/add" style="margin-right:15px;"><span class="glyphicon glyphicon-plus-sign"><span> Add Team</a>';
    echo '<a href="' . base_url() . 'index.php/Teams"><span class="glyphicon glyphicon-list"><span> Team List</a>';
    
}
?>
<div id="regForm">
    <?php
    $attributes = array('class' => 'sign', 'role' => 'form');
    echo form_open('Teams/add', $attributes);
    ?>
    <div class="row">
        <div class="col-lg-6 form-group">  
            <h3>Add New Team</h3>
            <lebel for="team_name">Team Name</lebel>
            <input type="text" class="form-control" id="researchTitle" name="team_name" placeholder="Team Name" value="<?php echo set_value('team_name');?>">
            
            <lebel for="team_leader">Team Leader</lebel>
            <select name="team_leader" id="team_leader" class="form-control">
                <?php if(isset($researchers)){ ?>
                
                <?php foreach ($researchers as $researcher):?>
                 <?php $staff = $this->Staff->find($researcher['staff_id']);?>
                <option value="<?php echo $researcher['researcher_id'];?>"><?php echo $staff['fname'].' '.$staff['lname'];?></option>
                <?php endforeach; }?>
            </select>

            <div class="hero-unit" style="margin-top:40px">
                <lebel for="biogr">Team Description</lebel>
                <hr/>
                <textarea name="team_desc" class="textarea form-control" placeholder="Enter text ..." style="width: 810px; height: 200px"><?php echo set_value('team_desc'); ?></textarea>
            </div>
            
            <div class="hero-unit" style="margin-top:40px">
                <lebel for="biogr">Team Focus Area</lebel>
                <hr/>
                <textarea name="focus_area" class="textarea form-control" placeholder="Enter text ..." style="width: 810px; height: 200px"><?php echo set_value('focus_area'); ?></textarea>
            </div>

        </div>
        <div class="col-lg-6" style="padding-top: 77px;">
            <?php if (isset($team)) { ?>
                <div class="panel-group" id="accordion">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title has-success">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $team['team_id']; ?>">
                                    <?php echo $team['team_name']; ?>
                                </a>
                            </h4>
                            <span style="font-size: 12px; color: #999;">Posted Date : <?php echo $team['created']; ?></span>
                        </div>
                        <div id="collapse<?php echo $team['team_id']; ?>" class="panel-collapse collapse out">
                            <div class="panel-body">
                                <?php echo $team['description']; ?>
                            </div>
                        </div>
                    </div>
                    <span class="has-success">Success!</span>
                </div>
            <?php } ?>
        </div>       
    </div>
    <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-floppy-save"></span> Save</button>


    <!----------end bottom-section----------->
    <?php echo form_close(); ?>
    <!----------end form----------->
</div>
<?php
echo validation_errors();
?>
