<?php
if ($this->Authorizer->authorize(array(ADMIN, DIRECTOR))) {
    echo '<a href="' . base_url() . 'index.php/Publications/add" style="margin-right:15px;"><span class="glyphicon glyphicon-plus-sign"><span> Add Publication</a>';
    echo '<a href="' . base_url() . 'index.php/Publications"><span class="glyphicon glyphicon-list"><span> Publication List</a>';
    
}
?>
<br/>
<br/>
<div class="panel-group" id="accordion">

    <?php if (isset($publications)) foreach ($publications as $publication): ?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $publication['publication_id']; ?>">
                            <?php echo $publication['publication_title']; ?>
                        </a>
                    </h4>
                    <span class="small_faded">Published Date : <?php echo $publication['published_date']; ?></span>
                    <?php
                    if ($this->Authorizer->authorize(array(ADMIN, DIRECTOR))) {
                        echo '<a href="' . base_url() . 'index.php/Publications/edit/' . $publication['publication_id'] . '" style="margin-right:20px; margin-left:20px;"><small><span class="glyphicon glyphicon-edit"></span> Edit Publication</small></a>';
                        echo '<a href="' . base_url() . 'index.php/Publications/delete/' . $publication['publication_id'] . '"><small><span class="glyphicon glyphicon-trash"></span> Remove Publication</small></a>';
                    }
                    ?>
                </div>
                <div id="collapse<?php echo $publication['publication_id']; ?>" class="panel-collapse collapse out">
                    <div class="panel-body">
                        <?php echo $publication['publication_synapse']; ?>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>


</div>
<?php
if (isset($paginator))
    echo $paginator;
?>