<?php
if ($this->Authorizer->authorize(array(ADMIN, DIRECTOR))) {
    echo '<a href="' . base_url() . 'index.php/Publications/add" style="margin-right:15px;"><span class="glyphicon glyphicon-plus-sign"><span> Add Publication</a>';
    echo '<a href="' . base_url() . 'index.php/Publications"><span class="glyphicon glyphicon-list"><span> Publication List</a>';
    
}
?>
<div id="regForm">
    <?php
    $attributes = array('class' => 'sign', 'role' => 'form');
    echo form_open('Publications/add', $attributes);
    ?>
    <div class="row">
        <div class="col-lg-6 form-group">  
            <h3>Add New Publication</h3>
            <lebel for="publicationTitle">Publication Title</lebel>
            <input type="text" class="form-control" id="publicationTitle" name="publication_title" placeholder="Publication Title">
            <lebel for="publicationUrl">Publication URL</lebel>
            <input type="text" class="form-control" id="publicationURL" name="publication_url" placeholder="http://yoururl.ext">
            <lebel for="pubDate">Publication Date</lebel>
            <input class="form-control" type="text" id="pubDate" name="pubDate" />
            <lebel for="publicationDesc">Publication Description</lebel>
            <textarea rows="10" class="form-control" id="publicationDesc" name="description"></textarea>
            
            
        </div>
        <div class="col-lg-6" style="padding-top: 77px;">
            <?php if (isset($publication)) { ?>
                <div class="panel-group" id="accordion">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title has-success">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $publication['publication_id']; ?>">
                                    <?php echo $publication['publication_title']; ?>
                                </a>
                            </h4>
                            <span style="font-size: 12px; color: #999;">Published Date : <?php echo $publication['published_date']; ?></span>
                            
                            </div>
                        <div id="collapse<?php echo $publication['publication_id']; ?>" class="panel-collapse collapse out">
                            <div class="panel-body">
                                <?php echo $publication['publication_synapse']; ?>
                            </div>
                        </div>
                    </div>
                    <span class="has-success">Success!</span>>
                </div>
            <?php } ?>
        </div>       
    </div>
    <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-floppy-save"></span> Save</button>


    <!----------end bottom-section----------->
    <?php echo form_close(); ?>
    <!----------end form----------->
</div>
<?php
echo validation_errors();
?>
