<?php if (isset($publication)) { ?>
    <article>
        <header><h3><u><?php echo $publication['publication_title']; ?></u></h3></header>
        <span class="small_faded">Trainer: <?php echo $publication['published_date']; ?></span>
        <span class="small_faded" style="margin-left: 50px;"><a href="<?php echo $publication['publication_url']; ?>">Go To Publication</a></span><br/>
        
        <p><?php echo $publication['publication_synapse']; ?></p>
        <?php if ($this->Authorizer->authorize(array(ADMIN, DIRECTOR))) { ?>
            <span class="small_faded">Created: <?php echo $publication['created']; ?></span>
            <span class="small_faded" style="margin-left: 50px;">Modified : <?php echo $publication['modified']; ?></span>
        <?php } ?>
    </article>
<?php } ?>