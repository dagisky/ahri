<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title>AHRI</title>
        <link rel="shortcut icon" href="<?php echo base_url(); ?>resource/img/Title.ico">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- Bootstrap -->
        <link href="<?php echo base_url(); ?>resource/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>resource/css/custom.css" rel="stylesheet">       

        <?php
        if (isset($styles)) {
            foreach ($styles as $style) {
                echo '<link rel="stylesheet" href="' . base_url() . 'resource/css/' . $style . '.css" type="text/css" media="all">';
            }
        }
        ?>
        <script src="<?php echo base_url(); ?>resource/js/respond.js"></script>

    </head>

    <body>

        <div class="container">

            <!-- row 1 -->
            <header class="row">
                <div class="col-lg-2 col-sm-12">
                    <a href="index.html"><img src="<?php echo base_url(); ?>resource/img/logo.png" alt="Wisdom Pets. click for home."></a>
                </div>
                <div class="col-lg-10 col-sm-12">                    
                    <?php
                    if (isset($mainNav))
                        $this->load->view($mainNav);
                    ?>
                </div>
            </header>

            <!-- row 2 -->
            <?php
            if (isset($jumbotron)) {
                foreach ($jumbotron as $view) {
                    $this->load->view($view);
                }
            }
            ?>


            <!-- row 3 -->
            <div class="row">
                <!-- Aside -->

                <div class="col-lg-9" style="padding-right: 30px;">
                    <?php
                    if (isset($mainPane)) {
                        foreach ($mainPane as $view) {
                            $this->load->view($view);
                        }
                    }
                    ?>
                </div>
                
                <aside class="col-lg-3">
                    <?php
                    if (isset($aside)) {
                        foreach ($aside as $view) {
                            $this->load->view($view);
                        }
                    }
                    ?>
                </aside>
            </div>



        </div> <!-- end container -->


        <!-- Footer Starts -->
        <footer class="row" style="margin-top: 100px;">

            <div class="col-lg-4 col-sm-6 col-xm-12">
                <h2>Stay In The Know !</h2>
                <p>Please enter your email to join our mailing list</p>
                <?php
                $attributes = array('class' => 'sign', 'role' => 'form');
                echo form_open('Mailing/add', $attributes);
                ?>
                <fieldset>
                    <legend>News Letter</legend>
                    <input style="display: inline; width: 320px;" name="mailingAddress" class="form-control" type="text" placeholder="Enter Your Email Address"/>
                    <input style="display: inline;" class="btn btn-default" type="submit" name="news_go"  value="GO" />
                </fieldset>
                <?php echo form_close(); ?>
                <p>To unsubscribe please <a href="#">click here &raquo;</a></p>
            </div>
            <div class="col-lg-4 col-sm-6 col-xm-12">
                <h2 >Further Information !</h2>
                <ul>
                    <li><a href="#">Business Terms &amp; Conditions &raquo;</a></li>
                    <li><a href="#">Disability Philosophy &raquo;</a></li>
                    <li class="last"><a href="#">Contact Our Company &raquo;</a></li>
                </ul>
            </div>
            <div class="col-lg-4 col-sm-6 col-xm-12">
                <h2>Company Details !</h2>
                <address>
                    AHRI (Armauer Hansen Research Institute)<br />
                    ALERT compound, Jimma Road<br />
                    Addis Ababa<br />
                    P.O. Box : 1005<br />
                </address>
                Tel:  +251-113-483752
                <!-- -->
            </div>

        </footer>
        <footer class="row cpyRg">
            <div class="col-lg-6">
                <span>Copyright &copy; 2011 - All Rights Reserved - <a href="#">Domain Name</a></span>               
            </div>
            <div>
                <p class="fl_right">Design and Development by <a href="http://abugidatemari.net" title="abugida Temari">AbugidaTemari</a></p>                
            </div>
        </footer>
        <!--Footer ends-->
        <!-- javascript -->
        <script src="<?php echo base_url(); ?>resource/js/jquery.min.js"></script>
        <script src="<?php echo base_url(); ?>resource/js/bootstrap.min.js"></script>
        <?php
        if (isset($scripts)) {
            foreach ($scripts as $script) {
                echo '<script src="' . base_url() . 'resource/js/' . $script . '.js"></script>';
            }
        }
        ?>

        <script type="text/javascript">
            $('.carousel').carousel({
                interval: 5000
            });

            $(function() {
                $('#datetimepicker1').datetimepicker();
            });

            $(function() {
                $('#datetimepicker2').datetimepicker();
                $('#datetimepicker3').datetimepicker();
            });

            $('.textarea').wysihtml5();


        </script>

    </body>
</html>
