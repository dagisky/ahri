<?php
if ($this->Authorizer->authorize(array(ADMIN, DIRECTOR))) {
    echo '<a href="' . base_url() . 'index.php/Events/add" style="margin-right:15px;"><span class="glyphicon glyphicon-plus-sign"><span> Add Event</a>';
    echo '<a href="' . base_url() . 'index.php/Events"><span class="glyphicon glyphicon-list"><span> Events List</a>';
    
}
?>
<br/>
<br/>
<div class="panel-group" id="accordion">

    <?php if (isset($events)) foreach ($events as $event): ?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $event['event_id']; ?>">
                            <?php echo $event['event_title']; ?>
                        </a>
                    </h4>
                    <span class="small_faded">Posted Date : <?php echo $event['created']; ?></span>
                     <?php
                    if ($this->Authorizer->authorize(array(ADMIN, DIRECTOR))) {
                        echo '<a href="' . base_url() . 'index.php/Events/edit/' . $event['event_id'] . '" style="margin-right:20px; margin-left:20px;"><small><span class="glyphicon glyphicon-edit"></span> Edit Event</small></a>';
                        echo '<a href="' . base_url() . 'index.php/Events/delete/' . $event['event_id'] . '"><small><span class="glyphicon glyphicon-trash"></span> Remove Event</small></a>';
                    }
                    ?>
                </div>
                <div id="collapse<?php echo $event['event_id']; ?>" class="panel-collapse collapse out">
                    <div class="panel-body">
                        <?php echo $event['event_content']; ?>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>


</div>
<?php
if (isset($paginator))
    echo $paginator;
?>