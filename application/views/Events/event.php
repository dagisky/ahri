<?php if (isset($event)) { ?>
    <h3><?php echo $event['event_title']; ?></h3>
    
    <p><?php echo $event['event_content']; ?></p>
    <?php if ($this->Authorizer->authorize(array(ADMIN, DIRECTOR))) { ?>
        <span class="small_faded">Created: <?php echo $event['created']; ?></span>
        <span class="small_faded" style="margin-left: 50px;">Modified : <?php echo $event['modified']; ?></span>
    <?php } ?>
<?php } ?>
