<?php
if ($this->Authorizer->authorize(array(ADMIN, DIRECTOR))) {
    echo '<a href="' . base_url() . 'index.php/Events/add" style="margin-right:15px;"><span class="glyphicon glyphicon-plus-sign"><span> Add Event</a>';
    echo '<a href="' . base_url() . 'index.php/Events"><span class="glyphicon glyphicon-list"><span> Events List</a>';
}
?>
<div id="regForm">
    <?php
    $attributes = array('class' => 'sign', 'role' => 'form');
    if (isset($event)) {
        echo form_open('Events/edit/'.$event['event_id'], $attributes);
        ?>
        <div class="row">
            <div class="col-lg-6 form-group">  
                <h3>Edit Event</h3>
                <lebel for="event_title">Event Title</lebel>
                <input type="text" class="form-control" id="event_title" name="event_title" placeholder="Event Title" value="<?php echo $event['event_title'];?>">

                <lebel for="event_content">Event Content</lebel>                
                <textarea name="event_content" id="event_content" class="textarea form-control" placeholder="Enter text ..." style=" height: 200px"><?php echo $event['event_content']; ?></textarea>
            </div>

            <div class="col-lg-6" style="padding-top: 77px;">

                <div class="panel-group" id="accordion">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title has-success">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $event['event_id']; ?>">
                                    <?php echo $event['event_title']; ?>
                                </a>
                            </h4>
                            <span style="font-size: 12px; color: #999;">Posted Date : <?php echo $event['created']; ?></span>
                        </div>
                        <div id="collapse<?php echo $event['event_id']; ?>" class="panel-collapse collapse out">
                            <div class="panel-body">
                                <?php echo $event['event_content']; ?>
                            </div>
                        </div>
                    </div>
                    
                </div>

            </div>       
        </div>
        <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-floppy-save"></span> Save</button>
    <?php } ?>

    <!----------end bottom-section----------->
    <?php echo form_close(); ?>
    <!----------end form----------->
</div>
<?php
echo validation_errors();
?>
