<?php if (isset($lab)) { ?>
    <article>     
        <header >                     
            <h3><?php echo $lab['laboratory_name']; ?></h3>                            
        </header>                      
        <img src="<?php echo str_replace('lab/', 'lab/thumb-', $lab['profile_img']); ?>" class="img-thumbnail"/>
        <p><?php echo $lab['description']; ?></p>
    </article>
    <?php if ($this->Authorizer->authorize(array(ADMIN, DIRECTOR))) { ?>
        <span class="small_faded">Created: <?php echo $lab['created']; ?></span>
        <span class="small_faded" style="margin-left: 50px;">Modified : <?php echo $lab['modified']; ?></span>
    <?php } ?>
<?php } ?>
