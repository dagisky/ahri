<?php
if ($this->Authorizer->authorize(array(ADMIN, DIRECTOR))) {
    echo '<a href="' . base_url() . 'index.php/laboratories/add" style="margin-right:15px;"><span class="glyphicon glyphicon-plus-sign"><span> Add Laboratory</a>';
    echo '<a href="' . base_url() . 'index.php/laboratories"><span class="glyphicon glyphicon-list"><span> Laboratory List</a>';
}
?>
<div id="regForm">
    <?php
    $attributes = array('class' => 'sign', 'role' => 'form');
    echo form_open_multipart('Laboratories/add', $attributes);
    ?>
    <div class="row">
        <div class="col-lg-6 form-group">  
            <h3>Add New Laboratory</h3>
            <lebel for="labName">Laboratory Name</lebel>
            <input type="text" class="form-control" id="labName" name="lab_name" value="<?php echo set_value('lab_name'); ?>" placeholder="Laboratory Name">


            <lebel for="stuffType">Upload Profile Image</lebel>
            <input type="file" class="form-control" name="gallery_img" />



            <div class="hero-unit" style="margin-top:40px">
                <lebel for="labDesc">Description</lebel>
                <hr/>
                <textarea name="lab_desc" class="textarea form-control" placeholder="Enter text ..." style="width: 810px; height: 200px"><?php echo set_value('lab_desc'); ?></textarea>
            </div>


        </div>
        <div class="col-lg-6" style="padding-top: 77px;">
            <?php if (isset($lab)) { ?>
                <div class="panel-group" id="accordion">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-lg-3">
                                    <a  href="#" >
                                        <img  class="thumb img-circle" alt="Profile Image" src="<?php echo $lab['profile_img'] != null ? $lab['profile_img'] : DEFAULT_RPOFILE_IMG; ?>">
                                    </a>
                                </div>
                                <div class="col-lg-9">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $lab['laboratory_id']; ?>">
                                    <h4><?php echo $lab['laboratory_name']; ?></h4>
                                    </a>
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $lab['laboratory_id']; ?>">
                                        <img src="<?php echo str_replace('lab/', 'lab/thumb-', $lab['profile_img']); ?>" class="img-thumbnail">
                                    </a>

                                </div>
                            </div>
                        </div>

                        <div id="collapse<?php echo $lab['laboratory_id']; ?>" class="panel-collapse collapse out">
                            <div class="panel-body">
                                <p>
                                    <?php echo $lab['description']; ?>
                                </p>
                            </div>
                        </div>

                    </div>
                    <span class="has-success">Success!</span>
                </div>
            <?php } ?>
        </div>       
    </div>
    <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-floppy-save"></span> Save</button>


    <!----------end bottom-section----------->
    <?php echo form_close(); ?>
    <!----------end form----------->
</div>
<?php
echo validation_errors();
?>
<script type="text/javascript">
    ele = document.getElementById('stuffType');
    ele.onchange = function() {
        foo = document.getElementById('researcherField');
        if (ele.value == 'Researcher') {
            foo.style["display"] = "block";
        } else {
            foo.style["display"] = "none";
        }
    }

</script>