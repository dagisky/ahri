<?php
if ($this->Authorizer->authorize(array(ADMIN, DIRECTOR))) {
    echo '<a href="' . base_url() . 'index.php/laboratories/add" style="margin-right:15px;"><span class="glyphicon glyphicon-plus-sign"><span> Add Laboratory</a>';
    echo '<a href="' . base_url() . 'index.php/laboratories"><span class="glyphicon glyphicon-list"><span> Laboratory List</a>';
}
?>
<div id="regForm">
    <?php
    $attributes = array('class' => 'sign', 'role' => 'form');
    if (isset($lab)) {
        echo form_open_multipart('Laboratories/edit/'.$lab['laboratory_id'], $attributes);
        ?>
        <div class="row">
            <div class="col-lg-6 form-group">  
                <h3>Edit Laboratory</h3>
                <lebel for="labName">Laboratory Name</lebel>
                <input type="text" class="form-control" id="labName" name="labName" value="<?php echo $lab['laboratory_name']; ?>" placeholder="Laboratory Name">


                <lebel for="stuffType">Change Profile Image</lebel>
                <img  class="thumbnail" alt="Profile Image" src="<?php echo str_replace("lab/", "lab/thumb-", $lab['profile_img']); ?>">
                <input type="file" class="form-control" name="gallery_img" />



                <div class="hero-unit" style="margin-top:40px">
                    <lebel for="labDesc">Description</lebel>
                    <hr/>
                    <textarea name="labDesc" class="textarea form-control"  style="width: 810px; height: 200px"><?php echo $lab['description']; ?></textarea>
                </div>


            </div>
            <div class="col-lg-6" style="padding-top: 77px;">

                <div class="panel-group" id="accordion">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-lg-3">
                                    <a  href="#" >
                                        <img  class="thumb img-circle" alt="Profile Image" src="<?php echo str_replace("lab/", "lab/thumb-", $lab['profile_img']); ?>">
                                    </a>
                                </div>
                                <div class="col-lg-9">

                                    
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $lab['laboratory_id']; ?>">
                                        <h4><?php echo $lab['laboratory_name']; ?></h4>
                                    </a>

                                </div>
                            </div>
                        </div>

                        <div id="collapse<?php echo $lab['laboratory_id']; ?>" class="panel-collapse collapse out">
                            <div class="panel-body">
                                <p>
                                    <?php echo $lab['description']; ?>
                                </p>
                            </div>
                        </div>

                    </div>
                    
                </div>

            </div>       
        </div>
        <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-floppy-save"></span> Save</button>
    <?php } ?>

    <!----------end bottom-section----------->
    <?php echo form_close(); ?>
    <!----------end form----------->
</div>
<?php
echo validation_errors();
?>
<script type="text/javascript">
    ele = document.getElementById('stuffType');
    ele.onchange = function() {
        foo = document.getElementById('researcherField');
        if (ele.value == 'Researcher') {
            foo.style["display"] = "block";
        } else {
            foo.style["display"] = "none";
        }
    }

</script>