<div class="panel-group" id="accordion">
    <?php
    if ($this->Authorizer->authorize(array(ADMIN, DIRECTOR))) {
        echo '<a href="' . base_url() . 'index.php/laboratories/add" style="margin-right:15px;"><span class="glyphicon glyphicon-plus-sign"><span> Add Laboratory</a>';
        echo '<a href="' . base_url() . 'index.php/laboratories"><span class="glyphicon glyphicon-list"><span> Laboratory List</a>';
    }
    ?>
    <ul>
        <?php if (isset($labs)) foreach ($labs as $lab): ?>
                <li style="list-style: none; margin-bottom: 20px; padding-bottom: 7px; border-bottom: 1px solid #DDD;">                   
                    <article>     
                        <header >                     
                            <h3><?php echo $lab['laboratory_name']; ?></h3>                            
                        </header>                      
                        <img src="<?php echo str_replace('lab/', 'lab/thumb-', $lab['profile_img']); ?>" class="img-thumbnail"/>
                        <p><?php echo $lab['description']; ?></p>
                    </article>
                    <?php
                    if ($this->Authorizer->authorize(array(ADMIN, DIRECTOR))) {
                        echo '<a href="' . base_url() . 'index.php/laboratories/edit/' . $lab['laboratory_id'] . '" style="margin-right:20px;"><small><span class="glyphicon glyphicon-edit"></span> Edit Laboratory</small></a>';
                        echo '<a href="' . base_url() . 'index.php/laboratories/delete/' . $lab['laboratory_id'] . '"><small><span class="glyphicon glyphicon-trash"></span> Remove Laboratory</small></a>';
                    }
                    ?>


                </li>
            <?php endforeach; ?>
    </ul>


</div>
<?php
if (isset($paginator))
    echo $paginator;
?>