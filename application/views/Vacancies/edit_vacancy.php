<?php
if ($this->Authorizer->authorize(array(ADMIN, DIRECTOR))) {
    echo '<a href="' . base_url() . 'index.php/Vacancies/add" style="margin-right:15px;"><span class="glyphicon glyphicon-plus-sign"><span> Add Vacancy</a>';
    echo '<a href="' . base_url() . 'index.php/Vacancies"><span class="glyphicon glyphicon-list"><span> Vacancy List</a>';
    
}
?>
<div id="regForm">
    <?php
    $attributes = array('class' => 'sign', 'role' => 'form');
    if (isset($vacancy)) {
    echo form_open('Vacancies/edit/'.$vacancy['vacancy_id'], $attributes);
    ?>
    <div class="row">
        <div class="col-lg-6 form-group">  
            <h3>Edit Vacancy</h3>
            <lebel for="position">Position</lebel>
            <input type="text" class="form-control" id="position" name="position" placeholder="News Title" value="<?php echo $vacancy['position']; ?>">
            
            
            <div class="hero-unit" style="margin-top:40px">
                <lebel for="eligibility">Eligibility</lebel>
                <hr/>
                <textarea name="eligibility" id="eligibility" class="textarea form-control" placeholder="Enter text ..." style="width: 810px; height: 200px"><?php echo $vacancy['eligibility']; ?></textarea>
            </div>
            
            <div class="hero-unit" style="margin-top:40px">
                <lebel for="responsibilities">Responsibilities</lebel>
                <hr/>
                <textarea name="responsibilities" id="responsibilities" class="textarea form-control" placeholder="Enter text ..." style="width: 810px; height: 200px"> <?php echo $vacancy['responsibilities']; ?> </textarea>
            </div>
            
            <lebel for="deadline">Deadline</lebel>
            <div class="form-group">
                <div class='input-group date' id='datetimepicker1'>
                    <input type='text' name="deadline" value="<?php echo $this->TimeF->MysqlToHuman($vacancy['deadline']); ?>" id="deadline" class="form-control"/>
                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>

        </div>
        <div class="col-lg-6" style="padding-top: 77px;">
            
                <div class="panel-group" id="accordion">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title has-success">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $vacancy['vacancy_id']; ?>">
                                    <?php echo $vacancy['position']; ?>
                                </a>
                            </h4>
                            <span style="font-size: 12px; color: #999;">Posted Date : <?php echo $vacancy['created']; ?></span>
                            <span style="font-size: 12px; color: #999;">Deadline : <?php echo $vacancy['deadline']; ?></span>
                        </div>
                        <div id="collapse<?php echo $vacancy['vacancy_id']; ?>" class="panel-collapse collapse out">
                            <div class="panel-body">
                                <h4>Eligibilities</h4>
                                <?php echo $vacancy['eligibility']; ?>
                                <h4>Responsibilities</h4>
                                <?php echo $vacancy['responsibilities']; ?>
                            </div>
                        </div>
                    </div>
                    
                </div>
            
        </div>       
    </div>
    <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-floppy-save"></span> Save</button>
    <?php } ?>

    <!----------end bottom-section----------->
    <?php echo form_close(); ?>
    <!----------end form----------->
</div>
<?php
echo validation_errors();
?>
