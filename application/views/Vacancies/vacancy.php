<?php if (isset($vacancy)) { ?>
<h3><?php echo $vacancy['position']; ?></h3>
<span class="small_faded">Position Openings: <?php echo $vacancy['openings']; ?></span>
<span class="small_faded" style="margin-left: 50px;">Deadline : <?php echo $vacancy['deadline']; ?></span>
<p><?php echo $vacancy['eligibility']; ?></p>
<p><?php echo $vacancy['responsibilities']; ?></p>
<?php if ($this->Authorizer->authorize(array(ADMIN, DIRECTOR))) { ?>
    <span class="small_faded">Created: <?php echo $vacancy['created']; ?></span>
    <span class="small_faded" style="margin-left: 50px;">Modified : <?php echo $vacancy['modified']; ?></span>
<?php } ?>
<?php }?>