<?php
if ($this->Authorizer->authorize(array(ADMIN, DIRECTOR))) {
    echo '<a href="' . base_url() . 'index.php/Vacancies/add" style="margin-right:15px;"><span class="glyphicon glyphicon-plus-sign"><span> Add Vacancy</a>';
    echo '<a href="' . base_url() . 'index.php/Vacancies"><span class="glyphicon glyphicon-list"><span> Vacancy List</a>';
}
?>
<br/>
<br/>
<div class="panel-group" id="accordion">

    <?php if (isset($vacancies)) foreach ($vacancies as $vacancy): ?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $vacancy['vacancy_id']; ?>">
                            <?php echo $vacancy['position']; ?>
                        </a>
                    </h4>
                    <span style="font-size: 12px; color: #999; margin-right: 10px;">Posted Date : <?php echo $vacancy['created']; ?></span>
                    <span style="font-size: 12px; color: #999; margin-right: 10px;">Deadline : <?php echo $vacancy['deadline']; ?></span>
                    <span class="glyphicon glyphicon-user" style="font-size: 12px; color: #999;"> Openings : <?php echo $vacancy['openings']; ?></span>
                    <?php
                    if ($this->Authorizer->authorize(array(ADMIN, DIRECTOR))) {
                        echo '<a href="' . base_url() . 'index.php/Vacancies/edit/' . $vacancy['vacancy_id'] . '" style="margin-right:20px; margin-left:20px;"><small><span class="glyphicon glyphicon-edit"></span> Edit Vacancy</small></a>';
                        echo '<a href="' . base_url() . 'index.php/Vacancies/delete/' . $vacancy['vacancy_id'] . '"><small><span class="glyphicon glyphicon-trash"></span> Remove Vacancy</small></a>';
                    }
                    ?>
                </div>
                <div id="collapse<?php echo $vacancy['vacancy_id']; ?>" class="panel-collapse collapse out">
                    <div class="panel-body">
                        <h4>Eligibilities</h4>
                        <?php echo $vacancy['eligibility']; ?>
                        <h4>Responsibilities</h4>
                        <?php echo $vacancy['responsibilities']; ?>
                    </div>
                </div>

            </div>
        <?php endforeach; ?>


</div>
<?php
if (isset($paginator))
    echo $paginator;
?>