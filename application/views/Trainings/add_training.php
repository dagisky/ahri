<?php
if ($this->Authorizer->authorize(array(ADMIN, DIRECTOR))) {
    echo '<a href="' . base_url() . 'index.php/Trainings/add" style="margin-right:15px;"><span class="glyphicon glyphicon-plus-sign"><span> Add Training</a>';
    echo '<a href="' . base_url() . 'index.php/Trainings"><span class="glyphicon glyphicon-list"><span> Training List</a>';
}
?>
<div id="regForm">
    <?php
    $attributes = array('class' => 'sign', 'role' => 'form');
    echo form_open('Trainings/add', $attributes);
    ?>
    <div class="row">
        <div class="col-lg-6 form-group">  
            <h3>Add New Training</h3>
            <lebel for="trainingTitle">Training Title</lebel>
            <input type="text" class="form-control" id="trainingTitle" value="<?php echo set_value('trainingTitle');?>" name="training_title" placeholder="Training Title">
            <lebel for="trainerName">Trainer Name</lebel>
            <input type="text" class="form-control" id="trainerName" value="<?php echo set_value('trainerName');?>" name="training_name" placeholder="Trainer Full Name">
            <lebel for="trainerEmail">Trainer Email</lebel>
            <input class="form-control" type="text" id="trainerEmail" name="trainerEmail" value="<?php echo set_value('trainerEmail');?>" placeholder="trainermail@domain.ext" />
            <lebel for="trainingDesc">Training Description</lebel>
            <textarea rows="10" class="form-control" id="trainingDesc" name="trainingDesc"><?php echo set_value('trainingDesc');?></textarea>
            
            <lebel for="dob">Starting Date</lebel>
            <div class="form-group">
                <div class='input-group date' id='datetimepicker1'>
                    <input type='text' name="start_date" value="<?php echo set_value('start_date'); ?>" id="dob" class="form-control"/>
                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
            
            <lebel for="dob">Ending Date</lebel>
            <div class="form-group">
                <div class='input-group date' id='datetimepicker2'>
                    <input type='text' name="end_date" value="<?php echo set_value('end_date'); ?>" id="dob" class="form-control"/>
                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
            
        </div>
        <div class="col-lg-6" style="padding-top: 77px;">
            <?php if (isset($training)) {?>
            
                <div class="panel-group" id="accordion">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title has-success">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $training['training_id']; ?>">
                                    <?php echo $training['training_title']; ?>
                                </a>
                            </h4>
                            <span style="font-size: 12px; color: #999;">Starting Date : <?php echo $training['training_starting_date']; ?></span>
                            <span style="font-size: 12px; color: #999; margin-left: 25px;">Ending Date : <?php echo $training['training_ending_date']; ?></span><br/>
                            <span style="font-size: 12px; color: #999; ">Trainer : <?php echo $training['trainer_name']; ?></span>
                        </div>
                        <div id="collapse<?php echo $training['training_id']; ?>" class="panel-collapse collapse out">
                            <div class="panel-body">
                                <?php echo $training['training_desc']; ?>
                            </div>
                        </div>
                    </div>
                    <span class="has-success">Success!</span>
                </div>
            <?php } ?>
        </div>       
    </div>
    <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-floppy-save"></span> Save</button>


    <!----------end bottom-section----------->
    <?php echo form_close(); ?>
    <!----------end form----------->
</div>
<?php
echo validation_errors();
?>
