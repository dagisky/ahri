<?php if (isset($training)) { ?>
    <article>
        <header><h3><u><?php echo $training['training_title']; ?></u></h3></header>
        <span class="small_faded">Trainer: <?php echo $training['trainer_name']; ?></span>
        <span class="small_faded" style="margin-left: 50px;">Trainer email: <?php echo $training['trainer_email']; ?></span><br/>
        <span class="small_faded">Starting Date: <?php echo $training['training_starting_date']; ?></span>
        <span class="small_faded" style="margin-left: 50px;">Ending Date: <?php echo $training['training_ending_date']; ?></span>
        <p><?php echo $training['training_desc']; ?></p>
        <?php if ($this->Authorizer->authorize(array(ADMIN, DIRECTOR))) { ?>
            <span class="small_faded">Created: <?php echo $training['created']; ?></span>
            <span class="small_faded" style="margin-left: 50px;">Modified : <?php echo $training['modified']; ?></span>
        <?php } ?>
    </article>
<?php } ?>