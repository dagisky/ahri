<?php
if ($this->Authorizer->authorize(array(ADMIN, DIRECTOR))) {
    echo '<a href="' . base_url() . 'index.php/Trainings/add" style="margin-right:15px;"><span class="glyphicon glyphicon-plus-sign"><span> Add Training</a>';
    echo '<a href="' . base_url() . 'index.php/Trainings"><span class="glyphicon glyphicon-list"><span> Training List</a>';
}
?>
<br/>
<br/>

<div class="panel-group" id="accordion">

    <?php if (isset($trainings)) foreach ($trainings as $training): ?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $training['training_id']; ?>">
                            <?php echo $training['training_title']; ?>
                        </a>
                    </h4>
                    <span class="small_faded">Starting Date : <?php echo $training['training_starting_date']; ?></span>
                    <span class="small_faded">Ending Date : <?php echo $training['training_ending_date']; ?></span>
                    <?php
                    if ($this->Authorizer->authorize(array(ADMIN, DIRECTOR))) {
                        echo '<a href="' . base_url() . 'index.php/Trainings/edit/' . $training['training_id'] . '" style="margin-right:20px; margin-left:20px;"><small><span class="glyphicon glyphicon-edit"></span> Edit Training</small></a>';
                        echo '<a href="' . base_url() . 'index.php/Trainings/delete/' . $training['training_id'] . '"><small><span class="glyphicon glyphicon-trash"></span> Remove Training</small></a>';
                    }
                    ?>
                </div>
                <div id="collapse<?php echo $training['training_id']; ?>" class="panel-collapse collapse out">
                    <div class="panel-body">
                        <?php echo $training['training_desc']; ?>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>


</div>
<?php
if (isset($paginator))
    echo $paginator;
?>