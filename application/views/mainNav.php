<nav class="row navbar-default navbar-custom" role="navigation">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#collapse">
            <span class="sr-only">Toggle Navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
    </div>
    <div class="collapse navbar-collapse" id="collapse">
        <ul class="nav nav-tabs nav-justified">
            <li><a href="<?php echo base_url(); ?>">Home</a></li>                       
            <li><a href="#" data-toggle="dropdown">About AHRI <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="<?php echo base_url(); ?>index.php/About">About Us</a></li>
                    <li><a href="<?php echo base_url(); ?>index.php/About/About/9">Vision and Mission</a></li>
                    <li class="dropdown-submenu"><a tabindex="-1" href="#">Organization</a>
                        <ul class="dropdown-menu">
                            <li><a href="<?php echo base_url().'index.php/About/index/1';?>">Office of The Director</a></li>
                            <li><a href="<?php echo base_url().'index.php/About/index/2';?>">Scientific Advisory Board</a></li>
                            <li><a href="<?php echo base_url().'index.php/About/index/3';?>">Organizational Chart</a></li>
                        </ul>
                    </li>
                    <li class="dropdown-submenu"><a tabindex="-1" href="">Staff Directory</a>
                        <ul class="dropdown-menu">
                            <li><a href="<?php echo base_url();?>index.php/Directors">The Director</a></li>
                            <li><a href="<?php echo base_url();?>index.php/Staffs/researchers">Researchers</a></li>
                            <li><a href="<?php echo base_url();?>index.php/Staffs/Laboratory">Laboratory</a></li>
                            <li><a href="<?php echo base_url();?>index.php/Staffs/DataStat">Data Management</a></li>
                            <li><a href="<?php echo base_url();?>index.php/Staffs/Other">Research Support</a></li>                            
                        </ul>
                    </li>
                    <li class="dropdown-submenu"><a tabindex="-1" href="">Initiatives</a>
                        <ul class="dropdown-menu">
                            <li><a href="<?php echo base_url().'index.php/About/index/4';?>">AHRI/ALERT Ethics Review Committee (AAERC)</a></li>
                            <li><a href="<?php echo base_url().'index.php/About/index/5';?>">Pan-Africa Bioethics Initiatives (PABIN)</a></li>
                            <li><a href="<?php echo base_url().'index.php/About/index/6';?>">Tuberculosis Research Advisory Committee (TRAC)</a></li>
                            <li><a href="<?php echo base_url().'index.php/About/index/7';?>">Stakeholder Engagement</a></li>
                        </ul>
                    </li>
                    <li><a href="<?php echo base_url();?>index.php/Vacancies">Vacancies</a></li>
                </ul>
            </li>                       
            <li><a href="#" data-toggle="dropdown">Research <span class="caret"></span></a>

                <ul class="dropdown-menu">
                    <li class="dropdown-submenu"><a tabindex="-1" href="">Research Teams</a>
                        <ul class="dropdown-menu">
                            <?php
                            $teams = $this->Team->view();
                            foreach ($teams['data'] as $team) {
                                echo '<li><a href="'.  base_url().'index.php/Teams/team/'.$team['team_id'].'">' . $team['team_name'] . '</a><li>';
                            }
                            ?>
                        </ul>
                    </li>
                    <li><a href="<?php echo base_url(); ?>index.php/Researchs">Research Projects</a></li>
                    <li><a href="<?php echo base_url();?>index.php/Laboratories">Laboratory Facilties </a></li>
                </ul>
            </li> 
            <li><a href="#" data-toggle="dropdown">Trainings <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="<?php echo base_url();?>index.php/About/index/11">Post Graduate Research Support Program</a></li>
                    
                    <li class="dropdown-submenu"><a href="#" tabindex="-1">Training and Workshops</a>
                        <ul class="dropdown-menu">
                            <li><a href="<?php echo base_url();?>index.php/Trainings">Up Coming Courses</a></li>
                            <li><a href="">Past Courses</a></li>
                            <li><a href="">Event Calender</a></li>
                        </ul>
                    </li>                    
                </ul>
            </li>
            <li><a href="#" data-toggle="dropdown">Resources <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="<?php echo base_url(); ?>index.php/Publications">Publications</a></li>
                    <li><a href="<?php echo base_url();?>index.php/Galleries">Photo Gallery</a></li>
                    <li><a href="<?php echo base_url();?>index.php/About/index/12">Useful Links</a></li>
                    <li><a href="#">Downloads</a></li>
                </ul>
            </li>                       
            <li><a href="#" data-toggle="dropdown">News/Events <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="<?php echo base_url();?>index.php/Nws">News</a></li>
                    <li><a href="<?php echo base_url();?>index.php/Events">Events</a></li>
                </ul>
            </li>                       
            <li><a href="<?php echo base_url();?>index.php/Contact">Contact Us</a></li>                       
        </ul>
    </div>
</nav>