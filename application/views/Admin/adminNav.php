<nav class="row navbar-default navbar-custom" role="navigation">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#collapse">
            <span class="sr-only">Toggle Navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
    </div>
    <div class="collapse navbar-collapse" id="collapse">
        <ul class="nav nav-pills nav-justified">            
            <li><a href="#" data-toggle="dropdown">AHRI <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="<?php echo base_url() . 'index.php/About/index/8'; ?>">About Us</a></li>
                    <li><a href="<?php echo base_url() . 'index.php/About/index/9'; ?>">Vision and Mission</a></li>               
                    <li><a href="<?php echo base_url() . 'index.php/Auth/logout'; ?>">Log Out</a></li>               

                </ul>
            </li>
            <li><a href="#" data-toggle="dropdown">Staff <span class="caret"></span></a>
                <ul class="dropdown-menu">                    
                    <li><a href="<?php echo base_url();?>index.php/Directors">The Director</a></li>
                    <li><a href="<?php echo base_url() . 'index.php/Staffs/researchers'; ?>">Researcher Staff</a></li>
                    <li><a href="<?php echo base_url() . 'index.php/Staffs/Laboratory'; ?>">Laboratory Staff</a></li>
                    <li><a href="<?php echo base_url() . 'index.php/Staffs/DataStat'; ?>">Data Analysis and Statistics Staff</a></li>                                    
                    <li><a href="<?php echo base_url() . 'index.php/Staffs/Other'; ?>">Other Staffs</a></li>                 

                </ul>
            </li>
            <li><a href="#" data-toggle="dropdown">Research <span class="caret"></span></a>
                <ul class="dropdown-menu">                    
                    <li><a href="<?php echo base_url(); ?>index.php/Researchs">On Going Research's</a></li>
                    <li><a href="<?php echo base_url(); ?>index.php/Teams">Research Teams</a></li>
                    <li><a href="<?php echo base_url(); ?>index.php/Laboratories">Laboratory</a></li>
                </ul>
            </li>
            <li><a href="<?php echo base_url(); ?>index.php/Publications">Publications</a></li>
            <li><a href="<?php echo base_url(); ?>index.php/Trainings">Training</a></li>
            <li><a href="<?php echo base_url(); ?>index.php/Galleries">Gallery</a></li>
            <li><a href="#" data-toggle="dropdown">News/Events <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="<?php echo base_url(); ?>index.php/Nws">News</a></li>
                    <li><a href="<?php echo base_url(); ?>index.php/Events">Events</a></li>
                    <li><a href="<?php echo base_url(); ?>index.php/Vacancies">Vacancy</a></li>
                    <li><a href="<?php echo base_url() . 'index.php/Mailing/sendMail'; ?>">Compose Mail</a></li>
                </ul>
            </li>    
        </ul>
    </div>
</nav>