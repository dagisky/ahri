<div class="alert alert-danger" role="alert">
    <a href="#" class="alert-link">Are You Sure to Remove This <?php echo $remove['data'];?> ? </a>
    <a class="btn btn-default" href="<?php echo $remove['yes_link'];?>">Yes</a>
    <a class="btn btn-default" href="<?php echo $remove['no_link'];?>">No</a>
    <div>
        <?php $this->load->view($remove['load']);?>
    </div>
</div>